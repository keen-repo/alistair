<?php
/*
Template Name: Careers Archive
*/
?>

@extends('layouts.app')

@section('content')

<?php
  $page_ID = get_page_by_path('careers')->ID;
  $header_image_ID = get_post_meta( $page_ID, '_crb_career_header', true );

  global $form_submitted;
  $form_submitted = true;
?>

<header>
    <section class="hero is-medium alignfull" style="background-image:url('<?php echo wp_get_attachment_url($header_image_ID); ?>'); background-size: cover;background-position: center center; ">
        <div class="hero-body">
            <div class="container">
                <h1 class="hero-title"></h1>
            </div>
        </div>
    </section>

 </header>

<div class="has-margin-top-100">
    <h1 class="has-text-centered is-marginless">Why join us</h1>
    <h3 class="has-text-centered has-text-weight-normal">~ OUR BENEFITS ~</h3>

    <div class="columns has-margin-top-100">
        <div class="column has-text-centered">
            <img class="careers-icon" src="@asset('images/Reward.png')" alt="reward">
            <p>WE REWARD HARD WORK</p>
        </div>
        <div class="column has-text-centered">
            <img class="careers-icon" src="@asset('images/Team.png')" alt="team">
            <p>WE ARE ONE TEAM</p>
        </div>
        <div class="column has-text-centered">
            <img class="careers-icon" src="@asset('images/Loyalty.png')" alt="loyalty">
            <p>WE REWARD LOYALTY</p>
        </div>
        <div class="column has-text-centered">
            <img class="careers-icon" src="@asset('images/Development.png')" alt="development">
            <p>WE ENCOURAGE DEVELOPMENT</p>
        </div>
        <div class="column has-text-centered">
            <img class="careers-icon" src="@asset('images/Equality.png')" alt="equality">
            <p>WE PROMOTE EQUALITY</p>
        </div>
        <div class="column has-text-centered">
            <img class="careers-icon" src="@asset('images/Fun.png')" alt="fun">
            <p>WE ARE FUN</p>
        </div>
    </div>
</div>

<div class="open-vacancies alignfull has-bg-green-really-light has-padding-100-desktop has-padding-50 has-margin-top-100 has-margin-bottom-100">
<div >
    <h1 class="has-text-centered is-marginless">Open vacancies</h1>
    <h3 class="has-text-centered has-text-weight-normal">~ JOIN US NOW ~</h3>
</div>

<div class="entry-content">
<section class="columns is-centered">
    <div class="columns column is-multiline">
        @while (have_posts()) @php the_post() @endphp
        <?php
            $job_position = carbon_get_the_post_meta( 'job_position' );
            $job_type = carbon_get_the_post_meta( 'job_type' );
            $job_description = carbon_get_the_post_meta( 'job_description' );
            $job_responsibilities = carbon_get_the_post_meta( 'job_responsibilities' );
            $job_skills_experience = carbon_get_the_post_meta( 'job_skills_experience' );
            ?>
            <div class="column vacancy-column has-padding-30">
                <p class="is-uppercase has-margin-top-20 has-margin-bottom-20 is-size-6 has-text-weight-bold"><?php echo $job_position ?></p>
                <div class="is-flex">
                    <img class="clock" src="@asset('images/Clock.png')" alt="clock">
                    <p class="job-type has-text-weight-bold is-marginless"><?php echo $job_type ?></p>
                </div>
                <p class="job-description is-size-6 has-text-weight-normal has-margin-top-20 has-margin-bottom-20"><?php echo $job_description ?></p>
                <div class="buttons">
                    <a href="<?php echo get_permalink() ?>" class="button is-normal is-radiusless btn-color-primary">READ MORE</a>
                    <a data-job-position="<?php echo $job_position ?>" class="apply button is-normal is-radiusless btn-color-primary">APPLY NOW</a>
                </div>
                <p><?php echo do_shortcode('[Sassy_Social_Share url="'.get_permalink().'" title="SHARE ON:" ]'); ?></p>
            </div>
        @endwhile
    </div>
</section>
</div>
</div>

<?php
    $archive_page = get_page_by_path('careers');
    global $post;
    $post = $archive_page;
    setup_postdata( $post );
    the_content();
?>

@php
    $form_shortcode = get_post_meta( $page_ID, '_crb_apply_form', true );
    $var = do_shortcode($form_shortcode);
@endphp

@include('partials/modal', ['title' => 'Application form', 'content' => $var, 'active' => $form_submitted])

  {!! get_the_posts_navigation() !!}
@endsection
