@extends('layouts.app')

@section('content')

<?php
  $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
  $header_image_ID = get_term_meta( $term->term_id, '_crb_banner_image', true );
?>

<header>
    <section class="hero is-medium alignfull" style="background-image:url('<?php echo wp_get_attachment_url($header_image_ID); ?>'); background-size: cover;background-position: center center; ">
        <div class="hero-body">
            <div class="container">
                <h1 class="hero-title"><?php echo $term->name ?></h1>
            </div>
        </div>
    </section>

 </header>

<div class="entry-content">
<section class="feautered_boxes has-text-centered has-padding-100 has-padding-50-mobile">
  <div class="columns is-marginless is-centered is-multiline">
  
  @while (have_posts()) @php the_post() @endphp

  <?php 
    $images = array();
    $images = carbon_get_the_post_meta('event_images');

    $title = carbon_get_the_post_meta('event_name');
  ?>

  <div class="column is-4-tablet">
          <div class="hero" style="background-image: url('<?php echo wp_get_attachment_url($images[0]); ?>'); background-size: cover;background-position: center center; " >
              <div class="box-wrap">
                  <div class="box-ratio">
                      <div class="box-content">
                          <div class="box-elements is-flex button-border">
                              <a href="<?php echo get_permalink(); ?>" class="button is-normal is-borderless is-radiusless btn-color-medium-light"><?php echo $title;?></a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  @endwhile

  </div>
  </section>

    <section class="has-padding-100 has-padding-50-mobile">
            <h2 class="section-title has-text-centered">Events & designs<span>~ PASSION FOR FLOWERS ~</span></h2>

            <?php $events_designs_category = get_terms( array(
                'taxonomy' => 'event_category',
            ) ); 

            ?>
            <?php $designs = get_posts(array( 'post_type' => 'design' )); ?>
            <section class="feautered_boxes">
            <div class="sliding-designs" style="width: 100%">
                <?php foreach ($events_designs_category as $single_term) {
                    $images = array();
                    $images = carbon_get_post_meta($single_term->ID, 'design_images');
                    $title = carbon_get_post_meta($single_term->ID, 'design_name');
                    $preview_image = get_term_meta( $single_term->term_id, '_crb_box_image', true );
                ?>
                    <li class="featured-image">
                        <div class="hero" style="background-image: url('<?php echo wp_get_attachment_url($preview_image); ?>'); background-size: cover;background-position: center center; " >
                            <div class="box-wrap">
                                <div class="box-ratio">
                                    <div class="box-content">
                                        <div class="box-elements is-flex button-border">
                                            <a href="<?php echo get_term_link($single_term); ?>" class="button is-normal is-borderless is-radiusless btn-color-medium-light"><?php echo $term->name;?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </div>
        </section>
    </section>
  
  </div>

  <?php 
    $seo_background_image = get_term_meta( $term->term_id, '_crb_seo_image', true ); 
    $seo_column_1 = get_term_meta( $term->term_id, '_crb_seo_column_1', true ); 
    $seo_column_2 = get_term_meta( $term->term_id, '_crb_seo_column_2', true ); 
  ?>
  <div class="seo-background alignfull has-padding-100 has-padding-50-mobile" style="background-image:url('<?php echo wp_get_attachment_url($seo_background_image); ?>'); background-size: cover;background-position: center center; ">
        <div class="columns is-mobile">
            <div class="column"><?php echo $seo_column_1 ?></div>
            <div class="column is-narrow vertical-separator"></div>
            <div class="column"><?php echo $seo_column_2 ?></div>
        </div>
  </div>

  {!! get_the_posts_navigation() !!}
@endsection
