<?php
/*
Template Name: Event Archive
*/
?>

@extends('layouts.app')

@section('content')

@php

    $args = array(
        'offset' => 0,
        'orderby' => 'post_date',
        'order' => 'DESC',
        'include' => '',
        'exclude' => '',
        'meta_key' => '',
        'meta_value' =>'',
        'post_type' => 'event',
        'post_status' => 'publish',
        'suppress_filters' => true,
        'numberposts' => -1,
    );

    $events = wp_get_recent_posts( $args, ARRAY_A );
@endphp

<?php
  $page_ID = get_page_by_path('events-corporate/events-designs')->ID;
  $header_image_ID = get_post_meta( $page_ID, '_crb_event_header', true );
?>

<header>
    <section class="hero is-medium alignfull" style="background-image:url('<?php echo wp_get_attachment_url($header_image_ID); ?>'); background-size: cover;background-position: center center; ">
        <div class="hero-body">
            <div class="container">
                <h1 class="hero-title">Events & designs</h1>
            </div>
        </div>
    </section>

 </header>

<div class="entry-content">

<section class="feautered_boxes has-text-centered">
  <div class="columns is-centered is-multiline has-margin-100-desktop has-margin-50">

  <?php
    $args = array(
        'taxonomy' => 'event_category',
        'orderby' => 'name',
        'order'   => 'ASC'
    );

    $cats = get_categories($args);

    foreach($cats as $cat){
        $image = carbon_get_term_meta( $cat->term_id, 'crb_box_image' );
        ?>
        <div class="column is-4-desktop">
          <div class="hero" style="background-image: url('<?php echo wp_get_attachment_url($image); ?>'); background-size: cover;background-position: center center; " >
              <div class="box-wrap">
                  <div class="box-ratio">
                      <div class="box-content">
                          <div class="box-elements is-flex button-border">
                              <a href="<?php echo get_term_link($cat) ?>" class="button is-normal is-borderless is-radiusless btn-color-medium-light"><?php echo $cat->cat_name;?></a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    <?php }
  ?>

  </div>
  </section>
  <?php
    $archive_page = get_page_by_path('events-corporate/events-designs');
    global $post;
    $post = $archive_page;
    setup_postdata( $post );
    the_content();
?>
  </div>

  {!! get_the_posts_navigation() !!}
@endsection
