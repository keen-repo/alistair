@php

  $order_id = $_GET['id'];
  $order = wc_get_order( $order_id );

  $get_pickup_date = get_post_meta( $order_id, 'pickup_date', $unique = false );

  $currency = $order->get_currency();
  $order_total = $order->get_total();

  $currency_symbol = get_woocommerce_currency_symbol( $currency );

  //clear the cart
  global $woocommerce;
  $woocommerce->cart->empty_cart();

@endphp
<div class="column is-three-fifths has-text-centered">

  <p class="step-thankyou">THANK YOU FOR YOUR ORDER!</p>
  <span class="content-thankyou">
    <p>Please make sure that you have <strong>{!! $currency_symbol !!}{{ $order_total }}</strong> with you on the <strong>{{ date('d/m/Y', strtotime($get_pickup_date[0])) }}</strong>.</p>
    Our drivers don’t carry a lot of spare change so having the exact amount would be greatly appreciated.
  </span>
  <div class="wp-block-button aligncenter is-style-squared">
    <a class="wp-block-button__link has-background" href="<?php echo get_permalink(get_page_by_path("shop")) ?>">BACK TO SHOP</a>
  </div>

</div>
