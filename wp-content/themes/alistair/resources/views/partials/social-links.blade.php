<ul class="social-links level-item">
  <li><a href="{{ carbon_get_theme_option('als_facebook') }}" class="icon-facebook" target="_blank"></a></li>
  <li><a href="{{ carbon_get_theme_option('als_pinterest') }}" class="icon-pinterest" target="_blank"></a></li>
  <li><a href="{{ carbon_get_theme_option('als_instagram') }}" class="icon-instagram" target="_blank"></a></li>
  <li><a href="{{ carbon_get_theme_option('als_linkedin') }}" class="icon-linkedin" target="_blank"></a></li>
  <li><a href="{{ carbon_get_theme_option('als_youtube') }}" class="icon-youtube" target="_blank"></a></li>
</ul>
