<article @php post_class() @endphp>
  <?php
    $page_ID = get_page_by_path('careers')->ID;
    $header_image_ID = get_post_meta( $page_ID, '_crb_career_header', true );
    $job_position = carbon_get_the_post_meta( 'job_position' );
    $job_type = carbon_get_the_post_meta( 'job_type' );
    $job_description = carbon_get_the_post_meta( 'job_description' );
    $job_responsibilities = carbon_get_the_post_meta( 'job_responsibilities' );
    $job_skills_experience = carbon_get_the_post_meta( 'job_skills_experience' );

    global $form_submitted;
  ?>
  <header class="has-padding-top-100">
    <h1 id="entry-title" class="has-margin-20 is-size-4 montserrat entry-title is-uppercase has-text-weight-semibold has-text-centered"><?php echo $job_position ?></h1>
    <div class="is-flex" style="align-items: center; justify-content: center;">
      <img class="clock" src="@asset('images/Clock.png')" alt="clock" style="width: auto !important; height: 20px; margin-right: 5px;">
      <p class="has-text-centered job-type has-text-weight-bold is-marginless"><?php echo $job_type ?></p>
    </div>
  </header>
  <div class="entry-content">
    <p class="has-text-centered has-margin-50 has-text-weight-normal"><?php echo $job_description ?></p>
    <div class="columns has-padding-top-50 has-padding-bottom-100 ">
      <div class="column has-padding-right-100 border-right">
        <p class="sub-title has-text-weight-bold has-margin-bottom-50">RESPONSIBILITIES</p>
        <p><?php echo $job_responsibilities ?></p>
      </div>
      <div class="column has-padding-left-100">
        <p class="sub-title has-text-weight-bold has-margin-bottom-50">SKILLS AND EXPERIENCE</p>
        <p><?php echo $job_skills_experience ?></p>
      </div>
    </div>
    <div class="has-text-centered">
    <a data-job-position="<?php echo $job_position ?>" class="apply button is-normal is-radiusless btn-color-primary">APPLY NOW</a>
    </div>
    <div class="has-text-centered">
    <p><?php echo do_shortcode('[Sassy_Social_Share url="'.get_permalink().'" title="SHARE ON:"]'); ?></p>
    </div>
    @php the_content() @endphp
  </div>

@php
  $form_shortcode = get_post_meta( $page_ID, '_crb_apply_form', true );
  $var = do_shortcode($form_shortcode);
@endphp

@include('partials/modal', ['title' => 'Application form', 'content' => $var, 'active' => $form_submitted])
  <footer>
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
  </footer>
</article>
