@if (is_page('cart') || is_product())
  @include('partials.tips-and-tricks')
@endif

@include('partials.subscribe')

<div class="footer-wrap">
  <footer class="footer">
      <div class="container">
        <div class="columns">
            <div class="column is-one-quarter-widescreen">
                <figure class="image is-152w">
                  <a href="{{ bloginfo('url') }}" class="logo is-inline-block">
                    <img src="@asset('images/alistair-logo.png')" alt="{{ __('Alistair Floral Design') }}">
                  </a>
                </figure>
                <div class="content">
                    @php echo carbon_get_theme_option( 'als_contact_address' ); @endphp<br>
                    {{ _e('Phone', 'als-front') }}: @php echo carbon_get_theme_option( 'als_contact_phone' ); @endphp<br>
                    {{ _e('Mobile', 'als-front') }}: @php echo carbon_get_theme_option( 'als_contact_mobile' ); @endphp<br>
                    {{ _e('Email', 'als-front') }}: <a href="mailto:@php echo carbon_get_theme_option( 'als_contact_email' ); @endphp">@php echo carbon_get_theme_option( 'als_contact_email' ); @endphp</a>
                </div>
                @include('partials.social-links')

            </div>
            <div class="column">
              <nav class="navbar is-flex" role="navigation" aria-label="main navigation">
                  @if (has_nav_menu('footer_navigation'))
                    {!! wp_nav_menu($footermenu) !!}
                  @endif
              </nav>
            </div>
        </div>
      </div>
  </footer>

  <script data-ad-client="ca-pub-5094552897810954" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

  @include('partials.copyright')
</div>

