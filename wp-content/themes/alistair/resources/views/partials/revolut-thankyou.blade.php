<div class="column is-three-fifths has-text-centered">
  <p class="step-thankyou">THANK YOU FOR YOUR CONFIRMATION!</p>
  <span class="content-thankyou">
    <p>We will check the payment and proceed with the order, if we don’t manage to find your payment we will contact you so make sure not to delete the transaction from your revolut account.</p>
  </span>
</div>
