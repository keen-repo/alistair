<div class="step-item">
  <div class="step-details">
    <p class="step-title">@php _e( 'Enter Delivery Details', 'als-front' ) @endphp</p>
  </div>
  <div class="step-marker">
    <span class="icon">
      <i class="icon-check"></i>
      <i class="icon-circle"></i>
    </span>
  </div>
</div>
<div class="step-item">
  <div class="step-details">
    <p class="step-title">@php _e( 'Message Card', 'als-front' ) @endphp</p>
  </div>
  <div class="step-marker">
    <span class="icon">
      <i class="icon-check"></i>
      <i class="icon-circle"></i>
    </span>
  </div>
</div>
<div class="step-item">
  <div class="step-details">
    <p class="step-title">@php _e( 'Confirm Order', 'als-front' ) @endphp</p>
  </div>
  <div class="step-marker">
      <span class="icon">
        <i class="icon-check"></i>
        <i class="icon-circle"></i>
      </span>
  </div>
</div>
<div class="step-item">
  <div class="step-details">
    <p class="step-title">@php _e( 'Enter Payment Details', 'als-front' ) @endphp</p>
  </div>
  <div class="step-marker">
      <span class="icon">
        <i class="icon-check"></i>
        <i class="icon-circle"></i>
      </span>
  </div>
</div>
