@php
    if($active) {
      $modal_class = "is-active";
    }else {
      $modal_class = " ";
    }
@endphp

<div id="modal" class="modal {{ $modal_class }}">
  <div id="modal-background" class="modal-background"></div>
  <div id="modal-content" class="modal-content">
    <div class="has-background-white has-padding-50">
        <h1 class="has-text-centered">{{ $title }}</h1>
        <div>{!! $content !!}</div>
    </div>
  </div>
  <button id="modal-close" class="modal-close is-large" aria-label="close"></button>
</div>