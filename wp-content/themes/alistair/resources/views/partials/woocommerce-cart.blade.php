
<div>
<h3>Order Details</h3>
    <div id="order-details-wrap">
    <?php
        global $woocommerce;

        // Get cart items
        $items = $woocommerce->cart->get_cart();

        foreach($items as $item => $values) {
        $product =  wc_get_product( $values['data']->get_id() );

        // check if variation or single product
        if($product->get_type() == 'variation'){
            $getProductDetail = wc_get_product( $values['variation_id'] );
            $price = get_post_meta($values['variation_id'] , '_price', true);
        } else {
            $getProductDetail = wc_get_product( $values['product_id'] );
            $price = get_post_meta($values['product_id'] , '_price', true);
        }

        $product_image = $getProductDetail->get_image('thumbnail'); // accepts 2 arguments ( size, attr )
        $line_total = wc_price( $price * $values['quantity'] );
        ?>

        <div class="order-details columns is-marginless is-multiline is-mobile">
            <div class="column is-12-mobile is-paddingless item-image"><?= $product_image; ?></div>
            <div class="column is-two-thirds-mobile is-paddingless item-details">
            <div class="is-flex">
                x<?= $values['quantity']; ?> <?= $product->get_title(); ?>
            </div>
            </div>
            <div class="column is-one-third-mobile is-one-fifth-tablet is-paddingless has-text-right item-price">
            <div class="is-flex"><?= $line_total; ?></div>
            </div>
        </div>

    <?php
    }

    ?>
    </div>
    <div class="cart-total-wrap columns is-marginless is-mobile">
    <div class="column is-paddingless">TOTAL: </div>
    <div class="column is-one-third is-paddingless has-text-right">
        <?= get_woocommerce_currency_symbol(); ?><span id="total-amount"><?= $woocommerce->cart->total; ?></span>
    </div>
    </div>
    <div class="addons woocommerce">
    {{ do_action('load_gift_addons') }}
    </div>
    <div class="payment-methods">
    <div class="columns is-marginless">
        <div class="column is-one-third is-paddingless">
            Payment method
        </div>
        <div class="column is-paddingless">
            <div class="field">
            <div class="control">
                <label class="radio is-flex">
                <input type="radio" name="payment_method" value="paypal" >
                <img src="@asset('images/paypal-logo.png')" >
                </label>
                <br>
                <label class="radio is-flex">
                <input type="radio" name="payment_method" value="phone_pay">
                PAY VIA PHONE
                </label>
                <br>
                {{-- <label class="radio is-flex">
                <input type="radio" name="payment_method" value="revolut_pay">
                <img src="@asset('images/revolut-logo-small.png')" >
                </label>
                <br> --}}
                <label class="radio is-flex">
                <input id="for_pickup" type="radio" name="payment_method" value="cod">
                <span id="pickup_label">PAY ON DELIVERY</span>
                </label>
                <br>
                <label id='pay_on_collection_radio' class="radio is-flex">
                    <input id="for_pay_on_collection" type="radio" name="payment_method" value="pay_on_collection">
                    PAY ON COLLECTION
                </label>
            </div>
            </div>
        </div>
    </div>
    </div>
</div>
