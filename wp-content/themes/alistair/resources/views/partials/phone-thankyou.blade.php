@php

  $order_id = $_GET['id'];
  $order = wc_get_order( $order_id );

  $order_data = $order->get_data(); // The Order data

  $order_billing_phone = $order_data['billing']['phone'];

  //clear the cart
  global $woocommerce;
  $woocommerce->cart->empty_cart();

@endphp
<div class="column is-three-fifths has-text-centered">

  <p class="step-thankyou">THANK YOU FOR YOUR ORDER!</p>
  <span class="content-thankyou">
    <p>We will call you back within 24 hours on this number to make payment:</p>
    <br>
    <strong style="font-size: 26px"> {{ $order_billing_phone }} </strong>
  </span>
  <div class="wp-block-button aligncenter is-style-squared">
    <a class="wp-block-button__link has-background" href="<?php echo get_permalink(get_page_by_path("shop")) ?>">BACK TO SHOP</a>
  </div>

</div>
