@php

  $order_id = $_GET['id'];
  $order = wc_get_order( $order_id );

  $currency = $order->get_currency();
  $order_total = $order->get_total();

  $currency_symbol = get_woocommerce_currency_symbol( $currency );

  //clear the cart
  global $woocommerce;
  $woocommerce->cart->empty_cart();

@endphp

<div id="rev_instructions" class="column">

<div class="columns">
  <div class="column">

    <p class="step-thankyou"><img src="@asset('images/revolut-logo-big.png')" ></p>

    <div class="columns is-centered is-mobile column">
        <div class="column is-half-desktop">
            <span class="content-thankyou">
              <p>Follow these 5 steps in your Revolut app to make payment.</p>
              Please note that the following step by step instructions have been created on an ios device,
              you have an android device certain buttons locations may be different.
            </span>
        </div>
    </div>

    <br><br>

    <div class="columns">
      <div class="column is-one-third">
        <p class="revolut-step-hearer">STEP 1</p>
        <img src="@asset('images/revolut-step-1.png')" >

        <p>Click the payments tab at the bottom of the screen and click on the add contact option at the top marked with a red circle.</p>
      </div>
      <div class="column is-one-third">
        <p class="revolut-step-hearer">STEP 2</p>
        <img src="@asset('images/revolut-step-2.png')" >
        <p>Under "Name" please write Alistair Floral Design and under "Phone or Email" please write <strong>77083404</strong></p>
      </div>
      <div class="column is-one-third">
          <p class="revolut-step-hearer">STEP 3</p>
          <img src="@asset('images/revolut-step-3.png')" >
          <p>At the top click on the search icon (magnifying glass) and write Alistair Floral Design. Click on the contact created.</p>
      </div>
    </div>

    <div class="columns">
        <div class="column">
          <p class="revolut-step-hearer">STEP 4</p>
          <img src="@asset('images/revolut-step-4.png')" >
          <p>Click on the "send" option</p>
        </div>
        <div class="column is-two-thirds">
          <p class="revolut-step-hearer">STEP 5</p>

          <div class="columns">
            <div class="column is-half">
              <img src="@asset('images/revolut-step-5.png')" >
            </div>

            <div class="column is-half">
              <p>In the amount section please write in the amount of <strong>{!! $currency_symbol !!}{{ $order_total }}</strong></p>
              <p>and in the note section please write the Order number <strong>#{{ $order_id }}</strong></p>
              <p>and click send.</p>
              <p>We have created a demonstration of a bouquet which costs €30 and the order number was 12345.</p>
              <p>Once payment has been made please click button below</p>
    
              <a href="javascript:;" id="revolut_button" class="button is-normal is-radiusless btn-color-primary" style="width: 320px;" >I HAVE MADE PAYMENT</a>
            </div>
          </div>
        </div>
      </div>

  </div>
</div>

</div> {{-- End of rev_instrustions --}}

<div id="rev_thankyou" style="text-align: -webkit-center;">
  @include('partials.revolut-thankyou')
</div>
