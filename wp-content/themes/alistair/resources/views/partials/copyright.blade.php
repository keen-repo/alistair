<div class="copyright-foot">
    <div class="container">
        <div class="columns is-marginless is-multiline">
            <div class="column is-half-desktop is-12">
              COPYRIGHT © {{ date('Y') }} ALISTAIR <span>|</span> <a href="{{ get_permalink(26) }}">PRIVACY POLICY </a> <span>|</span> <a href="{{ get_permalink(28) }}">TERMS AND CONDITIONS</a>
            </div>
            <div class="column credit">
                WEBSITE DESIGNED AND DEVELOPED BY <a href="https://www.keen.com.mt/" target="_blank">KEEN LTD</a> MALTA
            </div>
        </div>
    </div>
</div>

