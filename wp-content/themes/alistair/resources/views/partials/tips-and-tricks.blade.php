<section class="tips_and_tricks">
  <div class="columns is-radiusless is-gapless">
    <div class="column is-half has-text-right with-bg-image" style="background-image: url('@asset('images/tips-tricks-image.jpg')');">
      <div class="tip-box tip-right">
        <div class="content">
            <h1 class="title">Tips & Tricks</h1>
            <h3 class="subtitle">How your flowers bloom long and happily ever after!</h3>
            <a href="{{ get_site_url() }}/flower-care" class="button is-normal is-radiusless btn-color-primary">
              FLOWER CARE
            </a>
        </div>
      </div>
    </div>
    <div class="column is-half">
      <div class="tip-box tip-left">
        <ul class="delivery-links">
          <li>
            <article class="media is-flex">
              <figure class="media-left image is-64x64 is-flex">
                <img src="@asset('images/delivery-icon.svg')">
              </figure>
              <div class="media-content">
                <div class="content">
                  <span class="tip-text">Free Delivery in Malta</span>
                  <p>Right to the door step! Deliveries to Gozo upon request.</p>
                </div>
              </div>
            </article>
          </li>
          <li>
            <article class="media is-flex">
              <figure class="media-left image is-64x64">
                <img src="@asset('images/flower-icon.svg')">
              </figure>
              <div class="media-content">
                <div class="content">
                  <span class="tip-text">Outstanding Quality & Innovative Floral Design</span>
                  <p>Unforgettable moments deserve our ‘extra mile’.</p>
                </div>
              </div>
            </article>
          </li>
          <li>
            <article class="media is-flex">
              <figure class="media-left image is-64x64">
                <img src="@asset('images/maltese-cross-icon.svg')">
              </figure>
              <div class="media-content">
                <div class="content">
                  <span class="tip-text">Local Craftsmanship & International Aspiration</span>
                  <p>20 years of experience assure your peace of mind.</p>
                </div>
              </div>
            </article>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>



