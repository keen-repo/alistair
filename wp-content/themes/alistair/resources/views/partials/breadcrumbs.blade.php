
<div class="container container-breadcrumbs is-invisible-mobile">

  <div class="yoast-breadcrumbs">
    @php
      if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
        }
    @endphp
  </div>
</div>
