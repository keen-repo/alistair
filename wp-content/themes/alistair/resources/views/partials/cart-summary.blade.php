<a class="cart-summary is-flex" href="{{ wc_get_cart_url() }}" title="{{ __( 'View your shopping cart' ) }}">
  <img src="@asset('images/cart-icon.svg')" alt="View Cart" />
  <div class="cart-text">
      <div class="is-uppercase is-hidden-touch">{{ __('Your Cart', 'als-front') }}</div>

      <!--// Use in conjunction with https://gist.github.com/woogists/c0a86397015b88f4ca722782a724ff6c -->
      <div class="cart-total">
        <a class="cart-customlocation" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>" data-cart-number="<?php echo WC()->cart->get_cart_contents_count(); ?>">
          @php echo sprintf ( _n( '(%d)', '(%d)', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); @endphp
          - @php echo WC()->cart->get_cart_total(); @endphp
        </a>
      </div>
  </div>
</a>




