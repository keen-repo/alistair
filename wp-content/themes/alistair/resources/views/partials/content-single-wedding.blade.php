<article @php post_class() @endphp>
<?php
  $page_ID = get_page_by_path('weddings/our-weddings')->ID;
  $header_image_ID = get_post_meta( $page_ID, '_crb_wedding_header', true );
?>

<header>
    <section class="hero is-medium alignfull" style="background-image:url('<?php echo wp_get_attachment_url($header_image_ID); ?>'); background-size: cover;background-position: center center; ">
        <div class="hero-body">
            <div class="container">
                <h1 class="hero-title">Our weddings</h1>
            </div>
        </div>
    </section>
    <p id="entry-title" class="entry-title is-uppercase has-text-weight-semibold has-text-centered has-padding-50 custom-padding-4">{!! get_the_title() !!}</p>
 </header>


  <div class="entry-content">
    <section class="feautered_boxes has-text-semibold">

      <?php
        $images = array();
        $images = carbon_get_the_post_meta( 'wedding_images' );
        $index = 1;
      ?>

      <div id="easyPaginate" class="columns is-marginless is-centered is-multiline">
          <?php foreach ($images as $image_id) {
          ?>
              <li class="column is-one-quarter">
                  <div class="hero" onclick="openModal();currentSlide(<?php echo $index; $index++; ?>)" style="background-image: url('<?php echo wp_get_attachment_url($image_id); ?>'); background-size: cover;background-position: center center; "  >
                      <div class="box-wrap">
                          <div class="box-ratio">
                              <div class="box-content">

                              </div>
                          </div>
                      </div>
                  </div>
              </li>

          <?php } ?>
      </div>

      <div class="modal" id="myModal">
          <div class="modal-content is-vcentered">
              <?php foreach ($images as $image_id) { ?>
                <div class="item-slide">
                  <img src="<?php echo wp_get_attachment_url($image_id) ?>">
                </div>
              <?php } ?>
          </div>
          <a class="previous" onclick="plusSlides(-1)">&#10094;</a>
          <a class="nextus" onclick="plusSlides(1)">&#10095;</a>
        <button onclick="closeModal()" class="modal-close is-large" aria-label="close"></button>
      </div>

      <script>

        function openModal() {
          document.getElementById('myModal').style.display = "block";
        }

        function closeModal() {
          document.getElementById('myModal').style.display = "none";
        }

        var slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
          showSlides(slideIndex += n);
        }

        function currentSlide(n) {
          showSlides(slideIndex = n);
        }

        function showSlides(n) {
          var i;
          var slides = document.getElementsByClassName("item-slide");
          var captionText = document.getElementById("caption");
          if (n > slides.length) {slideIndex = 1}
          if (n < 1) {slideIndex = slides.length}
          for (i = 0; i < slides.length; i++) {
              slides[i].style.display = "none";
          }
          slides[slideIndex-1].style.display = "block";
        }

        </script>

        <style>
            .item-slide {
              box-sizing: border-box;
            }

            /* The Modal (background) */
            .modal {
              display: none;
              position: fixed;
              z-index: 1000;
              left: 0;
              top: 0;
              width: 100%;
              height: 100%;
              background-color: rgba(0,0,0,0.5);
            }

            /* Modal Content */
            .modal-content {
              position: relative;
              background-color: rgba(0,0,0,0);
              width: 100%;
              max-width: 100%;
              max-height: 100%;
              overflow: hidden;
              text-align: center;
              top: 50%;
              transform: translateY(-50%);
              margin: 0 auto;
            }

            .modal-close {
              top: 80px;
            }

            .item-slide{
              display: none;
            }

            /* Next & previous buttons */
            .previous,
            .nextus {
              cursor: pointer;
              position: absolute;
              top: 50%;
              width: auto;
              padding: 16px;
              margin-top: -50px;
              color: white;
              font-weight: bold;
              font-size: 20px;
              transition: 0.6s ease;
              border-radius: 0 3px 3px 0;
              user-select: none;
              -webkit-user-select: none;
            }

            /* Position the "next button" to the right */
            .nextus {
              right: 0;
              border-radius: 3px 0 0 3px;
            }


            /* Number text (1/3 etc) */
            .numbertext {
              color: #f2f2f2;
              font-size: 12px;
              padding: 8px 12px;
              position: absolute;
              right: 0;
              top: 0;
            }

            .item-slide img {
              max-width: 100%;
              max-height: 100%;
            }

            .caption-container {
              color: white;
            }

            .column img{
              margin-top: 20px;
            }  
        </style>

      </section>

    @php the_content() @endphp

    <h2 class="section-title has-text-centered ">
      Our Weddings
        <span>
        ~ EXPLORE MORE WEDDINGS ~
        </span>
    </h2>

    @php
      $other_weddings = get_posts(array(
        'numberposts' => 6,
        'post_type'        => 'wedding',
        'orderby'          => 'rand',
        'post__not_in'     => array(get_the_ID()),
      ));

    @endphp

    <section class="sliding_text feautered_boxes">
        <div class=" is-centered">
            <div class="sliding-text-slick">
                <?php foreach ($other_weddings as $wedding) {
                    $image = carbon_get_post_meta( $wedding->ID, "wedding_preview_image" );
                ?>
                    <div class="column is-4">
                        <div class="hero" style="background-image: url('<?= wp_get_attachment_url($image[0]); ?>'); background-size: cover;background-position: center center; " onclick="window.location='<?php echo get_permalink($wedding->ID); ?>';" >
                            <div class="box-wrap">
                                <div class="box-ratio">
                                    <div class="box-content">
                                        <div class="box-elements is-flex button-border">
                                            <a href="<?php echo get_permalink($wedding->ID) ?>" class="button is-normal is-borderless is-radiusless btn-color-medium-light"><?php echo $wedding->post_title; ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
  </div>
  <footer>
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
  </footer>
  @php comments_template('/partials/comments.blade.php') @endphp
</article>
