<?php
    $bg_image = carbon_get_theme_option('bg_image');
    $logo = carbon_get_theme_option('logo');
    $buttons = carbon_get_theme_option('crb_buttons');

?>

<div class="landing-page">
<div class="has-page-height full-screen-container" style="background-image: url(<?php echo wp_get_attachment_url($bg_image) ?>); background-size: cover;background-position: center center; ">
    <div class="has-text-centered logo-container">
        <img class="is-hidden-touch" src="<?php echo wp_get_attachment_url($logo) ?>" alt="">
    </div>

    <div class="columns column is-centered is-mobile is-vcentered is-paddingless is-marginless">
    <div class="columns column is-two-thirds-desktop is-four-fifths-mobile" style="max-width: 823px;">
        <?php
            foreach($buttons as $button){ ?>
            
            <div class="column has-padding-20">
                <a class="button has-text-weight-semibold" href="<?php echo $button['crb_btn_url'] ?>">
                    <p><?php echo $button['crb_btn_text'] ?></p>
                </a>
            </div>
        <?php }
        ?>
    </div>
    </div>

    <div class="has-text-centered bottom-button">
        <img id="enter-website-btn" class="has-cursor-pointer" src="@asset('images/icon-arrow.png')" alt="">
    </div>
</div>
</div>
