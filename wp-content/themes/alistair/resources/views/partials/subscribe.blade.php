<section style="background-image: url('@asset('images/newsletter-bg.jpg')'); background-size: cover;background-position: center center;">
  <div class="subscribe-overlay">
  <div class="container has-text-centered">
    <div class="subscribe">
        <p class="title">SUBSCRIBE TO OUR NEWSLETTER</p>
        <h2 class="subtitle is-6">
            Subscribe now and benefit from our latest promotions, new product updates and news on what's trending!
        </h2>
        <div class="columns is-marginless is-paddingless">
            <div class="column is-two-thirds is-offset-2">
              <div class="field has-addons has-addons-centered">
                @php echo do_shortcode( '[formidable id=6]' ) @endphp
              </div>
            </div>
        </div>
      </div>
    </div>

  <div>
</section>
