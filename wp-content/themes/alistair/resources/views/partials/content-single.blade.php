<section class="blog-post alignfull has-bg-green-really-light">
<article @php post_class() @endphp >
  <header>
    <img class="blog-featured-image" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
    <p class="is-marginless has-text-centered has-padding-50 has-text-weight-semibold	"><?php if(get_post_type()=='blog'){ ?>{{ get_the_date() }}<?php } ?></p>
    <h1 class="is-marginless is-size-2 has-text-centered has-text-weight-bold">{!! get_the_title() !!}</h1>
  </header>
  <div class="entry-content columns is-centered has-padding-50 has-padding-30-mobile ">
    <div class="column is-two-thirds ">
      <div class="has-padding-bottom-100 has-border-bottom-width-2 border-bottom">
        @php the_content() @endphp
      </div>
      <div class="is-size-7">
        <?php
          $categories_terms = wp_get_post_terms(get_post()->ID,get_post_taxonomies(get_post()->ID)[0]);
        ?>
        <p class="is-pulled-left has-text-weight-semibold has-margin-right-10">CATEGORIES: <?php foreach($categories_terms as $category){ echo $category->name . ' '; } ?></p>
      </div>
      <div class="is-size-7">
        {{-- <ul class="social-links level-item is-pulled-right">
+           <li><a href="{{ carbon_get_theme_option('als_facebook') }}" class="icon-facebook" target="_blank"></a></li>
+           <li><a href="{{ carbon_get_theme_option('als_pinterest') }}" class="icon-pinterest" target="_blank"></a></li>
+           <li><a href="{{ carbon_get_theme_option('als_instagram') }}" class="icon-instagram" target="_blank"></a></li>
+           <li><a href="mailto:{{ carbon_get_theme_option('als_contact_email') }}" class="icon-mail" target="_blank"></a></li>
+         </ul>
        --}}
+        <?php echo do_shortcode('[Sassy_Social_Share url="'.get_permalink().'" title="SHARE ON:"]'); ?>
      </div>
    </div>
  </div>
  <footer>
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
  </footer>
</article>

<div class="comments has-margin-left-100-desktop has-margin-right-100-desktop has-margin-right-50 has-margin-left-50">
<?php comments_template( '', true ); ?>
</div>

<?php if(get_post_type()=='blog'){ ?>
<div>
  <div class="">
    <h1 class="has-text-centered is-marginless">Articles you might like as well</h1>
    <p class="has-text-centered">~ PASSION FOR FLOWERS ~</p>
  </div>

  <div class=" is-centered has-padding-100 has-padding-50-mobile">
    <div id="related-blog-slider"  class="">
      <?php
        $args = array(
          'numberposts' => -1,
          'offset' => 0,
          'category' => 0,
          'orderby' => 'post_date',
          'order' => 'DESC',
          'include' => '',
          'exclude' => '',
          'meta_key' => '',
          'meta_value' =>'',
          'post_type' => 'blog',
          'post_status' => 'publish',
          'suppress_filters' => true
        );

        $recent_posts = wp_get_recent_posts( $args, ARRAY_A );

        foreach($recent_posts as $post){
          if($post['ID'] !== get_the_ID()) {
            $image = get_post_meta( $post['ID'], '_blog_preview_image', true );?>
          <li class="is-one-third">
            <a href="<?php echo get_permalink($post['ID']) ?>"  class="featured-image" style="background-image: url(<?php echo wp_get_attachment_url($image) ?>); background-size: cover;background-position: center center; ">
              <div class="button">
                READ MORE
              </div>
            </a>
            <p class="date is-marginless has-text-centered is-size-7 has-padding-30 has-text-weight-semibold	"><?php echo date('j.n.Y',strtotime($post['post_date'])) ?></p>
            <p class="montserrat is-marginless is-size-6 has-text-centered has-text-weight-bold"><?php echo $post['post_title'] ?></p>
            <p class="has-margin-top-30 has-max-height-100 is-size-7 has-text-centered has-text-weight-normal"><?php echo wp_trim_words(wp_strip_all_tags($post['post_content']),15,NULL)  ?></p>
        </li>
        <?php } 
      }
      ?>
    </div>
  </div>

  <div class="has-text-centered has-padding-bottom-100">
    <a href="/blog" class="button is-normal is-radiusless btn-color-primary">BACK TO BLOG</a>
  </div>
</div>

<?php } ?>

</section>

