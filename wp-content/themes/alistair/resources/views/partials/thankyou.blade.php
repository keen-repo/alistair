@php

  $order_id = $_GET['id'];
  $order_id = str_replace('#', '', $order_id);

  $order = wc_get_order( $order_id );

  $payment_method = $order->get_payment_method(); // Get the payment method ID
  $payment_title = $order->get_payment_method_title(); // Get the payment method title

@endphp
{{-- Include correct template based on payment --}}
@if($payment_method == 'cod')
  @include('partials.pod-thankyou')
@endif
@if($payment_method == 'phone_pay')
  @include('partials.phone-thankyou')
@endif
@if($payment_method == 'revolut_pay')
  @include('partials.revolut-instructions')
@endif
@if($payment_method == 'paypal')
  @include('partials.paypal-thankyou')
@endif
{{--@if($payment_method == 'pay_on_collection')
  @include('partials.poc-thankyou')
@endif --}}
