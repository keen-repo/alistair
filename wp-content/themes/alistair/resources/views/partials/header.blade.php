<header id="site-header" class="site-header">
  <div class="menu-container container">
    <div class="columns is-marginless quick-links-menu-wrap">
      <div class="column is-one-fifth">
        <a href="{{ bloginfo('url') }}" class="logo is-inline-block">
          <img src="@asset('images/alistair-logo.png')" alt="{{ __('Alistair Floral Design') }}">
        </a>
      </div>
      <div class="column">
        <div class="quick-links-and-cart is-flex">
          <div class="search-container" style="display: flex">
            <a href="javascript:;" class="icon-search level-item"></a>
            <div class="search-input-toggle">
              @php
                echo do_shortcode("[wcas-search-form]");
              @endphp
            </div>
          </div>
          <ul class="user-menu level-item">
              <li>
                @if(is_user_logged_in())
                  @php $nonce = wp_create_nonce(); @endphp
                  <a href="{{ wp_logout_url( home_url() ) }}">{{ __('Log Out', 'als-front') }}</a>
                @else
                  <a href="{{ get_site_url().'/login' }}">{{ __('Login', 'als-front') }}</a>
                @endif
              </li>
              <li><a href="{{ get_site_url().'/register' }}">{{ __('Register', 'als-front') }}</a></li>
          </ul>
          @include('partials.social-links')
          @include('partials.cart-summary')
        </div>
        <div class="top-menu-wrap">
            <nav class="navbar is-flex" role="navigation" aria-label="main navigation">
                @if (has_nav_menu('primary_navigation'))
                  {!! wp_nav_menu($primarymenu) !!}
                @endif
            </nav>
        </div>
      </div>
    </div>
  </div>
  <div class="secondary-menu">
    <div class="menu-container container">
      @if (has_nav_menu('secondary_navigation'))
        @if ((strpos($_SERVER['REQUEST_URI'], 'weddings') !== false))
          {!! wp_nav_menu($weddingsmenu) !!}
        @endif
        @if ((strpos($_SERVER['REQUEST_URI'], 'events-corporate') !== false) || (strpos($_SERVER['REQUEST_URI'], 'event-category') !== false))
          {!! wp_nav_menu($eventscorporatemenu) !!}
        @endif
        @if (strpos($_SERVER['REQUEST_URI'], 'shop') !== false)
          {!! wp_nav_menu($shopmenu) !!}
        @endif
        @if ((strpos($_SERVER['REQUEST_URI'], 'product') !== false) || (strpos($_SERVER['REQUEST_URI'], 'product-category') !== false))
          {!! wp_nav_menu($shopmenu) !!}
        @endif
        @if (strpos($_SERVER['REQUEST_URI'], 'flower-care') !== false)
          {!! wp_nav_menu($shopmenu) !!}
        @endif
        @if (strpos($_SERVER['REQUEST_URI'], 'flower-subscriptions') !== false)
          {!! wp_nav_menu($shopmenu) !!}
        @endif
        @if (strpos($_SERVER['REQUEST_URI'], 'cart') !== false)
          {!! wp_nav_menu($shopmenu) !!}
        @endif
      @endif
    </div>
  </div>
  <!-- Facebook Pixel Code -->
  <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '2010594615904397'); 
    fbq('track', 'PageView');
    </script>
    <noscript>
    <img height="1" width="1" 
    src="https://www.facebook.com/tr?id=2010594615904397&ev=PageView
    &noscript=1"/>
  </noscript>
  <!-- End Facebook Pixel Code -->  
</header>


