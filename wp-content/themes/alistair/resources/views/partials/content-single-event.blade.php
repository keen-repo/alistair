<article @php post_class() @endphp>
<?php 
  $page_ID = get_page_by_path('events-corporate/events-designs')->ID;
  $category = get_the_terms(get_post(), 'event_category')[0];

  $header_image_ID = get_term_meta( $category->term_id, '_crb_banner_image', true );
?>

  <header>
    <section class="hero is-medium alignfull" style="background-image:url('<?php echo wp_get_attachment_url($header_image_ID); ?>'); background-size: cover;background-position: center center; ">
        <div class="hero-body">
            <div class="container">
                <h1 class="hero-title">{{ html_entity_decode($category->name, ENT_COMPAT, 'UTF-8') }}</h1>
            </div>
        </div>
    </section>

    // <p id="entry-title" class="entry-title is-uppercase has-text-weight-semibold has-text-centered has-padding-50 custom-padding-4">{{ html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8') }}</p>
  </header>
  <div class="entry-content">
    <section class="feautered_boxes has-text-semibold">

      <?php 
        $images = array();
        $images = carbon_get_the_post_meta( 'event_images' );
      ?>

      <div id="designPaginate" class="columns is-marginless is-centered is-multiline">
          <?php foreach ($images as $image_id) {
          ?>
              <li class="column is-one-quarter">
                  <div class="hero" style="background-image: url('<?php echo wp_get_attachment_url($image_id); ?>'); background-size: cover;background-position: center center; "  >
                      <div class="box-wrap">
                          <div class="box-ratio">
                              <div class="box-content">
                              
                              </div>
                          </div>
                      </div>
                  </div>
          </li>
          <?php } ?>
      </div>

    </section>
      
    @php the_content() @endphp
  </div>
  <footer>
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
  </footer>
  @php comments_template('/partials/comments.blade.php') @endphp
</article>
