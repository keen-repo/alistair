<?php
/* 
Template Name: Blog Archive
*/
?>

@extends('layouts.app')

@section('content')

<?php 
  $blog_page_ID = get_page_by_path('blog')->ID;
  $header_image_ID = get_post_meta( $blog_page_ID, '_crb_blog_header', true );
?>

<header>
    <section class="hero is-medium alignfull" style="background-image:url('<?php echo wp_get_attachment_url($header_image_ID); ?>'); background-size: cover;background-position: center center; ">
        <div class="hero-body">
            <div class="container">
                <h1 class="hero-title">Blog</h1>
            </div>
        </div>
    </section>

 </header>
  
  <div class="blog-archive has-padding-bottom-100">
    <div class="filters-desktop">
      <div class="has-padding-70 columns column is-8 is-offset-2 is-centered is-full is-centered column is-narrow has-text-centered flex-wrap has-text-black">
        <a class="<?php if(!isset($_GET['category'])){ echo "selected"; } ?> column is-narrow is-uppercase is-clickable has-text-weight-semibold flex-item-no-overflow" href="/blog"><p>ALL</p></a>
        <?php 
        $args = array(
            'taxonomy' => 'blog_category'
        );
        $categories = get_categories($args);
          foreach($categories as $category){
            if($category->name !== "Uncategorized"){
              if($category->name !== "Others"){ ?>
                <a class="<?php if(isset($_GET['category'])){ if( $_GET['category'] == $category->slug){ echo "selected"; }} ?> column is-narrow is-uppercase is-clickable has-text-weight-semibold flex-item-no-overflow" href="/blog?category=<?php echo $category->slug ?>"><p><?php echo $category->name ?></p></a>
              <?php } ?>
            <?php 
            }
          }
      
      foreach($categories as $category){
          if($category->name == "Others"){
              ?>
                  <a class="<?php if(isset($_GET['category'])){ if( $_GET['category'] == "others"){ echo "selected"; }} ?> column is-narrow is-uppercase is-clickable has-text-weight-semibold flex-item-no-overflow" href="/blog?category=<?php echo $category->slug ?>"><p><?php echo $category->name ?></p></a>
              <?php
          }
      }
      ?>
      </div>
    </div>

    <div class="filters-mobile">
      <select name="" id="">
        <option <?php if(isset($_GET['category'])){ if( $_GET['category'] == "all"){ echo "selected='selected'"; }} ?> class="column is-narrow is-uppercase is-clickable has-text-weight-semibold flex-item-no-overflow" value="/blog"><p>ALL</p></option>
        <?php 
        $args = array(
            'taxonomy' => 'blog_category'
        );
        $categories = get_categories($args);
          foreach($categories as $category){
            if($category->name !== "Uncategorized"){
              if($category->name !== "Others"){ ?>
                <option <?php if(isset($_GET['category'])){ if( $_GET['category'] == $category->slug){ echo "selected='selected'"; }} ?> class="column is-narrow is-uppercase is-clickable has-text-weight-semibold flex-item-no-overflow" value="/blog?category=<?php echo $category->slug ?>"><p><?php echo $category->name ?></p></option>
              <?php } ?>
            <?php 
            }
          }
      

      foreach($categories as $category){
          if($category->name == "Others"){
              ?>
                  <option <?php if(isset($_GET['category'])){ if( $_GET['category'] == "others"){ echo "selected='selected'"; }} ?> class="column is-narrow is-uppercase is-clickable has-text-weight-semibold flex-item-no-overflow" value="/blog?category=<?php echo $category->slug ?>"><p><?php echo $category->name ?></p></option>
              <?php
          }
      }
      ?>
      </select>
    </div>

    <div class="columns is-centered is-multiline">
      <div id="blogPaginate" class="columns column is-multiline is-three-quarters">
        <?php 
          $category = $_GET['category'];

          $args = array(
            'numberposts' => -1,
            'offset' => 0,
            'category' => 0,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'include' => '',
            'exclude' => '',
            'meta_key' => '',
            'meta_value' =>'',
            'post_type' => 'blog',
            'post_status' => 'publish',
            'suppress_filters' => true
          );
          
          $posts = wp_get_recent_posts( $args, ARRAY_A );

          foreach($posts as $post){
            $terms = get_the_terms($post['ID'], "blog_category");
            $is_category = false;
            if($terms){
                foreach($terms as $term){
                    if($term->slug == $category){
                        $is_category = true;
                    }
                }
            }

            if(!isset($category)){
              $is_category = true;
            }


            if($is_category == true){

            $image = get_post_meta( $post['ID'], '_blog_preview_image', true );
        ?>
        <li class="column is-one-third is-marginless">
            <a href="<?php echo get_permalink($post['ID']) ?>"  class="featured-image" style="background-image: url(<?php echo wp_get_attachment_url($image) ?>); background-size: cover;background-position: center center; ">
              <div class="button">
                READ MORE
              </div>
            </a>
            <p class="date is-marginless has-text-centered is-size-7 has-padding-30 has-text-weight-semibold	"><?php echo date('j.n.Y',strtotime($post['post_date'])) ?></p>
            <p class="montserrat is-marginless is-size-6 has-text-centered has-text-weight-bold"><?php echo $post['post_title'] ?></p>
            <p class="has-margin-top-30 has-max-height-100 is-size-7 has-text-centered has-text-weight-normal"><?php echo wp_trim_words(wp_strip_all_tags($post['post_content']),15,NULL)  ?></p>
        </li>
        <?php }
        } ?>
      </div>
    </div>
  </div>

  <?php
    $archive_page = get_page_by_path('blog');
    global $post;
    $post = $archive_page;
    setup_postdata( $post );
    the_content();
  ?>

@endsection
