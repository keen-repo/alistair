@php
  // Getting the slug for controlle the bredcrubs or other part of the pages
  global $post;
  $post_slug = $post->post_name;
@endphp

<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  @if(is_front_page())
    @include('partials.landing-page')
  @endif
  <body @php body_class() @endphp>
    <div id="from-menu-down">
      @php do_action('get_header') @endphp
      @include('partials.header')
      <div id="wrap-container" class="wrap container" role="document">
        {{-- Taking controlle of the Breadcrumbs on which pages is shown --}}
        @if(is_front_page() OR $post_slug == 'cart' OR $post_slug == 'checkout' OR $post_slug == 'my-dashboard')
        {{-- No Bredcrumbs Please!  --}}
        @else
          @include('partials.breadcrumbs')
        @endif

          <div class="content @if(is_product_category() || is_tax()) @php echo 'columns is-multiline is-marginless' @endphp @endif">
            @if (App\display_sidebar())
              <aside class="sidebar column is-one-quarter">
                @include('partials.sidebar')
              </aside>
            @endif
            <main class="main @if (App\display_sidebar()) column @endif">
              @yield('content')
            </main>
        </div>
      </div>
      @php do_action('get_footer') @endphp
      @include('partials.footer')
      @php wp_footer() @endphp
    </div>
  </body>
</html>
