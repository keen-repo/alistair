@php
/*
Template Name: Feature Archive
*/
@endphp

@extends('layouts.app')

@section('content')

@php
    $selected_category = $_GET['category'];

    $args = array(
        'offset' => 0,
        'orderby' => 'post_date',
        'order' => 'DESC',
        'include' => '',
        'exclude' => '',
        'meta_key' => '',
        'meta_value' =>'',
        'post_type' => 'feature',
        'post_status' => 'publish',
        'suppress_filters' => true,
        'numberposts' => -1,
    );

    $features = wp_get_recent_posts( $args, ARRAY_A );
@endphp

<?php
  $blog_page_ID = get_page_by_path('weddings/features')->ID;
  $header_image_ID = get_post_meta( $blog_page_ID, '_crb_features_header', true );
?>

<header>
    <section class="hero is-medium alignfull" style="background-image:url('<?php echo wp_get_attachment_url($header_image_ID); ?>'); background-size: cover;background-position: center center; ">
        <div class="hero-body">
            <div class="container">
                <h1 class="hero-title is-capitalized"><?php echo get_term_by('slug', $selected_category, 'feature_category')->name ?> Features</h1>
            </div>
        </div>
    </section>

 </header>

<div class="archive-feature has-padding-bottom-100">

    <div class="">
        <div class="has-padding-70 columns column is-8 is-offset-2 is-centered is-full is-centered column is-narrow has-text-centered flex-wrap has-text-black">
        <a class="<?php if(!isset($_GET['category'])){ echo "selected"; } ?> column is-narrow is-uppercase is-clickable has-text-weight-semibold flex-item-no-overflow" href="/weddings/features"><p>ALL</p></a>
        <?php
            $args = array(
                'taxonomy' => 'feature_category'
            );
            $others = "";
            $categories = get_categories($args);
            foreach($categories as $category){
            if($category->name !== "Uncategorized"){
                if($category->name !== "Others"){
                ?>
            <a class="<?php if(isset($_GET['category'])){ if( $_GET['category'] == $category->slug){ echo "selected"; }} ?> column is-narrow is-uppercase is-clickable has-text-weight-semibold flex-item-no-overflow" href="/weddings/features?category=<?php echo $category->slug ?>"><p><?php echo $category->name ?></p></a>
        <?php
            }
            }
            }

            foreach($categories as $category){
                if($category->name == "Others"){
                    ?>
                <a class="<?php if(isset($_GET['category'])){ if( $_GET['category'] == "other"){ echo "selected"; }} ?> column is-narrow is-uppercase is-clickable has-text-weight-semibold flex-item-no-overflow" href="/weddings/features?category=<?php echo $category->slug ?>"><p><?php echo $category->name ?></p></a>
            <?php
                
            }
            }

        ?>
        </div>
    </div>

    <div id="featuresPaginate" class="columns is-centered">
        <li class="columns column is-multiline">
        <?php
            foreach($features as $feature){
            $post_categories = wp_get_post_terms($feature['ID'],get_post_taxonomies(get_post()->ID)[0]);
            $show_post = false;
            if($post_categories !== false){
                foreach($post_categories as $post_category){
                    if($post_category->slug == $selected_category){
                        $show_post = true;
                    }
                }
            }

            if($show_post || !isset($selected_category)){

            $image = get_post_meta( $feature['ID'], '_feature_preview_image', true );
        ?>
            <div class="column is-one-quarter has-padding-50-mobile" >
                <a class="feature-image" href="<?php echo get_permalink($feature['ID']) ?>"  class="featured-image" style="background-image: url(<?php echo wp_get_attachment_url($image) ?>); background-size: cover;background-position: center center; ">
                    <div class="button">
                        READ MORE
                    </div>
                </a>
            </div>
            <?php }} ?>
        </li>
    </div>

</div>
<?php

    $archive_page = get_page_by_path('weddings/ features');
    global $post;
    $post = $archive_page;
    setup_postdata( $post );
    the_content();
  ?>

  {!! get_the_posts_navigation() !!}
@endsection
