<?php
/*
Template Name: Testimonials Archive
*/
?>

@extends('layouts.app')

@section('content')

<?php
  $page_ID = get_page_by_path('testimonials')->ID;
  $header_image_ID = get_post_meta( $page_ID, '_crb_testimonial_header', true );

?>

<header>
    <section class="hero is-medium alignfull" style="background-image:url('<?php echo wp_get_attachment_url($header_image_ID); ?>'); background-size: cover;background-position: center center; ">
        <div class="hero-body">
            <div class="container">
                <h1 class="hero-title">Testimonials</h1>
            </div>
        </div>
    </section>

 </header>

<div id="category-menu" class="columns has-padding-75">
  <div class="columns column has-text-centered justify-content-center">
      <?php
        $args = array(
            'taxonomy' => 'testimonial_category',
        );

        $cats = get_categories($args);
        $number_of_categories = sizeof($cats);
      ?>
      <div id="category-menu-item--1" class="column is-uppercase is-clickable has-text-weight-semibold is-narrow has-margin-10" onclick="showMenuContents(-1, <?php echo $number_of_categories ?>)"><p>ALL</p></div>
      <?php
          foreach ($cats as $cat){
            if($cat->name !== "Other"){ 
              if($cat->name == "Weddings"){
                $wedding_id = $cat->term_id;
              }

              if($cat->name == "Corporate"){
                $corporate_id = $cat->term_id;
              }
      ?>
      <div id="category-menu-item-<?php echo $cat->term_id ?>" class="column is-uppercase is-clickable has-text-weight-semibold	is-narrow has-margin-10" onclick="showMenuContents(<?php echo $cat->term_id ?>, <?php echo $number_of_categories ?>)"><p><?php echo $cat->name ?></p></div>
      <?php 
        }
      } ?>

      <?php
      foreach ($cats as $cat){
      if($cat->name == "Other"){ 
      ?>
      <div id="category-menu-item-<?php echo $cat->term_id ?>" class="column is-uppercase is-clickable has-text-weight-semibold	is-narrow has-margin-10" onclick="showMenuContents(<?php echo $cat->term_id ?>, <?php echo $number_of_categories ?>)"><p><?php echo $cat->name ?></p></div>
      <?php 
      }
      } ?>  
  </div>
</div>

<div class="entry-content alignfull has-bg-green-medium-light">
<section id="testimonials-archive"  class="feautered_boxes has-text-centered columns is-multiline has-padding-75 column is-8-desktop is-offset-2-desktop">


  @while (have_posts()) @php the_post() @endphp

  <?php
    $name = carbon_get_the_post_meta('testimonial_client_name');
    $text = carbon_get_the_post_meta('testimonial_text');
    $image = carbon_get_the_post_meta('testimonial_image');

    $post_id = get_the_ID();
    $post_categories = get_the_terms($post_id, "testimonial_category");
    $category_id = $post_categories[0]->term_id;

  ?>

    <div class="columns column is-full is-centered is-multiline is-vcentered testimonial-category-<?php echo $category_id ?>">
        <div class="column is-4" style="background-image: url('<?php echo wp_get_attachment_url($image); ?>'); background-size: cover;background-position: center center; "  >
          <div class="box-wrap" style="border: 2px white solid;">
              <div class="box-ratio border-white">

              </div>
          </div>
        </div>
        <div class="column is-8 has-padding-left-75-tablet">
            <p class="has-text-left is-size-6 has-text-weight-normal has-margin-bottom-30"><?php echo $text ?></p>
            <p class="has-text-left is-size-6 has-text-weight-bold is-uppercase">~ <?php echo $name ?> ~</p>
        </div>
    </div>
  @endwhile

  </section>
  </div>

  <script>
      function showMenuContents(selectedPage, numberOfCategories) {

        var menu_div = document.getElementById("category-menu");
        menu_div.scrollIntoView({ block: 'start',  behavior: 'smooth' });

        if(selectedPage !== -1){

          var allMenuItems = document.getElementById("category-menu").children[0].children;
          for(var i=0; i<allMenuItems.length; i++){
            allMenuItems[i].style.color = 'black';
          }

          document.getElementById('category-menu-item-'+selectedPage.toString()).style.color = "#a02525";

          var allTestimonials = document.getElementById("testimonials-archive").children;
          for(var i=0; i<allTestimonials.length; i++){
            allTestimonials[i].classList.add("is-hidden");
          }

          var elements = document.getElementsByClassName("testimonial-category-"+selectedPage.toString());
          for(var i=0; i<elements.length; i++){
            elements[i].classList.remove("is-hidden");
          }
        }else{

          var allMenuItems = document.getElementById("category-menu").children[0].children;
          for(var i=0; i<allMenuItems.length; i++){
            allMenuItems[i].style.color = 'black';
          }

          document.getElementById('category-menu-item--1').style.color = "#a02525";

          var allTestimonials = document.getElementById("testimonials-archive").children;
          for(var i=0; i<allTestimonials.length; i++){
            allTestimonials[i].classList.remove("is-hidden");
          }
        }
      }
  </script>

  @if ($_GET["category"] == "weddings")
    <script>
      showMenuContents(@php echo $wedding_id @endphp, 5);
    </script> 
  @elseif ($_GET["category"] == "events-corporate")
    <script>
      showMenuContents(@php echo $corporate_id @endphp, 5);
    </script>      
  @endif

  {!! get_the_posts_navigation() !!}
@endsection
