<?php
/*
Template Name: Design Archive
*/
?>

@extends('layouts.app')

@section('content')

@php

    $args = array(
        'offset' => 0,
        'orderby' => 'post_date',
        'order' => 'DESC',
        'include' => '',
        'exclude' => '',
        'meta_key' => '',
        'meta_value' =>'',
        'post_type' => 'design',
        'post_status' => 'publish',
        'suppress_filters' => true,
        'numberposts' => -1,
    );

    $designs = wp_get_recent_posts( $args, ARRAY_A );
@endphp

<?php
  $page_ID = get_page_by_path('weddings/our-designs')->ID;
  $header_image_ID = get_post_meta( $page_ID, '_crb_design_header', true );
?>

<header>
    <section class="hero is-medium alignfull" style="background-image:url('<?php echo wp_get_attachment_url($header_image_ID); ?>'); background-size: cover;background-position: center center; ">
        <div class="hero-body">
            <div class="container">
                <h1 class="hero-title">Our designs</h1>
            </div>
        </div>
    </section>

 </header>

<div class="entry-content">
<section class="feautered_boxes has-text-centered has-padding-100-desktop has-padding-50">
  <div class="columns is-marginless is-centered is-multiline">


  <?php
        foreach($designs as $design){
            $images = array();
            $images = carbon_get_post_meta($design['ID'], 'design_images');
            $title = carbon_get_post_meta($design['ID'], 'design_name');
            $is_wide = carbon_get_post_meta($design['ID'], 'is_wide');
            $preview_image = carbon_get_post_meta($design['ID'], 'design_preview_image');
  ?>

    <div class="column <?php if($is_wide == true){ echo "is-two-thirds-desktop"; }else { echo "is-4-desktop"; } ?>">
          <div class="hero" style="background-image: url('<?php echo wp_get_attachment_url($preview_image[0]); ?>'); background-size: cover;background-position: center center; " >
              <div class="box-wrap">
                  <div class="box-ratio <?php if($is_wide == true){ echo "has-wide-ratio"; } ?>">
                      <div class="box-content">
                          <div class="box-elements is-flex button-border">
                              <a href="<?php echo get_permalink($design['ID']); ?>" class="button is-normal is-borderless is-radiusless btn-color-medium-light"><?php echo $title;?></a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    <?php } ?>

  </div>
  </section>
  </div>
  <?php
    $archive_page = get_page_by_path('weddings/our-designs');
    global $post;
    $post = $archive_page;
    setup_postdata( $post );
    the_content();
  ?>
  {!! get_the_posts_navigation() !!}
@endsection
