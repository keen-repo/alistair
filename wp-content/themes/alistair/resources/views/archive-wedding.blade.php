<?php
/*
Template Name: Wedding Archive
*/
?>

@extends('layouts.app')

@section('content')

@php

    $args = array(
        'offset' => 0,
        'orderby' => 'post_date',
        'order' => 'DESC',
        'include' => '',
        'exclude' => '',
        'meta_key' => '',
        'meta_value' =>'',
        'post_type' => 'wedding',
        'post_status' => 'publish',
        'suppress_filters' => true,
        'numberposts' => -1,
    );

    $weddings = wp_get_recent_posts( $args, ARRAY_A );
@endphp

<?php
  $page_ID = get_page_by_path('weddings/our-weddings')->ID;
  $header_image_ID = get_post_meta( $page_ID, '_crb_wedding_header', true );
?>

<header>
    <section class="hero is-medium alignfull" style="background-image:url('<?php echo wp_get_attachment_url($header_image_ID); ?>'); background-size: cover;background-position: center center; ">
        <div class="hero-body">
            <div class="container">
                <h1 class="hero-title">Our weddings</h1>
            </div>
        </div>
    </section>

 </header>

<section class="feautered_boxes has-text-centered has-margin-25 has-margin-100-desktop">
  <div id="weddingPaginate" class="columns is-marginless is-centered is-multiline">

    <?php
        foreach($weddings as $wedding){
            $images = array();
            $images = carbon_get_post_meta($wedding['ID'], 'wedding_images');
            $title = carbon_get_post_meta($wedding['ID'], 'wedding_surname');
            $preview_image = carbon_get_post_meta($wedding['ID'], 'wedding_preview_image');
    ?>

    <li class="column is-4-desktop">
          <div class="hero" style="background-image: url('<?php echo wp_get_attachment_url($preview_image[0]); ?>'); background-size: cover;background-position: center center; " >
              <div class="box-wrap">
                  <div class="box-ratio">
                      <div class="box-content">
                          <div class="box-elements is-flex button-border">
                              <a href="<?php echo get_permalink($wedding['ID']); ?>" class="button is-normal is-borderless is-radiusless btn-color-medium-light"><?php echo $title;?></a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
    </li>
    <?php } ?>

  </div>
  </section>
  <?php
    $archive_page = get_page_by_path('weddings/our-weddings');
    global $post;
    $post = $archive_page;
    setup_postdata( $post );
    the_content();
  ?>
  {!! get_the_posts_navigation() !!}
@endsection
