<?php
/**
 * Customer completed order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-completed-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php /* translators: %s: Customer first name */ ?>
<p><?php printf( esc_html__( 'Hi %s,', 'woocommerce' ), esc_html( $order->get_billing_first_name() ) ); ?></p>
<?php /* translators: %s: Site title */ ?>
<p><?php esc_html_e( 'Kindly note that your order has been confirmed and will be delivered or ready to collect in store as per details below.', 'woocommerce' ); ?></p>
<?php

/*
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

/*
* get all the meta data values we need
*/
$get_door_number = get_post_meta( $order->id, 'door_number', $unique = false );
$get_address_type = get_post_meta( $order->id, 'address_type', $unique = false );
$rec_mobile = get_post_meta( $order->id, 'delivery_phone', $unique = false );

?>

<table id="addresses" cellspacing="0" cellpadding="0" border="0" style="width:100%;vertical-align:top;margin-bottom:40px;padding:0">
		<tbody>
		<tr>
			<td valign="top" width="50%" style="text-align:left;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif;border:0;padding:0">
				<h2 style="color:#965250;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:18px;font-weight:bold;line-height:130%;margin:0 0 18px;text-align:left">
          <?php _e( 'Delivery address', 'woocommerce' ); ?>
				</h2>
				<address class="address" style="padding:12px;color:#636363;border:1px solid #e5e5e5">
          <?php echo $order->get_formatted_shipping_address(); ?>
          <p><?= $rec_mobile[0]; ?></p>
          <?php if (!empty($get_door_number[0])) { ?>
          <p>Door Number: <?= $get_door_number[0]; ?></p>
          <?php } ?>
          <p>Address Type: <?= $get_address_type[0]; ?></p>
				</address>
			</td>
		</tr>
		</tbody>
	</table>

<?php
			/*
			* get all the meta data values we need
			*/
      $delivery_type = get_post_meta( $order->id, 'delivery_type', $unique = false );

      $get_pickup_date = get_post_meta( $order->id, 'pickup_date', $unique = false );
      $get_pickup_time = get_post_meta( $order->id, 'pickup_time', $unique = false );
      $get_delivery_time = get_post_meta( $order->id, 'delivery_time', $unique = false );

      $customer_note = get_post_meta( $order->id, 'customer_note', $single = true );
		?>
        <br class="clear" />
        <h2 style="color:#965250;display:block;font-family:Helvetica Neue,Helvetica,Roboto,Arial,sans-serif;font-size:18px;font-weight:bold;line-height:130%;margin:0 0 18px;text-align:left">Additional Info</h2>
        <p>Delivery Type: <?= $delivery_type[0]; ?></p>

        <p><strong>Delivery/Pick Up Date:</strong> <?= $get_pickup_date[0]; ?></p>
        <?php if (!empty($get_pickup_time[0])) { ?>
            <p><strong>Pick Up Time:</strong> <?= $get_pickup_time[0]; ?></p>
        <?php } ?>
        <?php if(!empty($get_delivery_time[0])) { ?>
            <p><strong>Delivery Time:</strong> <?= $get_delivery_time[0]; ?></p>
        <?php } ?>
        </p>
<?php

    if(empty($customer_note)) {
      $customer_note = '<i>n/a</i>';
    }

    $get_message_oncard = get_post_meta( $order->id, 'message_on_card', $single = true );
    if(empty($get_message_oncard))
      $get_message_oncard = '<i>n/a</i>';
    ?>

    <h4>Customer Note</h4>
    <p><?= $customer_note; ?></p>

    <h4>Message on Card</h4>
    <p><?= $get_message_oncard; ?></p>

<p>
<?php esc_html_e( 'Thanks for shopping with us.', 'woocommerce' ); ?>
</p>
<?php

/*
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
