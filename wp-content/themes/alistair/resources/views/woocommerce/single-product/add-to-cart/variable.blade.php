@php
global $product;

$attribute_keys  = array_keys( $attributes );
$variations_json = wp_json_encode( $available_variations );
$variations_attr = function_exists( 'wc_esc_json' ) ? wc_esc_json( $variations_json ) : _wp_specialchars( $variations_json, ENT_QUOTES, 'UTF-8', true );

do_action( 'woocommerce_before_add_to_cart_form' );
@endphp
<form class="variations_form cart" action="@php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); @endphp" method="post" enctype='multipart/form-data' data-product_id="@php echo absint( $product->get_id() ); @endphp" data-product_variations="@php echo $variations_attr; // WPCS: XSS ok. @endphp">
	@php do_action( 'woocommerce_before_variations_form' ); @endphp

	@php if ( empty( $available_variations ) && false !== $available_variations ) : @endphp
		<p class="stock out-of-stock">@php esc_html_e( 'This product is currently out of stock and unavailable.', 'woocommerce' ); @endphp</p>
	@php else : @endphp
    <div class="variations">
        @php foreach ( $attributes as $attribute_name => $options ) : @endphp
          <div class="variation">
            @php $hasIcon = false; @endphp
            @if( $attribute_name == 'pa_size')
              <h4>@php _e( 'Select a Size' ) @endphp</h4>
              @php
                if(in_array('small', $options)){
                  if($options[0] !== 'small') {
                    $options = array_reverse( $options );
                  }
                  $hasIcon = true;
                } else {
                  asort($options);
                  $hasIcon = false;
                }
              @endphp
            @elseif ($attribute_name == 'pa_how-often-should-we-deliver')
              <h4>@php _e( 'How often should we deliver?' ) @endphp</h4>
            @elseif ($attribute_name == 'pa_how-long-should-we-deliver')
              <h4>@php _e( 'How long should we deliver?' ) @endphp</h4>
            @endif
            <div class="options is-flex">
              @foreach( $options as $label => $option )
                @php
                $checked = '';
                if(
                  (isset($_POST['attribute_pa_size']) && $_POST['attribute_pa_size'] == $option)
                  || (isset($_GET['attribute_pa_size']) && $_GET['attribute_pa_size'] == $option)
                ){
                    $checked = 'checked';
                }
                @endphp
                <div class="option">
                  <input type="radio" @if($hasIcon) class="has-icon" @endif @php echo $checked @endphp name="attribute_@php echo esc_attr( sanitize_title( $attribute_name ) ); @endphp" value="@php echo $option @endphp" id="@php echo esc_attr( sanitize_title( $attribute_name ).'_'.$option ); @endphp" />
                  <label for="@php echo esc_attr( sanitize_title( $attribute_name ).'_'.$option ); @endphp">@php echo str_replace( '-', ' ', $option ); @endphp</label>
                </div>
              @endforeach
            </div>
            <div class="hidden-select">
              <label for="@php echo esc_attr( sanitize_title( $attribute_name ) ); @endphp">@php echo wc_attribute_label( $attribute_name ); @endphp</label>
              @php
                wc_dropdown_variation_attribute_options( array(
                  'options'   => $options,
                  'attribute' => $attribute_name,
                  'product'   => $product,
                ) );
                echo end( $attribute_keys ) === $attribute_name ? wp_kses_post( apply_filters( 'woocommerce_reset_variations_link', '<a class="reset_variations" href="#">' . esc_html__( 'Clear', 'woocommerce' ) . '</a>' ) ) : '';
              @endphp
            </div>
          </div>
				@php endforeach; @endphp
    </div>
		<div class="single_variation_wrap">
			@php
				/**
				 * Hook: woocommerce_before_single_variation.
				 */
				do_action( 'woocommerce_before_single_variation' );

				/**
				 * Hook: woocommerce_single_variation. Used to output the cart button and placeholder for variation data.
				 *
				 * @since 2.4.0
				 * @hooked woocommerce_single_variation - 10 Empty div for variation data.
				 * @hooked woocommerce_single_variation_add_to_cart_button - 20 Qty and cart button.
				 */
				do_action( 'woocommerce_single_variation' );

				/**
				 * Hook: woocommerce_after_single_variation.
				 */
				do_action( 'woocommerce_after_single_variation' );
			@endphp
		</div>
	@php endif; @endphp

	@php do_action( 'woocommerce_after_variations_form' ); @endphp
</form>

@php
do_action( 'woocommerce_after_add_to_cart_form' );
@endphp
