<div class="orderby-wrap is-pulled-right">
  @php _e( 'Sort by', 'als-front' ) @endphp:
  <form class="woocommerce-ordering" method="get">
    <select name="orderby" class="orderby">
      @php foreach ( $catalog_orderby_options as $id => $name ) : @endphp
        <option value="<?php echo esc_attr( $id ); ?>" <?php selected( $orderby, $id ); ?>><?php echo esc_html( $name ); ?></option>
      @php endforeach; @endphp
    </select>
    <input type="hidden" name="paged" value="1" />
    @php wc_query_string_form_fields( null, array( 'orderby', 'submit', 'paged', 'product-page' ) ); @endphp
  </form>
</div>
