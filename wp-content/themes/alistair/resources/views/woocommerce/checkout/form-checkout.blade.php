{{--
 Checkout Form

 This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.

 HOWEVER, on occasion WooCommerce will need to update template files and you
 (the theme developer) will need to copy the new files to your theme to
 maintain compatibility. We try to do this as little as possible, but it does
 happen. When this occurs the version of the template file will be bumped and
 the readme will list any important changes.

 @see https://docs.woocommerce.com/document/template-structure/
 @package WooCommerce/Templates
 @version 3.5.0
--}}
@section('content')
<!-- Checkout Form -->
<form name="checkout" id="form-checkout" method="post" class="" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

<div class="steps" id="checkout-steps">

  @include('partials.steps')

  <div class="steps-content">

    <div class="step-content has-text-centered">
      <div class="columns is-centered delivery-options">
        <div class="column is-7">
          <div class="field is-horizontal">
              <div class="field-body">
                <label class="check-wrap checkbox">
                    <input id="pickUp" name="pick_up" value="pick_up" type="checkbox">Pick Up
                    <span class="checkmark"><i class="icon-check"></i></span>
                </label>
              </div>
              <div class="field-body">
                <label class="check-wrap checkbox">
                    <input id="deliveryToMe" name="delivery_to_Me" value="delivery_to_Me" type="checkbox">Delivery to Me
                    <span class="checkmark"><i class="icon-check"></i></span>
                </label>
              </div>
              <div class="field-body">
                <label class="check-wrap checkbox">
                    <input id="deliveryToReciever" name="delivery_to_Receiver" value="delivery_to_Receiver" type="checkbox">Delivery to Receiver
                    <span class="checkmark"><i class="icon-check"></i></span>
                </label>
              </div>
          </div>

          {{-- Pick Up Form  --}}
          <div id="formOne" style="display: none">
            <div class="columns is-centered">
              <div class="column ">
                  <label class="label has-text-left">MY INFO</label>
                  <div class="field">
                    <div class="control">
                      <input id="pickup_name" class="input is-radiusless" type="text" name="name" placeholder="NAME AND SURNAME">
                    </div>
                  </div>
                  <div class="field">
                    <div class="control">
                      <input id="pickup_mobile" class="input is-radiusless" type="text" name="mobile" placeholder="MOBILE">
                    </div>
                  </div>
                  <div class="field">
                    <div class="control">
                      <input id="pickup_email" class="input is-radiusless" type="email" name="email" placeholder="EMAIL">
                    </div>
                  </div>
              </div>
              <div class="column ">
                <label class="label has-text-left">PICK UP INFO</label>
                <div class="field is-grouped">
                    <div class="control">
                      {{-- <input id="pickup_date" class="input is-radiusless" type="date" name="pickup_date" placeholder="SELECT DATE" min="{{ date('Y-m-d') }}"> --}}
                      <input id="pickup_date" class="input is-radiusless" type="date" name="pickup_date" placeholder="SELECT DATE" min="{{ date('Y-m-d') }}">
                    </div>
                    <div class="control">
                      <div class="help">Please note Sundays are only available between October & December.</div>
                    </div>
                  </div>
                  <div class="field is-grouped">
                    <div class="control">
                      <div class="select is-radiusless">
                          <select id="pickup_time" name="time">
                            <option>SELECT TIME</option>
                            <option style="display: none;">8:00 AM</option>
                            <option>9:00 AM</option>
                            <option>10:00 AM</option>
                            <option>11:00 AM</option>
                            <option>12:00 PM</option>
                            <option>1:00 PM</option>
                            <option>2:00 PM</option>
                            <option>3:00 PM</option>
                            <option>4:00 PM</option>
                            <option>5:00 PM</option>
                            <option>6:00 PM</option>
                            <option>7:00 PM</option>
                          </select>
                      </div>
                    </div>
                    <div class="control">
                      <div class="help">Only available from 9:00AM - 7:00PM</div>
                    </div>
                  </div>
                  <div id="disabled-dates" data-disableddates="{{ json_encode(carbon_get_theme_option('checkout_disabled_dates')) }}"></div>
                  <p class="delivery-warning" style="display: none">{{ carbon_get_theme_option('checkout_sameday_note') }}</p>
                  <p class="disabled-warning" style="display: none">{{ carbon_get_theme_option('checkout_disabled_note') }}</p>
                  
              </div>
            </div>
          </div> <!--End Form 1 -->
          {{-- Delivery to Me Form  --}}
          <div id="formTwo" style="display: none">
              <div class="columns is-centered">
                  <div class="column ">
                      <label class="label has-text-left">MY INFO</label>
                      <div class="field">
                        <div class="control">
                          <input id="billing_name" class="input is-radiusless" type="text" name="name2" placeholder="NAME AND SURNAME">
                        </div>
                      </div>
                      <div class="field">
                        <div class="control">
                          <input id="billing_mobile" class="input is-radiusless" type="text" name="mobile2" placeholder="MOBILE">
                        </div>
                      </div>
                      <div class="field">
                        <div class="control">
                          <input id="billing_email" class="input is-radiusless" type="email" name="email2" placeholder="EMAIL">
                        </div>
                      </div>
                      <label class="label has-text-left">DELIVERY ADDRESS</label>
                      <div class="field">
                        <div class="control">
                          <input id="billing_door_number" class="input is-radiusless" type="text" name="door_number2" placeholder="DOOR NUMBER">
                        </div>
                      </div>
                      <div class="field">
                        <div class="control">
                          <input id="billing_address1" class="input is-radiusless" type="text" name="address12" placeholder="ADDRESS LINE 1">
                        </div>
                      </div>
                      <div class="field">
                          <div class="control">
                            <input id="billing_address2" class="input is-radiusless" type="text" name="address22" placeholder="ADDRESS LINE 2">
                          </div>
                      </div>
                      <div class="field">
                        <div class="control">
                          <input id="billing_city" class="input is-radiusless" type="text" name="city2" placeholder="CITY">
                        </div>
                      </div>
                      <div class="field">
                        <div class="control">
                          <label class="radio">
                            <input id="billing_region_malta" type="radio" name="billing_region" value="Malta" checked="checked">
                            @php _e( 'Malta', 'als-front' ) @endphp
                          </label>
                          <label class="radio">
                            <input id="billing_region_gozo" type="radio" name="billing_region" value="Gozo">
                            @php _e( 'Gozo', 'als-front' ) @endphp
                          </label>
                        </div>
                      </div>
                      <div class="field">
                        <div class="control">
                          <input id="billing_post_code" class="input is-radiusless" type="text" name="post_code2" placeholder="POST CODE">
                        </div>
                      </div>
                      <div class="field">
                          <div class="control">
                            <label class="radio">
                              <input id="billing_addr_type" type="radio" name="addr_type" value="Residential" checked="checked">
                              @php _e( 'RESIDENTIAL', 'als-front' ) @endphp
                            </label>
                            <label class="radio">
                              <input id="billing_addr_type" type="radio" name="addr_type" value="Commercial">
                              @php _e( 'COMMERCIAL', 'als-front' ) @endphp
                            </label>
                          </div>
                      </div>
                  </div>
                  <div class="column ">
                    <label class="label has-text-left">DELIVERY INFO</label>
                    <div class="field is-grouped">
                        <div class="control">
                          @php
                            $min_date = date('Y-m-d');
                            //Activate after Valentines Day 2022
                            if(((date('D') == 'Sat') && (date('H') > 14)) || date('D') == 'Sun') { 
                              //Today is Saturday after 2pm or Sunday;
                              $next_monday = strtotime('next monday');
                              $min_date = date('Y-m-d', $next_monday);
                            }
                          @endphp
                          <input id="billing_date" class="input is-radiusless" type="date" name="date2" placeholder="SELECT DATE" min="{{ $min_date }}">
                          {{-- <input id="billing_date" class="input is-radiusless" type="date" name="date2" placeholder="SELECT DATE" min=""> --}}
                        </div>
                        <div class="control">
                          {{-- <div class="help">An additional fee of €20 for Malta and €30 Gozo may be applied on certain Sundays & Public Holidays</div> --}}
                        </div>
                    </div>
                    <div class="field">
                      <div class="control" id="me-deliver-control">
                        <label class="radio">
                          <input id="billing_delivery_time" type="radio" name="delivery_time2" value="Morning" checked="checked">
                          MORNING
                        </label>
                        <label class="radio">
                          <input id="billing_delivery_time" type="radio" name="delivery_time2" value="Afternoon">
                          AFTERNOON
                        </label>
                      </div>
                      <div id="disabled-dates" data-disableddates="{{ json_encode(carbon_get_theme_option('checkout_disabled_dates')) }}"></div>
                      <p class="delivery-warning" style="display: none">{{ carbon_get_theme_option('checkout_sameday_note') }}</p>
                      <p class="disabled-warning" style="display: none">{{ carbon_get_theme_option('checkout_disabled_note') }}</p>
                      <div id='timeOfday'></div>
                    </div>
                    <div class="field">
                      <div class="control">
                        <textarea name="additional_info2" class="textarea is-radiusless" placeholder="ADDITIONAL INFO FOR DELIVERY"></textarea>
                      </div>
                    </div>
                </div>
              </div>
          </div> <!-- End Form 2 -->

          {{-- Delivery to Receiver Form --}}
          <div id="formThree" style="display: none">
            <div class="columns is-centered">
                <div class="column ">
                    <label class="label has-text-left">MY INFO</label>
                    <div class="field">
                      <div class="control">
                        <input id="name3" class="input is-radiusless" type="text" name="name3" placeholder="NAME AND SURNAME">
                      </div>
                    </div>
                    <div class="field">
                      <div class="control">
                        <input id="mobile3" class="input is-radiusless" type="text" name="mobile3" placeholder="MOBILE">
                      </div>
                    </div>
                    <div class="field">
                      <div class="control">
                        <input id="email3" class="input is-radiusless" type="email" name="email3" placeholder="EMAIL">
                      </div>
                    </div>
                    <label class="label has-text-left">DELIVERY ADDRESS</label>
                    <div class="field">
                      <div class="control">
                        <input id="door_number3" class="input is-radiusless" type="text" name="door_number3" placeholder="DOOR NUMBER">
                      </div>
                    </div>
                    <div class="field">
                      <div class="control">
                        <input id="address13" class="input is-radiusless" type="text" name="address13" placeholder="ADDRESS LINE 1">
                      </div>
                    </div>
                    <div class="field">
                        <div class="control">
                          <input id="address23" class="input is-radiusless" type="text" name="address23" placeholder="ADDRESS LINE 2">
                        </div>
                    </div>
                    <div class="field">
                      <div class="control">
                        <input id="city3" class="input is-radiusless" type="text" name="city3" placeholder="CITY">
                      </div>
                    </div>
                    <div class="field">
                      <div class="control">
                        <label class="radio">
                          <input id="billing_region_malta_2" type="radio" name="billing_region_2" value="Malta" checked="checked">
                          @php _e( 'Malta', 'als-front' ) @endphp
                        </label>
                        <label class="radio">
                          <input id="billing_region_gozo_2" type="radio" name="billing_region_2" value="Gozo">
                          @php _e( 'Gozo', 'als-front' ) @endphp
                        </label>
                      </div>
                    </div>
                    <div class="field">
                      <div class="control">
                        <input id="post_code3" class="input is-radiusless" type="text" name="post_code3" placeholder="POST CODE">
                      </div>
                    </div>
                </div>
                <div class="column ">
                  <label class="label has-text-left">RECEIVER'S INFO</label>
                  <div class="field">
                    <div class="control">
                      <input id="rec_name" class="input is-radiusless" type="text" name="rec_name" placeholder="NAME AND SURNAME">
                    </div>
                  </div>
                  <div class="field">
                    <div class="control">
                      <input id="rec_mobile" class="input is-radiusless" type="text" name="rec_mobile" placeholder="MOBILE">
                    </div>
                  </div>
                  <div class="field">
                    <div class="control">
                      <input id="rec_email" class="input is-radiusless" type="text" name="rec_email" placeholder="EMAIL">
                    </div>
                  </div>
                  <label class="label has-text-left">DELIVERY INFO</label>
                  <div class="field is-grouped">
                      <div class="control">
                        @php
                          $min_date = date('Y-m-d');
                          //Activate after Valentines Day 2022
                          if(((date('D') == 'Sat') && (date('H') > 14)) || date('D') == 'Sun') { 
                            //Today is Saturday after 2pm or Sunday;
                            $next_monday = strtotime('next monday');
                            $min_date = date('Y-m-d', $next_monday);
                          }
                        @endphp
                        {{-- <input id="rec_date" class="input is-radiusless" type="date" name="rec_date" placeholder="SELECT DATE" min="{{ date('Y-m-d') }}"> --}}
                        <input id="rec_date" class="input is-radiusless" type="date" name="rec_date" placeholder="SELECT DATE" min="{{ $min_date }}">
                      </div>
                      <div class="control">
                        {{-- <div class="help">An additional fee of €20 for Malta and €30 Gozo may be applied on certain Sundays & Public Holidays</div> --}}
                      </div>
                  </div>
                  <div class="field">
                    <div class="control" id="rec-deliver-control">
                      <label class="radio">
                        <input type="radio" name="delivery_time3" value="Morning" checked="checked">
                        MORNING
                      </label>
                      <label class="radio">
                        <input type="radio" name="delivery_time3" value="Afternoon">
                        AFTERNOON
                      </label>
                    </div>
                    <div id="disabled-dates" data-disableddates="{{ json_encode(carbon_get_theme_option('checkout_disabled_dates')) }}"></div>
                    <p class="delivery-warning" style="display: none">{{ carbon_get_theme_option('checkout_sameday_note') }}</p>
                    <p class="disabled-warning" style="display: none">{{ carbon_get_theme_option('checkout_disabled_note') }}</p>
                    <div id='timeOfday2'></div>
                  </div>
                  <div class="field">
                    <div class="control">
                      <textarea name="additional_info3" class="textarea is-radiusless" placeholder="ADDITIONAL INFO FOR DELIVERY"></textarea>
                    </div>
                  </div>
              </div>
            </div>
          </div> <!-- End Form 3 -->

        </div> <!-- End main column -->
      </div>
    </div> <!-- End Step 1 -->

    <div class="step-content has-text-centered">
        <input id='step2' name="step2" type="hidden" value="">
        <div class="columns is-centered delivery-options">
          <div class="column is-7">
            <div class="field is-horizontal">
                <div class="field-body">
                  <label class="check-wrap checkbox">
                      <input id="write_message" name="message_card" type="checkbox" checked="checked">Write Message
                      <span class="checkmark"><i class="icon-check"></i></span>
                  </label>
                </div>
                <div class="field-body">
                  <label class="check-wrap checkbox">
                      <input id="nocard" name="message_card" type="checkbox" >No Message Card
                      <span class="checkmark"><i class="icon-check"></i></span>
                  </label>
                </div>
                <div class="field-body">
                  <label class="check-wrap checkbox">
                      <input id="myself" name="message_card" type="checkbox" >Will Write It Myself
                      <span class="checkmark"><i class="icon-check"></i></span>
                  </label>
                </div>
            </div>
            <label class="label has-text-left">MESSAGE</label>
            <div class="field">
              <div class="control">
                <textarea id="message" name="message_on_card" class="textarea is-radiusless" placeholder="TYPE MESSAGE HERE" maxlength="100"></textarea>
              </div>
            </div>

          </div>
        </div>
    </div> <!-- End Step 2 -->

    <div id='step3' class="step-content has-text-centered">
        <input id='step3' name="step3" type="hidden" value="">
        <div class="columns">

          @php
            global $woocommerce;
            // Get cart items
            $items = $woocommerce->cart->get_cart();
          @endphp
        </div>

        <div class="columns confirmation-wrap has-text-left delivery-options">

          <div id="show_pickup" class="column">
              <h3>Pick Up Information</h3>
              <!--<div id="show_pickup_name"></div>
              <div id="show_pickup_mobile"></div>
              <div id="show_pickup_email"></div>-->
              <div id="show_pickup_date"></div>
              <div id="show_pickup_time"></div>
          </div>
          <div class="column">
              <div id="show_pickup2">
                  <h3>Message Card</h3>
                  <div id="show_pickup_message"></div>
              </div>
          </div>


          <div id="show_other_info" class="column">
            <div>
              <h3>Delivery Address</h3>
              <div id="show_reciever_name"></div>

              <div id="show_billing_door_number"></div>
              <div id="show_billing_address1"></div>
              <div id="show_billing_address2"></div>
              <div id="show_billing_city"></div>
              <div id="show_billing_region"></div>

              <div id="show_billing_post_code"></div>

              <div id="show_reciever_mobile"></div>
              <div id="show_reciever_email"></div>

              <h3>Delivery Information</h3>
              <div id="show_billing_date"></div>
              <div id="show_billing_time"></div>
            </div>
          </div>

          <div id="show_other_info2" class="column">
            <div>
              <h3>My Info</h3>
              <div id="show_billing_name"></div>

              <div id="show_billing_door_number"></div>
              <div id="show_billing_address1"></div>
              <div id="show_billing_address2"></div>
              <div id="show_billing_city"></div>
              <div id="show_billing_region"></div>

              <div id="show_billing_post_code"></div>
              <div id="show_billing_mobile"></div>
              <div id="show_billing_email"></div>


                <h3>Message Card</h3>
                <div id="show_billing_message3"></div>
            </div>
          </div>


          <div class="column is-half confirmation-wrap">
              <h3>Order Details</h3>
              <div id="order-details-wrap">
                <?php
                  global $woocommerce;

                  // Get cart items
                  $items = $woocommerce->cart->get_cart();

                  foreach($items as $item => $values) {
                    $product =  wc_get_product( $values['data']->get_id() );

                    // check if variation or single product
                    if($product->get_type() == 'variation'){
                      $getProductDetail = wc_get_product( $values['variation_id'] );
                      $price = get_post_meta($values['variation_id'] , '_price', true);
                    } else {
                      $getProductDetail = wc_get_product( $values['product_id'] );
                      $price = get_post_meta($values['product_id'] , '_price', true);
                    }

                    $product_image = $getProductDetail->get_image('thumbnail'); // accepts 2 arguments ( size, attr )
                    $line_total = wc_price( $price * $values['quantity'] );
                    ?>

                    <div class="order-details columns is-marginless is-multiline is-mobile">
                      <div class="column is-12-mobile is-paddingless item-image"><?= $product_image; ?></div>
                      <div class="column is-two-thirds-mobile is-paddingless item-details">
                        <div class="is-flex">
                            x<?= $values['quantity']; ?> <?= $product->get_title(); ?>
                        </div>
                      </div>
                      <div class="column is-one-third-mobile is-one-fifth-tablet is-paddingless has-text-right item-price">
                        <div class="is-flex"><?= $line_total; ?></div>
                      </div>
                    </div>

                <?php
                }

                ?>
              </div>
              <div class="cart-total-wrap columns is-marginless is-mobile">
                <div class="column is-paddingless">TOTAL: </div>
                <div class="column is-one-third is-paddingless has-text-right">
                  <?= get_woocommerce_currency_symbol(); ?><span id="total-amount"><?= $woocommerce->cart->total; ?></span>
                </div>
              </div>
              <div class="addons woocommerce">
                {{ do_action('load_gift_addons') }}
              </div>
              <div class="payment-methods woocommerce-checkout-payment">
                <div class="columns is-marginless">
                  <div class="column is-one-third is-paddingless">
                      Payment method
                  </div>
                  <div class="column is-paddingless">
                      <div class="field">
                        <div class="control">
                          <label class="radio is-flex">
                            <input type="radio" name="payment_method" value="mypos_virtual" >
                            Credit Card or Debit Card
                          </label>
                          <br>
                          <label class="radio is-flex">
                            <input type="radio" name="payment_method" value="paypal" >
                            <img src="@asset('images/paypal-logo.png')" >
                          </label>
                          <br>
                          {{-- <label class="radio is-flex">
                            <input type="radio" name="payment_method" value="phone_pay">
                            PAY VIA PHONE
                          </label> --}}
                          {{-- <br> --}}
                          {{-- <label class="radio is-flex">
                            <input type="radio" name="payment_method" value="revolut_pay">
                            <img src="@asset('images/revolut-logo-small.png')" >
                          </label> --}}
                          {{-- <br> --}}
                          <label class="radio is-flex">
                            <input id="for_pickup" type="radio" name="payment_method" value="cod">
                            <span id="pickup_label">PAY ON DELIVERY</span>
                          </label>
                          <br>
                          <label id='pay_on_collection_radio' class="radio is-flex">
                              <input id="for_pay_on_collection" type="radio" name="payment_method" value="pay_on_collection">
                              PAY ON COLLECTION
                          </label>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
          </div>
      </div>
    </div> <!-- End Step 3 -->


    <div class="step-content has-text-centered">
        <input id='step4' type="hidden" value="">
        <div class="columns is-centered is-marginless">
          {{-- Include Thank you template when order is done.  --}}
          @php
            $order_id = $_GET['id'];
            $order_id = str_replace('#', '', $order_id);
          @endphp
          @if($order_id)
            @include('partials.thankyou')
          @endif
        </div>
    </div> <!-- End Step 4 -->


    <br><br>
    <div class="steps-actions">
      <div id="btn_prev" class="steps-action">
        <a href="javascript:;" class="button is-normal is-radiusless btn-color-primary">Previous</a>
      </div>
      <div id="btn_next" class="steps-action">
        <a href="javascript:;" class="button is-normal is-radiusless btn-color-primary">Next</a>
      </div>
      <div id="btn_payment" class="steps-action">
        <!-- Submit button on the end -->
        <input id="checkout-proceed-button" type="submit" name="process-checkout" class="button is-normal is-radiusless btn-color-primary" value="PROCEED TO PAYMENT"  />
      </div>

      <div class="processing-modal">
        <p class="processing-text">Processing..</p>
      </div>
    </div>
  </div> <!-- Steps content -->
</div> <!-- Steps main -->

</form>
@endsection

