// import external dependencies
import 'jquery';
import 'jquery-ui-dist/jquery-ui.min.js';

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';
import contact from './routes/contact';
import woocommerce from './routes/woocommerce';
import woocommerceCheckout from './routes/woocommerceCheckout';
import easyPaginate from './routes/easyPaginate';

// Import Slick
import 'slick-carousel/slick/slick.min';

// Import Remodal
import 'remodal/dist/remodal.min';

// Import AOS
import AOS from 'aos/dist/aos';
AOS.init({
  delay: 500,
  easing: 'ease-in-out-quad',
  disable: 'mobile',
});

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
  // contact us page the map
  contact,
  // Single product
  woocommerce,
  // woocommerce Chekout
  woocommerceCheckout,
  //easyPaginate
  easyPaginate,

});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
