export default {
  init() {
    // JavaScript to be fired on all pages
    // Initialize Slick Sliders
    $('.related.products > ul.products, .sliding.products > ul.products').slick({
      slidesToShow: 3,
      slidesToScroll: 3,
      responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });

    //Sliding Hero
    $('.hero-slick-carousel').children('p').each(function() {
      var $this = $(this);
      if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
        $this.remove();
    });

    $('.hero-slick-carousel').slick({
      autoplay: true,
      autoplaySpeed: 3000,
      arrows: false,
      dots: true,
    });

    // Testimonials Slider
    $('.testimonials ul.items').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
    });

    //Sliding text
    $('.sliding-text-slick').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      arrows: true,
      responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
          },
        },
      ],
    });

    $('.sliding-designs').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
          },
        },
      ],
    });

    $('.our-clients-events').children().slick({
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 5,
      responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
          },
        },
      ],
    });

    $('#related-blog-slider').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: true,
      responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
          },
        },
      ],
    });

    $('#easyPaginate').easyPaginate({
        paginateElement: 'li',
        elementsPerPage: 16,
        effect: 'fade',
        firstButton: false,
        lastButton: false,
        prevButtonText: 'PREV',
        nextButtonText: 'NEXT',
    });

    $('#clientsPaginate').easyPaginate({
        paginateElement: 'li',
        elementsPerPage: 16,
        effect: 'fade',
        firstButton: false,
        lastButton: false,
        prevButtonText: 'PREV',
        nextButtonText: 'NEXT',
    });

    $('#designPaginate').easyPaginate({
        paginateElement: 'li',
        elementsPerPage: 16,
        effect: 'fade',
        firstButton: false,
        lastButton: false,
        prevButtonText: 'PREV',
        nextButtonText: 'NEXT',
    });

    $('#weddingPaginate').easyPaginate({
        paginateElement: 'li',
        elementsPerPage: 12,
        effect: 'fade',
        firstButton: false,
        lastButton: false,
        prevButtonText: 'PREV',
        nextButtonText: 'NEXT',
    });

    $('#blogPaginate').easyPaginate({
      paginateElement: 'li',
      elementsPerPage: 6,
      effect: 'fade',
      firstButton: false,
      lastButton: false,
      prevButtonText: 'PREV',
      nextButtonText: 'NEXT',
  });

  $('#featuresPaginate').easyPaginate({
    paginateElement: 'li',
    elementsPerPage: 12,
    effect: 'fade',
    firstButton: false,
    lastButton: false,
    prevButtonText: 'PREV',
    nextButtonText: 'NEXT',
});

    $('.apply').click(function() {
      $('#modal').addClass('is-active');

      var jobPosition = $(this).attr('data-job-position');
      $('#field_job-position').val(jobPosition);
      $('#field_job-position').attr('data-frmval',jobPosition);
    });

    $('#modal-background').click(function() {
      $('#modal').removeClass('is-active');
    });

    $('#modal-close').click(function() {
      $('#modal').removeClass('is-active');
    });

    $('#enter-website-btn').click(function() {
      if($('#site-header').is(':visible')) {
        $([document.documentElement, document.body]).animate({
          scrollTop: $('#site-header').offset().top,
        }, 1000);
      }else {
        $([document.documentElement, document.body]).animate({
          scrollTop: $('#wrap-container').offset().top,
        }, 1000);
      }
    });

    // stuff for gift addons
    // var currentAddon = 0;

    // $('.gifts-slider .item').click(function() {
    //   var item = $(this).data('item');
    //   currentAddon = item;
    // })

    // $(document).on('opening', '.remodal.addons-modal', function() {
    //   $('#addons-list').hide();
    //   $('#single-addons-list').show();
    //   $('#show-addons-list').removeClass('active');

    //   if(!$('#single-addons-list .single-addons-slider').hasClass('slick-initialized'))
    //     $('#single-addons-list .single-addons-slider').slick();
    // });

    // $(document).on('opened', '.remodal.addons-modal', function() {
    //   $('#single-addons-list .single-addons-slider').slick('slickGoTo', currentAddon);
    // });

    // $('#single-addons-list button, #addons-list .column button').click(function() {
    //   var addonID = $(this).data('addon-id');

    //   if($(this).hasClass('added')){
    //     // remove
    //     $(this).removeClass('added');
    //     $(this).html('Add');
    //     $('.gifts-slider .item input[name="gift_addon_'+addonID+'"]').prop('checked', false);
    //   } else {
    //     // add
    //     // do extra checks for variations
    //     if($(this).parents('.item').find('.variations').length) {
    //       var selectedValue = $(this).parents('.item').find('.variations select').val();
    //       if(selectedValue == 0){
    //         alert('Please select an option before adding addon to cart');
    //       } else {
    //         $(this).addClass('added');
    //         $(this).html('Added');
    //         $('.gifts-slider .item input[name="gift_addon_'+addonID+'"]').prop('checked', true);
    //         $('.gifts-slider .item[data-prod-id="'+addonID+'"] input[type="hidden"]').val(selectedValue);

    //         //add item to Order Details
    //         var addOnText2 = $(this).closest('.item').find('.addon-title').text();
    //         var addOnPrice2 = $(this).closest('.item').find('.price').text();
    //         var addOnImage2 = $(this).closest('.item').find('.image').children(':first').attr('src');

    //         var newItem2 = $('.order-details:first').clone();
    //         newItem2.find('.item-details').find('.is-flex').text(addOnText2);
    //         newItem2.find('.item-price').find('.is-flex').text(addOnPrice2);
    //         newItem2.find('.item-image').children(':first').attr('src',addOnImage2);
    //         newItem2.find('.item-image').children(':first').attr('srcset',addOnImage2);

    //         newItem2.appendTo('#order-details-wrap');

    //         //calculate price
    //         var totalPrice2 = 0;

    //         $('#order-details-wrap').find('.item-price').find('.is-flex').each(function(){

    //           var price2 = $(this).text();
    //           price2 = price2.replace('€','').replace('&euro;', '');
    //           var priceNumber2 = parseFloat(price2);
    //           totalPrice2 = totalPrice2 + priceNumber2;
              
    //         });

              
    //         $('#total-amount').text(totalPrice2);
    //       }
    //     } else {
    //       $(this).addClass('added');
    //       $(this).html('Added');
    //       $('.gifts-slider .item input[name="gift_addon_'+addonID+'"]').prop('checked', true);

    //       //add item to Order Details
    //       var addOnText = $(this).closest('.item').find('.addon-title').text();
    //       var addOnPrice = $(this).closest('.item').find('.price').text();
    //       var addOnImage = $(this).closest('.item').find('.image').children(':first').attr('src');

    //       var newItem = $('.order-details:first').clone();
    //       newItem.find('.item-details').find('.is-flex').text(addOnText);
    //       newItem.find('.item-price').find('.is-flex').text(addOnPrice);
    //       newItem.find('.item-image').children(':first').attr('src',addOnImage);
    //       newItem.find('.item-image').children(':first').attr('srcset',addOnImage);

    //       newItem.appendTo('#order-details-wrap');

    //       //calculate price
    //       var totalPrice = 0;

    //       $('#order-details-wrap').find('.item-price').find('.is-flex').each(function(){

    //         var price = $(this).text();
    //         price = price.replace('€','').replace('&euro;', '');
    //         var priceNumber = parseFloat(price);
    //         totalPrice = totalPrice + priceNumber;
            
    //       });

            
    //       $('#total-amount').text(totalPrice);

    //     }
    //   }
    // });

    // handle gift addon variation change
    $('#single-addons-list .variations select').change(function(){
      var price = $(this).find(':selected').data('price');
      var priceElParent = $(this).parents('.item');

      if(price == undefined){
        var defaultPrice = priceElParent.find('.default-price').html();
        priceElParent.find('.price').html(defaultPrice);
      } else {
        priceElParent.find('.price').html('&euro;' + price.toFixed(2));
      }

    })

    // handle gift modal view switch
    $('#show-addons-list').click(function() {
      $(this).toggleClass('active');
      $('#addons-list').toggle();
      $('#single-addons-list').toggle();
    })

    // sidebar toggle
    $('.mobile-toggle').click(function() {
      $(this).parent().toggleClass('open');
    })

    // copy things to mobile menu
    $('.search-container').clone().appendTo('.shiftnav-main-toggle-content');

    var mobileBasket = $('.site-header .cart-summary').clone().appendTo('.shiftnav-main-toggle-content');
    var cartCount = $('.cart-customlocation').data('cart-number');
    if (cartCount > 0){
      mobileBasket.find('img').after('<p class="mobile-cart-amount">'+cartCount+'</p>');
    }
    $('.site-header .social-links').clone().appendTo('.shiftnav-nav');
    $('.site-header .user-menu.level-item').clone().appendTo('.shiftnav-nav');


    $('.product-image-wrap').find('br').remove();

    $('.easyPaginateNav a').on('click', function() {
      window.scrollTo(0, 0);
    });


    $('.shiftnav-nav').find('.menu-item-has-children').append('<div class="submenu-arrow icon-angle-down"></div>');

    $('.submenu-arrow').click(function() {
      if($(this).siblings('.sub-menu').css('display') == 'block'){
        $(this).css('transform','rotate(0deg)');
        $(this).siblings('.sub-menu').css('display','none');
      }else {
        $(this).css('transform','rotate(180deg)');
        $(this).siblings('.sub-menu').css('display','block');
      }
    });

    var input = $('.phone').find('input')[0];

    if(input){

      var iti = window.intlTelInput(input, {
        nationalMode: true,
        initialCountry: 'auto',
        geoIpLookup: function(success) {
          $.get('https://ipinfo.io', function() {}, 'jsonp').always(function(resp) {
            var countryCode = (resp && resp.country) ? resp.country : '';
            success(countryCode);
          });
        },
        utilsScript: 'https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.4/js/utils.js'});

      var handleChange = function() {
        iti.isValidNumber() ? input.value = iti.getNumber() : null;
      };

      // listen to "keyup", but also "change" to update when the user selects a country
      input.addEventListener('change', handleChange);
      input.addEventListener('keyup', handleChange);

    }

    $('#proudly-featured .kb-gallery-ul ').slick({
      infinite: true,
      slidesToShow: 6,
      slidesToScroll: 6,
       responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 6,
            slidesToScroll: 6,
            infinite: true,
            dots: true,
          },
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
          },
        },
        {
          breakpoint: 500,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });

    $('#latest-wedding-slider').slick({
      slidesToShow: 3,
      slidesToScroll: 3,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });

    $('.search-container .icon-search').click(function(){
      $('.search-container').toggleClass('open');
      $(this).siblings('.search-input-toggle').toggleClass('open');
    });

    $('.filters-mobile').find('select').change(function() {
      window.location = $(this).find('option:selected').val();
    });

    $('#form-checkout').on('keyup keypress', function(e) {
      if(!$(e.target).is('textarea')){
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) { 
          e.preventDefault();
          return false;
        }
      }
    });
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};

/*
jQuery('.product-remove').on('click', function() {
  jQuery(this).parent().remove();
  } );
*/
