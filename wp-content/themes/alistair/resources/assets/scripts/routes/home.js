// Import ScrollMagic
import ScrollMagic from 'scrollmagic/scrollmagic/minified/ScrollMagic.min';

export default {
  init() {
    // JavaScript to be fired on the home page
    // $(document).scroll(function(){
    //   var elementTop = $('.landing-page').offset().top;
    //   var elementBottom = elementTop + $('.landing-page').outerHeight();
    //   var viewportTop = $(window).scrollTop();

    //   if(elementBottom > viewportTop+500){
    //     if (window.console){
    //       console.log('foo');
    //     }

    //     if($('#site-header').is(':visible')) {
    //       $([document.documentElement, document.body]).animate({
    //         scrollTop: $('#site-header').offset().top,
    //       }, 1000);
    //     }else {
    //       $([document.documentElement, document.body]).animate({
    //         scrollTop: $('#wrap-container').offset().top,
    //       }, 1000);
    //     }
    //   }

    // });

    var controller = new ScrollMagic.Controller();

    console.log(document.documentElement.clientHeight);

    new ScrollMagic.Scene({
      reverse: false,
      duration: document.documentElement.clientHeight,	// the scene should last for a scroll distance of 100px
      offset: 10,	// start this scene after scrolling for 50px
    })
    .on('enter', function(){
      if($('#site-header').is(':visible')) {
        $([document.documentElement, document.body]).animate({
          scrollTop: $('#site-header').offset().top,
        }, 500);
      }else {
        $([document.documentElement, document.body]).animate({
          scrollTop: $('#wrap-container').offset().top,
        }, 500);
      }
    })
    .addTo(controller); // assign the scene to the controller
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
