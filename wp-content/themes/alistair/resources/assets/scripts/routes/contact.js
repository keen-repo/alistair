import 'leaflet/dist/leaflet';
import * as markerimage from '../../images/map_marker.svg'; //Top-level of the document

export default {
  init() {

    /*function getThemeDir() {

      var global_folder = '/wp-content/themes/alistair/dist/';
      var domain_url = window.location.origin;
      var location = domain_url+global_folder;

      return location;
    }*/

     /* *************************************************************************** */
     var L = require('leaflet');

     // specify the path to the leaflet images folder
     L.Icon.Default.imagePath = 'node_modules/leaflet/dist/images/';

     // initialize the map
     var map = L.map('gmap_canvas', {
       scrollWheelZoom: false,
     });

     // set the position and zoom level of the map
     map.setView([35.902403, 14.456541], 17);

     // set an attribution string
     var attribution = '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors';

     // set the tiles the map will use
     var tiles = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';

     /*var themeDir = getThemeDir();
     console.log(themeDir+'/images/map_marker.svg');*/
     //console.log(markerimage);

     var myIcon = L.icon({
       iconUrl: markerimage,
       iconSize: [38, 95],
     });

     L.marker([35.902403, 14.456541], {icon: myIcon}).addTo(map);

     // create a tileLayer with the tiles, attribution
     var layer = L.tileLayer(tiles, {
       //maxZoom: 17,
       attribution: attribution,
     });

     // add the tile layer to the map
     layer.addTo(map);
   /* *************************************************************************** */

  },

  finalize() {
    // JavaScript to be fired on the woo page, after the init JS
  },
};
