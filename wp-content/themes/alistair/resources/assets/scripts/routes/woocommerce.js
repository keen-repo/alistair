export default {
  init() {
    // JavaScript to be fired on the woo page

    /* OLD CODE... FUNCTIONALITY HAS CHANGED :S
    function giftAddonAddRemove(id, title, price, action){
      console.log(id, title, price, action);
    }

    function giftAddonClick(el, status){
      el.prop('checked', !status);

      // All the below can be used if addon info needs to be displayed on shop

      var action = 'add';
      var id = el.data('id');
      var title = el.data('title');
      var price = el.data('price');

      if(status)
        action = 'remove';

      giftAddonAddRemove(id, title, price, action);

    }

    $('.gifts-slider .item').click(function(){
      var checkbox = $(this).find('input[type="checkbox"]');
      var status = checkbox.prop('checked');

      giftAddonClick(checkbox, status);
    });

    // activate checkbox on pre-selected items
    $('.gifts-slider input[type="checkbox"].is-checked').prop('checked', true);

    */

    // Testimonials Slider
    $('.testimonials ul.items').slick();


    $('.gifts-slider').slick({
      slidesToShow: 4,
      slidesToScroll: 4,
      infinite: false,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
          },
        },
        {
          breakpoint: 500,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });

    // workaroud for custom variation handling
    $(document).on('change', '.single-product .variation input[type="radio"]', function() {
      $('select[name="'+$(this).attr('name')+'"]').val($(this).val()).trigger('change');
    });

    $('.page-description').children('br').first().remove();

  },
  finalize() {
    // JavaScript to be fired on the woo page, after the init JS
  },
};
