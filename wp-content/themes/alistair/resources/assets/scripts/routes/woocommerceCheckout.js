// import Bulma extentions
// bulma steps
import bulmaSteps from 'bulma-steps/dist/js/bulma-steps.min';

export default {
  init() {

    function addDeliveryCharge(chargeOutput, chargeMessage){
      if(jQuery('#order-details-wrap .sunday-delivery').length < 1){
        $('#order-details-wrap').append('<div class="order-details sunday-delivery columns is-marginless">'
          +'<div class="column is-paddingless item-image"></div>'
          +'<div class="column is-paddingless item-details">'
            +'<div class="is-flex">'+chargeMessage+'</div>'
          +'</div>'
          +'<div class="column is-one-fifth is-paddingless has-text-right item-price">'
            +'<div class="is-flex"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">€</span>'+chargeOutput+'</span></div>'
          +'</div>'
        +'</div>');
      } else {
        $('#order-details-wrap .sunday-delivery').html('<div class="column is-paddingless item-image"></div>'
          +'<div class="column is-paddingless item-details">'
            +'<div class="is-flex">'+chargeMessage+'</div>'
          +'</div>'
          +'<div class="column is-one-fifth is-paddingless has-text-right item-price">'
            +'<div class="is-flex"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">€</span>'+chargeOutput+'</span></div>'
          +'</div>');
      }
    }

    // Steps to Checkout
    $('input[type="checkbox"]').click(function () {
      if (this.checked) {

        //console.log($(this).attr('id'));

        if($(this).attr('id') == 'pickUp') {


          /********************************************************************** */
          var date = document.querySelector('[type=date]');
          //console.log('It is a Date: '+date);



          date.addEventListener('input',noSundays);
          console.log('No Mondays: '+date);
          /********************************************************************** */


          $('#formOne').slideDown('fast');
          $('#formTwo').slideUp('fast');
          $('#formThree').slideUp('fast');
          $('#deliveryToMe').attr('checked', false);
          $('#deliveryToReciever').attr('checked', false);
        }
        if($(this).attr('id') == 'deliveryToMe') {
          $('#formTwo').slideDown('fast');
          $('#formOne').slideUp('fast');
          $('#formThree').slideUp('fast');
          $('#pickUp').attr('checked', false);
          $('#deliveryToReciever').attr('checked', false);

        }
        if($(this).attr('id') == 'deliveryToReciever') {
          $('#formThree').slideDown('fast');
          $('#formOne').slideUp('fast');
          $('#formTwo').slideUp('fast');
          $('#deliveryToMe').attr('checked', false);
          $('#pickUp').attr('checked', false);
        }
        // message checkboxex
        if($(this).attr('id') == 'write_message') {
          $('#nocard').attr('checked', false);
          $('#myself').attr('checked', false);
          $('#message').attr('disabled', false);
        }
        if($(this).attr('id') == 'nocard') {
          $('#write_message').attr('checked', false);
          $('#myself').attr('checked', false);
          $('#message').attr('disabled', true);
        }
        if($(this).attr('id') == 'myself') {
          $('#write_message').attr('checked', false);
          $('#nocard').attr('checked', false);
          $('#message').attr('disabled', true);
        }

      } else {
        if($(this).attr('id') == 'pickUp') {
          $('#formOne').slideUp('fast');
        }
        if($(this).attr('id') == 'deliveryToMe') {
          $('#formTwo').slideUp('fast');
        }
        if($(this).attr('id') == 'deliveryToReciever') {
          $('#formThree').slideUp('fast');
        }
      }
    });

    new bulmaSteps(document.getElementById('checkout-steps'), {
      'onShow': function(id){
        // Default state of buttons
        $('#btn_next').show();
        $('#btn_payment').hide();

        //console.log('The Step Id '+id);
        // If the order was submited move to the last step and change the class statuses
        var baseUrl = (window.location).href;

        var urlid = baseUrl.substring(baseUrl.lastIndexOf('=') + 1);
        var hash = urlid.replace('#', '');
        console.log(hash);

        if(urlid && $.isNumeric(urlid)){

          $('.step-item').first().removeClass('is-active');

          if(id == 0) {
            // hide next button and show submit
            $('#btn_prev').hide();
            $('#btn_next').hide();

            $('.step-content').first().removeClass('is-active');
            $('.step-content').last().addClass('is-active');

            // Revolut confirm payment button show thankyou message
            $('#rev_thankyou').hide();
            $('#revolut_button').click(function(){
              $('#rev_instructions').hide();
              $('#rev_thankyou').show();
           });

          }
        } // End of redirect Check

        // Functions on Next button with Validation
        $( '#btn_next a' ).click(function(event) {
          event.stopImmediatePropagation();
          // Cheeking if validation is alredy done (no need to show it more that once)
          if ($('.validation')[0]){
            $('div.validation').remove();
          }

          var stepId = $('.step-item.is-active').data('step-id');
          var step2 = $('input[name=step2]').val();
          console.log('StepNew: '+stepId);
          if(stepId == 0) {
            console.log('On First');
            // we went to far left now only one step next
            /*$('.step-item').first().removeClass('is-active');
            $('.step-item').first().addClass('is-completed');
            $('.step-content').first().removeClass('is-active');

            $('.step-item').eq(1).addClass('is-active');
            $('.step-content').eq(1).addClass('is-active');*/
            step2 = 1;
          }

          //var step2 = $('input[name=step2]').val();
          console.log('AA '+step2);
          // Calling validation function passing the current step id
          validate(id);
          showConfirmmation(step2);

        });

        // Function on Previous button
      $( '#btn_prev a' ).click(function() {
        var stepId = $('.step-item.is-active').data('step-id');
        //console.log('Step ID:'+stepId);
        if(stepId == 1) {
          // go to Step 1 (forms)
          $('.step-item').first().addClass('is-active');
          $('.step-item').first().removeClass('is-completed');
          $('.step-content').first().addClass('is-active');

          // remove from current step
          $('.step-item').eq(1).removeClass('is-active');
          $('.step-item').eq(1).removeClass('is-completed');
          $('.step-content').eq(1).removeClass('is-active');
        }
        if(stepId == 2){
          // go to Step 2 (card message)
          $('.step-item').eq(1).addClass('is-active');
          $('.step-item').eq(1).removeClass('is-completed');
          $('.step-content').eq(1).addClass('is-active');

          // remove from current step
          $('.step-item').eq(2).removeClass('is-active');
          $('.step-item').eq(2).removeClass('is-completed');
          $('.step-content').eq(2).removeClass('is-active');

          $('#btn_next').show();
          $('#btn_payment').hide();
        }

      });

      $('#checkout-proceed-button').click(function(){
        $('.processing-modal').show();
      });

      },

    });

    function showConfirmmation(id) {

      console.log('SHOW CONFIRMATION', id);

      var message = $('#message').val();
      //console.log('Inside ID: '+id);

      if($('#write_message').is(':checked') && message == '') {
        $('#btn_next').show();
        $('#btn_payment').hide();
        //console.log('No message');

      } else {

        if(id == 2){
          console.log('CONFIRM YOUR ORDER');

          var $delivery_type = $('input[type="checkbox"]:checked');

          if( $delivery_type.attr('id') == 'pickUp' ) {
            console.log($delivery_type.attr('id'));

            $('#for_pickup').parent().next().remove();
            $('#pickup_label').hide();
            $('#for_pickup').hide();

            $('#show_other_info').hide();
            $('#show_other_info2').hide();

            var pickup_name = $('#pickup_name').val();
            var pickup_mobile = $('#pickup_mobile').val();
            var pickup_email = $('#pickup_email').val();
            var pickup_date = $('#pickup_date').val();
            var pickup_time = $('#pickup_time').val();

            console.log(pickup_date);
            console.log(pickup_time);
            console.log(pickup_date);
            console.log(message);

            $('#show_pickup_name').html(pickup_name);
            $('#show_pickup_mobile').html(pickup_mobile);
            $('#show_pickup_email').html(pickup_email);
            $('#show_pickup_date').html('Date: '+pickup_date);
            $('#show_pickup_time').html('Time: '+pickup_time);

            // message on card
            message = $('#message').val();
            // Show message on card
            $('#show_pickup_message').html(message);

          }
          if( $delivery_type.attr('id') == 'deliveryToMe' ) {
            console.log($delivery_type.attr('id'));

            $('#show_pickup').hide();
            $('#show_pickup2').hide();

            // hiding pay on collection if is not pick up
            $('#pay_on_collection_radio').css({
              display: 'none !important',
              visibility: 'hidden',
            });

            var billing_name = $('#billing_name').val();
            var billing_mobile = $('#billing_mobile').val();
            var billing_email = $('#billing_email').val();

            var billing_door_number = $('#billing_door_number').val();
            var billing_address1 = $('#billing_address1').val();
            var billing_address2 = $('#billing_address2').val();
            var billing_city = $('#billing_city').val();
            var billing_post_code = $('#billing_post_code').val();
            var billing_region = $('input[name="billing_region"]:checked').val()
            var billing_addr_type = $('#billing_addr_type').val();

            var billing_date = $('#billing_date').val();
            var billing_delivery_time = $('input[name=delivery_time2]:checked').val();

            $('#show_billing_name').html(billing_name);
            $('#show_billing_mobile').html(billing_mobile);
            $('#show_billing_email').html(billing_email);

            $('#show_billing_door_number').html(billing_door_number);
            $('#show_billing_address1').html(billing_address1);
            $('#show_billing_address2').html(billing_address2);
            $('#show_billing_city').html(billing_city);
            $('#show_billing_region').html(billing_region);
            $('#show_billing_post_code').html(billing_post_code);
            $('#show_billing_addr_type').html(billing_addr_type);

            $('#show_billing_date').html('Date: '+billing_date);
            $('#show_billing_time').html('Time: '+billing_delivery_time);

            // Recievers details
            $('#show_reciever_name').html(billing_name);
            $('#show_reciever_mobile').html(billing_mobile);
            $('#show_reciever_email').html(billing_email);

            $('#show_reciever_door_number').html(billing_door_number);
            $('#show_reciever_address1').html(billing_address1);
            $('#show_reciever_address2').html(billing_address2);
            $('#show_reciever_city').html(billing_city);
            $('#show_reciever_region').html(billing_region);
            $('#show_reciever_post_code').html(billing_post_code);
            $('#show_reciever_addr_type').html(billing_addr_type);

            // message on card
            message = $('#message').val();
            // Show message on card
            $('#show_billing_message3').html(message);

          }
          if( $delivery_type.attr('id') == 'deliveryToReciever' ) {
            console.log($delivery_type.attr('id'));
            $('#show_pickup').hide();
            $('#show_pickup2').hide();

            $('#for_pickup').parent().next().remove();
            $('#pickup_label').hide();
            $('#for_pickup').hide();

            // hiding pay on collection if is not pick up
            $('#pay_on_collection_radio').css({
              display: 'none !important',
              visibility: 'hidden',
            });

            billing_name = $('#name3').val();
            billing_mobile = $('#mobile3').val();
            billing_email = $('#email3').val();

            billing_door_number = $('#door_number3').val();
            billing_address1 = $('#address13').val();
            billing_address2 = $('#address23').val();
            billing_city = $('#city3').val();
            billing_region = $('input[name="billing_region_2"]:checked').val();
            billing_post_code = $('#post_code3').val();
            billing_addr_type = $('#addr_type3').val();

            billing_date = $('#rec_date').val();
            billing_delivery_time = $('input[name=delivery_time3]:checked').val();

            // Get recivever info
            var rec_name = $('#rec_name').val();
            var rec_mobile = $('#rec_mobile').val();
            var rec_email = $('#rec_email').val();

            $('#show_billing_name').html(billing_name);
            $('#show_billing_mobile').html(billing_mobile);
            $('#show_billing_email').html(billing_email);

            $('#show_billing_door_number').html(billing_door_number);
            $('#show_billing_address1').html(billing_address1);
            $('#show_billing_address2').html(billing_address2);
            $('#show_billing_city').html(billing_city);
            $('#show_billing_region').html(billing_region);
            $('#show_billing_post_code').html(billing_post_code);
            $('#show_billing_addr_type').html(billing_addr_type);

            $('#show_billing_date').html('Date: '+billing_date);
            $('#show_billing_time').html('Time: '+billing_delivery_time);

            // Recievers details
            $('#show_reciever_name').html(rec_name);
            $('#show_reciever_mobile').html(rec_mobile);
            $('#show_reciever_email').html(rec_email);

            // message on card
            message = $('#message').val();
            $('#show_billing_message3').html(message);

          }

          // Check if chosen date is on a Sunday
          console.log('Billing Date', billing_date);
          if(billing_date !== undefined){
            var bDateArr = billing_date.split('-');

            if(bDateArr[0] !== undefined && bDateArr[1] !== undefined && bDateArr[2] !== undefined) {

              var cartTotal = parseFloat($('.cart-total-wrap #total-amount').text());

              var maltaSundayDelivery = 15;
              var gozoDelivery = 20;
              var gozoSundayDelivery = 30;

              var deliveryCharge = 0;
              var chargeOutput = '';
              var chargeMessage = '';

              var bDate = new Date();
              bDate.setFullYear(parseInt(bDateArr[0]));
              bDate.setMonth(parseInt(bDateArr[1])-1);
              bDate.setDate(parseInt(bDateArr[2]));

              // define public holidays
              var deliveryYear = bDate.getFullYear();
              var publicHolidays = [
                deliveryYear + '-01-01', // new year
                deliveryYear + '-02-10', // st paul
                deliveryYear + '-03-19', // st joseph
                deliveryYear + '-03-31', // freedom day
                deliveryYear + '-05-01', // workers day
                deliveryYear + '-06-07', // sette giugno
                deliveryYear + '-06-29', // st peter and paul
                deliveryYear + '-08-15', // assumption
                deliveryYear + '-09-08', // lady of victories
                deliveryYear + '-09-21', // independence
                deliveryYear + '-12-08', // immaculate conception
                deliveryYear + '-12-13', // republic day
                deliveryYear + '-12-25', // christmas day
              ];

              var exclusions = [
                '2019-05-12', // mother's day
                '2023-04-07', // good friday
                '2023-04-09', //easter
              ];

              if(exclusions.indexOf(billing_date) < 0 && (bDate.getDay() == 0 || publicHolidays.indexOf(billing_date) > -1)){
                // it's a Sunday
                // define region charge
                if(billing_region == 'Gozo'){
                  deliveryCharge = gozoSundayDelivery;
                  chargeMessage = 'Gozo Sunday/Public Holiday Delivery';
                } else if(billing_date=='2022-05-08'){
                  deliveryCharge = 5;
                  chargeMessage = 'Mothers Day Delivery';
                }else{
                  deliveryCharge = maltaSundayDelivery;
                  chargeMessage = 'Sunday/Public Holiday Delivery';
                }

                // add to total
                cartTotal = cartTotal + deliveryCharge;
                chargeOutput = parseFloat(Math.round(deliveryCharge * 100) / 100).toFixed(2);

                // check if delivery charge already exists
                addDeliveryCharge(chargeOutput, chargeMessage);
              } else {
                if(billing_region == 'Gozo'){
                  deliveryCharge = gozoDelivery;
                  cartTotal = cartTotal + deliveryCharge;
                  chargeOutput = parseFloat(Math.round(deliveryCharge * 100) / 100).toFixed(2)
                  chargeMessage = 'Gozo Delivery';
                  addDeliveryCharge(chargeOutput, chargeMessage);
                } else {
                  // just to be sure, let's make sure the delivery charge is not in place
                  if(jQuery('#order-details-wrap .sunday-delivery').length > 0){
                    $('#order-details-wrap .sunday-delivery').remove();
                    deliveryCharge = maltaSundayDelivery
                    cartTotal = cartTotal - deliveryCharge;
                  }
                }
              }
              // update cart total
              $('.cart-total-wrap #total-amount').text(parseFloat(Math.round(cartTotal * 100) / 100).toFixed(2));

            }
          }

          // hide next button and show submit
          $('#btn_next').hide();
          $('#btn_payment').show();
          // $('#form-checkout').on('submit', function() {
          //   $(this).find('input[type=submit]').attr('disabled', 'disabled');
          // });

          $('.gifts-slider').slick({
            slidesToShow: 4,
            slidesToScroll: 4,
            infinite: false,
            responsive: [
              {
                breakpoint: 768,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3,
                },
              },
              {
                breakpoint: 500,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                },
              },
            ],
          });
        } else {
          // we went to far left now only one step next
          $('.step-item').first().removeClass('is-active');
          $('.step-item').first().addClass('is-completed');
          $('.step-content').first().removeClass('is-active');

          $('.step-item').eq(2).removeClass('is-active');
          $('.step-content').eq(2).removeClass('is-active');

          $('.step-item').eq(1).addClass('is-active');
          $('.step-content').eq(1).addClass('is-active');
        } // End of BIG IF

    }// another if to chek


    } // End showConfimation

    function validate($step_id){
      var $delivery_type = $('input[type="checkbox"]:checked');
      var valid = true;


      // Validate Step 1
      if( $step_id == 0 ) {
        //console.log('validating form: '+$delivery_type.attr('id'));
        // Validating Step 1 second form Pick Up
        if($delivery_type.attr('id') == 'pickUp'){

          if($('#pickup_name').val() == ''){
            $('#pickup_name').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }
          if($('#pickup_mobile').val() == '') {
            $('#pickup_mobile').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }
          if($('#pickup_email').val() == '') {
            $('#pickup_email').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }
          if(!isEmail($('#pickup_email').val())){
            $('#pickup_email').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">Please enter a correct email.</div>');
            valid = false;
          }
          if($('#pickup_date').val() == '') {
            $('#pickup_date').parent().parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }
          if($('#pickup_time').val() == 'SELECT TIME') {
            $('#pickup_time').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }

          if($('#pickup_time').is(':disabled')){
            $('#pickup_time').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">Please select another date.</div>');
            valid = false;
          }

          if(valid == true){
            $('.step-item').first().removeClass('is-active');
            $('.step-item').first().addClass('is-completed');
            $('.step-content').first().removeClass('is-active');

            $('.step-item').eq(1).addClass('is-active');
            $('.step-content').eq(1).addClass('is-active');

            $('input[name=step2]').val(2);
          }
        }
        // Validating Step 1 second form Delivery To Me
        if($delivery_type.attr('id') == 'deliveryToMe') {

          if($('#billing_name').val() == '') {
            $('#billing_name').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }
          if($('#billing_mobile').val() == '') {
            $('#billing_mobile').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }
          if($('#billing_email').val() == '' ) {
            $('#billing_email').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }
          if(!isEmail($('#billing_email').val())){
            $('#billing_email').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">Please enter a correct email.</div>');
            valid = false;
          }
          if($('#billing_door_number').val() == '') {
            $('#billing_door_number').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }
          if($('#billing_address1').val() == '') {
            $('#billing_address1').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }
          if($('#billing_city').val() == '') {
            $('#billing_city').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }
          // if($('#billing_post_code').val() == '') {
          //   $('#billing_post_code').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
          //   valid = false;
          // }
          if($('#billing_addr_type').val() == '') {
            $('#billing_addr_type').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }
          if($('#billing_date').val() == '') {
            $('#billing_date').parent().parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }


          // if($('#billing_date').val()!='2020-05-08' && $('#billing_date').val()!='2020-05-09' && $('#billing_date').val()!='2020-05-10' ){
          // // Checking if is morning or aftrenoon
          //   if($('input[name=delivery_time2]:checked').val()) {

          //     var delivert_time = $('input[name=delivery_time2]:checked').val();

          //     var data = [
          //         [0, 4, 'night'],
          //         [5, 11, 'morning'],          //Store messages in an array
          //         [12, 17, 'afternoon'],
          //         [18, 24, 'night'],
          //     ],

          //     hr = new Date().getHours();

          //     for(var i = 0; i < data.length; i++){
          //       if(hr >= data[i][0] && hr <= data[i][1]){
          //           console.log(data[i][2]);
          //           console.log(delivert_time);
          //           if(hr > 11){
          //             $('#timeOfday').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">You selected '+ delivert_time +'. Only Afternoon option is available at this moment. </div>');
          //           }
          //           if(hr > 18){
          //             $('#timeOfday').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">You selected '+ delivert_time +'. Delivery would only be available the next morning (earliest).</div>');
          //           }

          //       }
          //     }
          //   }else{
          //     $('#timeOfday').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">Please select another date.</div>');
          //     valid = false;
          //   } // End of checking if is morning or afternoon or evening
          // }

          if($('input[name=delivery_time2]:checked').val() == '') {

            $('#delivery_time2').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }

          if(valid == true) {
            $('.step-item').first().removeClass('is-active');
            $('.step-item').first().addClass('is-completed');
            $('.step-content').first().removeClass('is-active');

            $('.step-item').eq(1).addClass('is-active');
            $('.step-content').eq(1).addClass('is-active');

            $('input[name=step2]').val(2);
          }
        }
        // Validating Step 1 second form Delivery To Me
        if($delivery_type.attr('id') == 'deliveryToReciever') {

          if($('#name3').val() == '') {
            $('#name3').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }
          if($('#mobile3').val() == '') {
            $('#mobile3').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }
          if($('#email3').val() == '') {
            $('#email3').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }
          if(!isEmail($('#email3').val())){
            $('#email3').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">Please enter a correct email.</div>');
            valid = false;
          }
          if($('#door_number3').val() == '') {
            $('#door_number3').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }
          if($('#address13').val() == '') {
            $('#address13').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }
          if($('#city3').val() == '') {
            $('#city3').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }
          // if($('#post_code3').val() == '') {
          //   $('#post_code3').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
          //   valid = false;
          // }
          /*if($('#addr_type3').val() == '') {
            $('#addr_type3').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }*/
          if($('#rec_date').val() == '') {
            $('#rec_date').parent().parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }
          /*if($('input[name=delivery_time3]:checked').val()  == '') {
            $('#delivery_time3').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }*/
          if($('#rec_name').val() == '') {
            $('#rec_name').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }
          if($('#rec_mobile').val() == '') {
            $('#rec_mobile').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }
          /*if($('#rec_email').val() == '') {
            $('#rec_email').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
            valid = false;
          }*/

          // Checking if is morning or aftrenoon
          // if($('input[name=delivery_time3]:checked').val()) {

          //   var delivert_time2 = $('input[name=delivery_time3]:checked').val();

          //   var data2 = [
          //       [0, 4, 'night'],
          //       [5, 11, 'morning'],          //Store messages in an array
          //       [12, 17, 'afternoon'],
          //       [18, 24, 'night'],
          //   ],

          //   hr2 = new Date().getHours();

          //   for(var i2 = 0; i2 < data2.length; i2++){
          //     if(hr2 >= data2[i2][0] && hr <= data2[i2][1]){
          //         console.log(data2[i2][2]);
          //         console.log(delivert_time2);
          //         if(hr2 > 11){
          //           $('#timeOfday2').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">You choosed '+ delivert_time2 +'. Only Afternoon option is available </div>');
          //         }
          //         if(hr2 > 18){
          //           $('#timeOfday2').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">You choosed '+ delivert_time2 +'. Delivery would only be available the next morning (earliest)</div>');
          //         }

          //     }
          //   }
          // }else{
          //   $('#timeOfday2').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">Please select another date.</div>');
          //   valid = false;
          // } // End of checking if is morning or afternoon or evening


          if(valid == true){
            $('.step-item').first().removeClass('is-active');
            $('.step-item').first().addClass('is-completed');
            $('.step-content').first().removeClass('is-active');

            $('.step-item').eq(1).addClass('is-active');
            $('.step-content').eq(1).addClass('is-active');

            $('input[name=step2]').val(2);
          }
        }
      } // End Step 1

      // Validate Step 2
      if($('#write_message').is(':checked')) {
        var message = $('#message').val();
        if(message == ''){
          $('#message').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');

          $('#btn_next').show();
          $('#btn_payment').hide();

          valid = false;
        }
        if(valid == true){
          $('.step-item').eq(1).removeClass('is-active');
          $('.step-item').eq(1).addClass('is-completed');
          $('.step-content').eq(1).removeClass('is-active');

          $('.step-item').eq(2).addClass('is-active');
          $('.step-content').eq(2).addClass('is-active');

          $('input[name=step3]').val(2);
        }
      }
      if($('#nocard').is(':checked') || $('#myself').is(':checked')) {
        // go to Next step
        if(valid == true){
          $('#message').val('');
          $('.step-item').eq(1).removeClass('is-active');
          $('.step-item').eq(1).addClass('is-completed');
          $('.step-content').eq(1).removeClass('is-active');

          $('.step-item').eq(2).addClass('is-active');
          $('.step-content').eq(2).addClass('is-active');

          $('input[name=step3]').val(2);
        }
      }

      $('input[name=message_card] ').change(function() {
        if ($('.validation')[0]){
          console.log('class exist');
          $('div.validation').remove();
        }
        console.log('Validating Step 2 ');
        if( $(this).is(':checked')) {
          if( $(this).attr('id') == 'write_message') {
            var message = $('#message').val();
            //console.log(message);
            if(message == ''){
              $('#message').parent().after('<div class="validation" style="color:red;margin-bottom: 20px;">This field is required.</div>');
              valid = false;
            }
          }
          if( $(this).attr('id') == 'nocard' || $(this).attr('id') == 'myself' ) {
            console.log('No need to validate');

            $('#show_pickup_message').html('');
            $('#show_billing_message3').html('');
            if ($('.validation')[0]){
              $('div.validation').remove();
            }
            valid = true;
          }
        }

    });

      return valid;
    }

    //check if datepicker is supported, else do jqueryui datepicker
    if ( $('[type="date"]').prop('type') != 'date' ) {
      $('[type="date"]').datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: new Date(),
      });
    }

    //if today is Sunday, minimum delivery date tomorrow
    var today = new Date();
    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);

    var tomorrowString = tomorrow.getFullYear()+'-'+('0'+(tomorrow.getMonth()+1)).slice(-2)+'-'+tomorrow.getDate();
    var todayString = today.getFullYear()+'-'+('0'+(today.getMonth()+1)).slice(-2)+'-'+today.getDate();
    var isSunday = false;
    if(today.getDay() == 0){
      isSunday = true;
      $('#billing_date').attr('min', tomorrowString);
      $('#rec_date').attr('min', tomorrowString);
    }

    $('#billing_region_gozo, #billing_region_malta').click(function(){
      var region = $(this).val();
      if(!isSunday){
        if(region == 'Gozo' && !isSunday){
          $('#billing_date').attr('min', tomorrowString);

          if($('#billing_date').val() == todayString){
            $('#billing_date').val(tomorrowString);
          }
        }else {
          $('#billing_date').attr('min', todayString);
        }

        var date = $('#billing_date').val();
        var dateInDate = new Date(date);

        //check if today
        if(today.toDateString() == dateInDate.toDateString()){
          //remove time options and add anytime
          $('#me-deliver-control').empty();
          $('#me-deliver-control').append('<label class="radio"><input id="billing_delivery_time" type="radio" name="delivery_time2" value="Anytime" checked="checked"> ANYTIME</label>')
        }else {
          $('#me-deliver-control').empty();
          $('#me-deliver-control').append('<label class="radio"><input id="billing_delivery_time" type="radio" name="delivery_time2" value="Morning" checked="checked"> MORNING</label><label class="radio"><input id="billing_delivery_time" type="radio" name="delivery_time2" value="Afternoon"> AFTERNOON</label>');
        }
      }
    });

    $('#billing_region_gozo_2, #billing_region_malta_2').click(function(){
      var region = $(this).val();

      if(!isSunday){
        if(region == 'Gozo'){
          $('#rec_date').attr('min', tomorrowString);

          if($('#rec_date').val() == todayString){
            $('#rec_date').val(tomorrowString);
          }
        }else {
          $('#rec_date').attr('min', todayString);
        }

        var date = $('#rec_date').val();
        var dateInDate = new Date(date);

        //check if today
        if(today.toDateString() == dateInDate.toDateString()){
          //remove time options and add anytime
          $('#rec-deliver-control').empty();
          $('#rec-deliver-control').append('<label class="radio"><input type="radio" name="delivery_time3" value="Anytime" checked="checked"> ANYTIME</label>');
        }else {
          $('#rec-deliver-control').empty();
          $('#rec-deliver-control').append('<label class="radio"><input type="radio" name="delivery_time3" value="Morning" checked="checked"> MORNING</label><label class="radio"><input type="radio" name="delivery_time3" value="Afternoon"> AFTERNOON</label>');
        }
      }

    });


  // Workaround for HTML5 date type
  function noSundays(e){
      var day = new Date( e.target.value ).getUTCDay();
      var month = new Date( e.target.value ).getMonth();
      var dateDay = new Date(e.target.value).getDate();

      // Days in JS range from 0-6 where 0 is Sunday and 6 is Saturday
      if((month == 1) && (dateDay == 14)) {
        //check if Valentine's day
        console.log('Exception');
        //disable times
        // $('#pickup_time option:eq(5)').attr('disabled','disabled');
        // $('#pickup_time option:eq(6)').attr('disabled','disabled');
        // $('#pickup_time option:eq(7)').attr('disabled','disabled');
        // $('#pickup_time option:eq(8)').attr('disabled','disabled');
        // $('#pickup_time option:eq(9)').attr('disabled','disabled');
        // $('#pickup_time option:eq(10)').attr('disabled','disabled');
        // $('#pickup_time option:eq(11)').attr('disabled','disabled');

      } else if(month < 10){
        if( day == 0 ){
            e.target.setCustomValidity('OH NOES! We hate Sundays! Please pick any day but Sunday.');
            console.log('OH NOES! We hate Sundays! Please pick any day but Sunday.');
            e.target.value = '';
        } else {
            e.target.setCustomValidity('');
        }

        $('#pickup_time option:eq(5)').removeAttr('disabled');
        $('#pickup_time option:eq(6)').removeAttr('disabled');
        $('#pickup_time option:eq(7)').removeAttr('disabled');
        $('#pickup_time option:eq(8)').removeAttr('disabled');
        $('#pickup_time option:eq(9)').removeAttr('disabled');
        $('#pickup_time option:eq(10)').removeAttr('disabled');
        $('#pickup_time option:eq(11)').removeAttr('disabled');
      }
  }

  $('#pickup_date').change(function(){
    var date = $(this).val();

    //check if date is disabled
    $('.disabled-warning').hide();
    $('#pickup_time').show();
    $('#pickup_time option:contains("9:00 AM")').prop('disabled', false);
    $('#pickup_time option:contains("10:00 AM")').prop('disabled', false);
    $('#pickup_time option:contains("11:00 AM")').prop('disabled', false);
    $('#pickup_time option:contains("12:00 PM")').prop('disabled', false);
    $('#pickup_time option:contains("1:00 PM")').prop('disabled', false);
    $('#pickup_time option:contains("2:00 PM")').prop('disabled', false);
    $('#pickup_time option:contains("3:00 PM")').prop('disabled', false);
    $('#pickup_time option:contains("4:00 PM")').prop('disabled', false);
    $('#pickup_time option:contains("5:00 PM")').prop('disabled', false);
    $('#pickup_time option:contains("6:00 PM")').prop('disabled', false);
    $('#pickup_time option:contains("7:00 PM")').prop('disabled', false);
    $('#pickup_time').prop('disabled', false);
    var disabledDates = $('#disabled-dates').data('disableddates');
    var morningDisabled = false;
    var afternoonDisabled = false;
    disabledDates.forEach(disabledDate => {
      if(disabledDate.crb_disabled_date_date == date) {
        var dateDisabled = false;
        disabledDate.crb_disabled_date_time.forEach(element => {
          if(element == 'anytime'){
            //disable all items
            dateDisabled = true;
            $('.disabled-warning').show();
            $('#pickup_time').prop('disabled', 'disabled');
          }

          if(element == 'morning') {
            $('#pickup_time option:contains("9:00 AM")').prop('disabled', true);
            $('#pickup_time option:contains("10:00 AM")').prop('disabled', true);
            $('#pickup_time option:contains("11:00 AM")').prop('disabled', true);
            $('.disabled-warning').show();
            morningDisabled = true;
          }

          if(element == 'afternoon') {
            $('#pickup_time option:contains("12:00 PM")').prop('disabled', true);
            $('#pickup_time option:contains("1:00 PM")').prop('disabled', true);
            $('#pickup_time option:contains("2:00 PM")').prop('disabled', true);
            $('#pickup_time option:contains("3:00 PM")').prop('disabled', true);
            $('#pickup_time option:contains("4:00 PM")').prop('disabled', true);
            $('#pickup_time option:contains("5:00 PM")').prop('disabled', true);
            $('#pickup_time option:contains("6:00 PM")').prop('disabled', true);
            $('#pickup_time option:contains("7:00 PM")').prop('disabled', true);
            $('.disabled-warning').show();
            afternoonDisabled = true;
          }
        });

        if(dateDisabled) {
          $('#rec-deliver-control').empty();
        }
      }
    });

    if(morningDisabled && afternoonDisabled){
      $('#pickup_time option:eq(0)').html('ANYTIME');
      $('#pickup_time option:eq(0)').prop('selected', true);
    }else {
      $('#pickup_time option:eq(0)').html('SELECT TIME');
    }
  });

  $('#billing_date').change(function(){
    var date = $(this).val();

    var dateInDate = new Date(date);

    //check if today
    if(today.toDateString() == dateInDate.toDateString()){
      //remove time options and add anytime
      $('#me-deliver-control').empty();
      $('#me-deliver-control').append('<label class="radio"><input id="billing_delivery_time" type="radio" name="delivery_time2" value="Anytime" checked="checked"> ANYTIME</label>');

      //check if after 4pm
      var d = new Date();
      if(d.getHours() >= 16) {
        $('.delivery-warning').show();
      }
    }else {
      $('#me-deliver-control').empty();
      $('#me-deliver-control').append('<label class="radio"><input id="billing_delivery_time" type="radio" name="delivery_time2" value="Morning" checked="checked"> MORNING</label><label class="radio"><input id="billing_delivery_time" type="radio" name="delivery_time2" value="Afternoon"> AFTERNOON</label>');
      $('.delivery-warning').hide();
    }

    //check if date is disabled
    $('.disabled-warning').hide();
    var disabledDates = $('#disabled-dates').data('disableddates');
    disabledDates.forEach(disabledDate => {
      if(disabledDate.crb_disabled_date_date == date) {
        var anytimeDisabled = false;
        var morningDisabled = false;
        var afternoonDisabled = false;
        disabledDate.crb_disabled_date_time.forEach(element => {
          if(element == 'anytime'){
            anytimeDisabled = true;
          }

          if(element == 'morning') {
            morningDisabled = true;
          }

          if(element == 'afternoon') {
            afternoonDisabled = true;
          }
        });

        if(morningDisabled) {
          $('#me-deliver-control input[value="Morning"]').parent().remove();
          $('.disabled-warning').show();
        }

        if(afternoonDisabled) {
          $('#me-deliver-control input[value="Afternoon"]').parent().remove();
          $('.disabled-warning').show();
        }

        if(afternoonDisabled && morningDisabled) {
          $('#me-deliver-control').empty();
          $('#me-deliver-control').append('<label class="radio"><input id="billing_delivery_time" type="radio" name="delivery_time2" value="Anytime" checked="checked"> ANYTIME</label>');
          $('.disabled-warning').show();
        }

        if(morningDisabled && afternoonDisabled && anytimeDisabled) {
          $('#me-deliver-control').empty();
          $('.disabled-warning').show();
        }
      }
    });

    // if(date=='2020-05-08' || date=='2020-05-09' || date=='2020-05-10'){
    //   if(!$('#mothers-day-msg').length){
    //     $('#me-deliver-control').after('<p id="mothers-day-msg">Due to how busy we are during mothers day, deliveries will be done from 8am till 7pm.</p>');
    //   }else{
    //     $('#mothers-day-msg').show();
    //   }
    //   $('#me-deliver-control').hide();
    // }else {
    //   $('#me-deliver-control').show();
    //   $('#mothers-day-msg').hide();
    // }
  });

  $('#rec_date').change(function(){
    var date = $(this).val();

    var dateInDate = new Date(date);

    //check if today
    if(today.toDateString() == dateInDate.toDateString()){
      //remove time options and add anytime
      $('#rec-deliver-control').empty();
      $('#rec-deliver-control').append('<label class="radio"><input id="billing_delivery_time" type="radio" name="delivery_time3" value="Anytime" checked="checked"> ANYTIME</label>')
    }else {
      $('#rec-deliver-control').empty();
      $('#rec-deliver-control').append('<label class="radio"><input id="billing_delivery_time" type="radio" name="delivery_time3" value="Morning" checked="checked"> MORNING</label><label class="radio"><input id="billing_delivery_time" type="radio" name="delivery_time3" value="Afternoon"> AFTERNOON</label>');
    }

    //check if date is disabled
    $('.disabled-warning').hide();
    var disabledDates = $('#disabled-dates').data('disableddates');
    disabledDates.forEach(disabledDate => {
      if(disabledDate.crb_disabled_date_date == date) {
        var anytimeDisabled = false;
        var morningDisabled = false;
        var afternoonDisabled = false;
        disabledDate.crb_disabled_date_time.forEach(element => {
          if(element == 'anytime'){
            anytimeDisabled = true;
          }

          if(element == 'morning') {
            morningDisabled = true;
          }

          if(element == 'afternoon') {
            afternoonDisabled = true;
          }
        });

        if(morningDisabled) {
          $('#rec-deliver-control input[value="Morning"]').parent().remove();
          $('.disabled-warning').show();
        }

        if(afternoonDisabled) {
          $('#rec-deliver-control input[value="Afternoon"]').parent().remove();
          $('.disabled-warning').show();
        }

        if(afternoonDisabled && morningDisabled) {
          $('#rec-deliver-control').empty();
          $('#rec-deliver-control').append('<label class="radio"><input id="billing_delivery_time" type="radio" name="delivery_time2" value="Anytime" checked="checked"> ANYTIME</label>');
          $('.disabled-warning').show();
        }

        if(morningDisabled && afternoonDisabled && anytimeDisabled) {
          $('#rec-deliver-control').empty();
          $('.disabled-warning').show();
        }
      }
    });

    if(date=='2020-05-08' || date=='2020-05-09' || date=='2020-05-10'){
      if(!$('#mothers-day-msg2').length){
        $('#rec-deliver-control').after('<p id="mothers-day-msg2">Due to how busy we are during mothers day, deliveries will be done from 8am till 7pm.</p>');
      }else {
        $('#mothers-day-msg2').show();
      }
      $('#rec-deliver-control').hide();
    }else {
      $('#rec-deliver-control').show();
      $('#mothers-day-msg2').hide();
    }
  });

  function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }

  },


    finalize() {
      // JavaScript to be fired on the woo page, after the init JS
    },
  };
