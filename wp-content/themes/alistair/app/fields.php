<?php

namespace App;

use Carbon_Fields\Container;
use Carbon_Fields\Field;
use Carbon_Fields\Block;

/**
 * Define custom fields
 * Docs: https://carbonfields.net/docs/
 */
add_action( 'carbon_fields_register_fields', function () {
    // Custom Carbon fields.
    /*Container::make( 'post_meta', 'Page Layout' )
        ->where( 'post_type', '=', 'page' )
        ->add_fields( array(
            Field::make( 'text', 'sage_greeting', 'Greeting' )
        ));*/

    /* ************************************************************************************** */
    // Custom Carbon Blocks.
    /* ************************************************************************************** */
    // Sliding Hero
    Block::make( __( 'Sliding Hero' ) )
    ->add_fields( array(
        Field::make( 'complex', 'crb_slides', 'Sliding Hero' )
            ->set_layout( 'tabbed-horizontal' )
            ->add_fields( array(
                Field::make( 'text', 'heading', 'Heading Title' ),
                Field::make( 'text', 'sub_heading', 'Sub Heading Title' ),
                Field::make( 'checkbox', 'crb_white_heading', 'White Heading' )->set_option_value( 'yes' ),
                Field::make( 'image', 'image', 'Image' ),
                Field::make( 'checkbox', 'crb_white_filter', 'Has White Filter' )->set_option_value( 'yes' ),
                Field::make( 'textarea', 'crb_texarea', 'Hero Text' ),
                Field::make( 'checkbox', 'crb_has_button', 'Has Button' )->set_option_value( 'yes' ),
                Field::make( 'text', 'btn_text', 'Button Text' ),
                Field::make( 'text', 'btn_url', 'Button URL (without domain)' ),
            ) ),
    ) )
    ->set_icon( 'images-alt2' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        $slides = $block['crb_slides'];
        ?>
        <section class="hero is-large alignfull">
            <div class="hero-slick-carousel">
                <?php foreach ( $slides as $slide ) {
                    $image = wp_get_attachment_image_src( $slide['image'], 'hero' );
                    $btn_link = site_url().$slide['btn_url'];
                    if(isset($slide['crb_white_heading'])){
                        $white_text = $slide['crb_white_heading'];
                    }else{
                        $white_text = false;
                    }
                ?>
                    <div style="background-image:url('<?php echo $image[0]; ?>');" class="slide-item">
                        <div class="siding-hero-overlay-none">
                            <div class="hero-body">
                                <div class="container">
                                    <h1 class="title">
                                        <span class="<?php if($white_text){ echo "has-text-white"; }else{ echo "has-text-black"; } ?>"><?php echo $slide['heading']; ?></span>
                                    </h1>
                                    <h2 class="subtitle <?php if($white_text){ echo "has-text-white"; }else{ echo "has-text-black"; } ?>">
                                        <?php  echo $slide['sub_heading']; ?>
                                    </h2>
                                    <?php if($slide['crb_texarea']){ ?>
                                    <h3 class="hero-subtitle"><?php echo $slide['crb_texarea']; ?></h3>
                                    <?php } ?>
                                    <?php if($slide['crb_has_button']){ ?>
                                    <br>
                                    <a href="<?php echo $btn_link; ?>" class="button is-normal is-radiusless btn-color-primary"><?php echo $slide['btn_text']; ?></a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </section>
        <?php
    } );

    // Static Hero
    Block::make( __( 'Static Hero' ) )
        ->add_fields( array(
            Field::make( 'text', 'heading', 'Heading Title' ),
            Field::make( 'text', 'sub_heading', 'Sub Heading Title' ),
            Field::make( 'checkbox', 'crb_white_heading', 'White Heading' )->set_option_value( 'yes' ),
            Field::make( 'image', 'image', 'Image' ),
            Field::make( 'checkbox', 'crb_white_filter', 'Has White Filter' )->set_option_value( 'yes' ),
            Field::make( 'textarea', 'crb_texarea', 'Hero Text' ),
            Field::make( 'checkbox', 'crb_has_button', 'Has Button' )->set_option_value( 'yes' ),
            Field::make( 'text', 'btn_text', 'Button Text' ),
            Field::make( 'text', 'btn_url', 'Button URL' ),
        )
   )
    ->set_icon( 'format-image' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        $image = wp_get_attachment_image_src( $block['image'], 'hero' );
        if(isset($block['crb_white_heading'])){
            $white_text = $block['crb_white_heading'];
        }else{
            $white_text = false;
        }
        ?>
        <section class="hero is-medium alignfull" style="background-image:url('<?php echo $image[0]; ?>'); background-size: cover;background-position: center center; ">
            <div class="hero-body <?php if($block['crb_white_filter']){ echo "white-filter"; } ?>">
                <div class="container">
                    <?php if(isset($block['sub_heading'])){ ?>
                    <?php if($block['sub_heading']){ ?>
                    <h2 class="title">
                        <span><?php echo $block['sub_heading']; ?></span>
                    </h2>
                    <?php } ?>
                    <?php } ?>
                    <h1 class="hero-title <?php if($white_text){ echo "has-text-white"; } ?>" ><?php echo $block['heading']; ?></h1>
                    <?php if($block['crb_texarea']){ ?>
                    <h3 class="hero-subtitle"><?php echo $block['crb_texarea']; ?></h3>
                    <?php } ?>
                    <?php if($block['crb_has_button']){ ?>
                    <br>
                    <a href="<?php echo $block['btn_url']; ?>" class="button is-normal is-radiusless btn-color-primary has-text-white" style="background-color: #965250 !important"><?php echo $block['btn_text']; ?></a>
                    <?php } ?>
                </div>
            </div>
        </section>
        <?php
    } );

    // Headings (with option to display subheading before or after)
    Block::make( __( 'Add Headings' ) )
        ->add_fields( array(
                Field::make( 'text', 'id', 'ID' ),
                Field::make( 'text', 'heading', 'Heading Title' ),
                Field::make( 'text', 'sub_heading', 'Sub Heading Title' ),
                Field::make( 'checkbox', 'crb_flip_headings', 'Flip Titles' )->set_option_value( 'yes' ),
                Field::make( 'radio', 'crb_htype', 'H Type' )->add_options( array(
                    'h2' => 'H2',
                    'h1' => 'H1',
                ))->set_default_value(
                    'h2'
                ),
            )
        )
        ->set_icon( 'editor-textcolor' )
        ->set_preview_mode( true )
        ->set_render_callback( function ( $block ) {
            $flip = $block['crb_flip_headings'];
            $htype = $block['crb_htype'];
            if(!array_key_exists('id', $block)){
                $block['id'] = "";
            }
            if($flip == true){
            ?>
                <<?php echo $htype; ?> id="<?php if($block['id']){ echo $block['id']; } ?>" class="section-title has-text-centered">
                <span>
                    <?php echo $block['sub_heading']; ?>
                </span>
                <?php echo $block['heading']; ?>
                </<?php echo $htype; ?>>
            <?php
            } else { ?>
                <<?php echo $htype; ?> id="<?php if($block['id']){ echo $block['id']; } ?>" class="section-title has-text-centered">
                <?php echo $block['heading']; ?>
                <span>
                    <?php echo $block['sub_heading']; ?>
                </span>
                </<?php echo $htype; ?>>
            <?php
            }

        } );


    // Sliding Products
    $productsArr = [];
    $products_from_option = get_option('all_products');

    foreach( $products_from_option as $product ){
        $productsArr[$product->ID] = $product->post_title;
    }

    Block::make( __( 'Sliding Products' ) )
    ->add_fields( array(
        Field::make( 'multiselect', 'crb_sliding_products', 'Products List' )
            ->add_options( $productsArr ),
    ) )
    ->set_icon( 'image-flip-horizontal' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        $products = $block['crb_sliding_products'];
        ?>
        <div class="woocommerce">
            <section class="sliding products">
            <?php woocommerce_product_loop_start(); ?>
                <?php foreach ( $products as $product ) : ?>
                    <?php
                        $post_object = get_post( $product );

                        setup_postdata( $GLOBALS['post'] =& $post_object );

                        wc_get_template_part( 'content', 'product' ); 
                    ?>
                <?php endforeach; ?>
            <?php woocommerce_product_loop_end(); ?>
            </section>
        </div>
        <?php
    } );

    // Sliding Products By Category

    $categories = get_terms();
    $categoriesArr = array();
    foreach($categories as $category){
        if($category->taxonomy == "product_cat"){
            $categoriesArr[$category->term_id] = $category->name;
        }
    }

    Block::make( __( 'Sliding Products By Category' ) )
    ->add_fields( array(
        Field::make( 'select', 'crb_sliding_products_categories', 'Products List' )
            ->add_options( $categoriesArr ),
    ) )
    ->set_icon( 'image-flip-horizontal' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        $category = $block['crb_sliding_products_categories'];

        $args = array(
            'post_type' => 'product',
            'numberposts' => 6,
            'tax_query' => array(
                array(
                    'taxonomy'  => 'product_cat',
                    'field'     => 'term_id',
                    'terms'     => $category
                ),
            ),
        );
        $products = get_posts($args);
        ?>
        <div class="woocommerce">
        <section class="sliding products">
        <?php woocommerce_product_loop_start(); ?>
                <?php foreach ( $products as $product ) : ?>
                    <?php
                        $post_object = get_post( $product );

                        setup_postdata( $GLOBALS['post'] =& $post_object );

                        wc_get_template_part( 'content', 'product' );
                    ?>
                <?php endforeach; ?>
            <?php woocommerce_product_loop_end(); ?>
        </section>
        </div>
        <?php
    } );

    Block::make( __( 'Custom Hand-Picked Products' ) )
    ->add_fields( array(
        Field::make( 'multiselect', 'crb_hand_picked_products', 'Products List' )
            ->add_options( $productsArr ),
    ) )
    ->set_icon( 'image-screenoptions' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        $products = $block['crb_hand_picked_products'];
        ?>
        <div class="woocommerce">
            <section class="hand-picked-products">
            <?php woocommerce_product_loop_start(); ?>
                <?php foreach ( $products as $product ) : ?>
                    <?php
                        $post_object = get_post( $product );

                        setup_postdata( $GLOBALS['post'] =& $post_object );

                        wc_get_template_part( 'content', 'product' ); // this needs to be replaced with actual code and not a template include
                    ?>
                <?php endforeach; ?>
            <?php woocommerce_product_loop_end(); ?>
            </section>
        </div>
        <?php
    } );

    // Timeline
    Block::make( __( 'Timeline' ) )
    ->add_fields( array(
        Field::make( 'complex', 'crb_timeline_items', 'Timeline Items' )
            ->set_layout( 'tabbed-horizontal' )
            ->add_fields( array(
                Field::make( 'image', 'image', 'Image' ),
                Field::make( 'text', 'heading', 'Heading' ),
                Field::make( 'textarea', 'content', 'Content' ),
            ) ),
    ) )
    ->set_icon( 'clock' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        $items = $block['crb_timeline_items'];
        ?>
        <section class="timeline">
            <ul class="timeline-items">
            <?php $liIndex = 1; ?>
            <?php foreach( $items as $i=>$item ){ ?>
                <li class="item colums timeline-item_<?php echo $liIndex; $liIndex++; ?> <?= $i%2 == 0 ? 'even' : 'odd'; ?>" data-aos="ade-<?= $i%2 == 0 ? 'left' : 'right'; ?>" data-aos-duration="600" data-aos-dela="<?php echo $i*50; ?>">
                    <?php if(!empty($item['image'])){ ?>
                        <div class="colum image">
                            <?php $image = wp_get_attachment_image_src( $item['image'], 'medium' ); ?>
                            <div class="clip" style="background-image: url('<?= $image[0]; ?>')"></div>
                        </div>
                    <?php } ?>
                    <div class="column text <?= empty($item['image']) ? 'has-text-centered' : '' ;?>">
                        <div class="heading"><?= $item['heading']; ?></div>
                        <div class="content"><?= nl2br($item['content']); ?></div>
                    </div>
                </li>
            <?php } ?>
            </ul>
        </section>
        <div class="clear"></div>
        <?php
    } );

    // Text / Image Blocks
    Block::make( __( 'Text and Image blocks' ) )
    ->add_fields( array(
        Field::make( 'complex', 'crb_text_image_block_items', 'Block Items' )
            ->set_layout( 'tabbed-horizontal' )
            ->add_fields( array(
                Field::make( 'radio', 'type', 'Type' )
                    ->set_options(array(
                        'text' => 'Text',
                        'link' => 'Link'
                    )),
                Field::make( 'text', 'heading', 'Heading' ),
                Field::make( 'text', 'subheading', 'Sub Heading' ),
                Field::make( 'image', 'image', 'Image' ),
                Field::make( 'text', 'text', 'Text' ),
                Field::make( 'text', 'url', 'URL' ),
            ) ),
    ) )
    ->set_icon( 'feedback' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        ?>
        <section class="alignfull">
            <div class="text-image-blocks columns is-marginless is-multiline faeture feautered_boxes">
                <?php foreach( $block['crb_text_image_block_items'] as $item ){
                    if($item['type'] == 'link'){
                        $image = wp_get_attachment_image_src($item['image'], 'large');
                    } else {
                        $image = array('');
                    }
                    ?>
                    <div class="column is-half-desktop button-border <?php echo $item['type']; ?>" style="background-image: url('<?php echo $image[0]; ?>')">
                        <div class="content-wrap is-flex">
                            <?php if($item['type'] == 'text'){ ?>
                                <h2 class="section-title"><?php echo $item['heading']; ?><span><?php echo $item['subheading']; ?></span></h2>
                            <?php } else { ?>
                                <a href="<?php echo $item['url']; ?>" class="button is-normal is-borderless is-radiusless btn-color-medium-light"><?php echo $item['text']; ?></a>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </section>
        <?php
    } );


    // Alternating Text and Images
    Block::make( __( 'Alt. Text and Images' ) )
    ->add_fields( array(
        Field::make( 'complex', 'crb_alt_text_images', 'Items' )
            ->set_layout( 'tabbed-horizontal' )
            ->add_fields( array(
                Field::make( 'image', 'alt_image', 'Image' ),
                Field::make( 'text', 'alt_heading', 'Heading' ),
                Field::make( 'textarea', 'alt_content', 'Content' ),
            ) ),
    ) )
    ->set_icon( 'align-left' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        $items = $block['crb_alt_text_images'];
        ?>
        <section class="alt-text-images">
            <div class="overlay"></div>
            <div class="items">
            <?php foreach( $items as $i=>$item ){ ?>
                <?php $image = wp_get_attachment_image_src( $item['alt_image'], 'large' ); ?>
                <div class="item columns is-gapless is-multiline <?= $i%2 == 0 ? 'even' : 'odd'; ?>">
                    <div class="column">
                        <div class="text">
                            <h2><?= $item['alt_heading']; ?></h2>
                            <div class="content"><?= nl2br($item['alt_content']); ?></div>
                        </div>
                    </div>
                    <div class="column image" style="background-image: url('<?= $image[0]; ?>')"></div>
                </div>
            <?php } ?>
            </div>
        </section>
        <?php
    } );

    // Subscriptions Alternating Text and Images
    Block::make( __( 'Subscriptions Alt. Text and Images' ) )
    ->add_fields( array(
        Field::make( 'complex', 'crb_subscription_alt_text_images', 'Items' )
            ->set_layout( 'tabbed-horizontal' )
            ->add_fields( array(
                Field::make( 'image', 'alt_image', 'Image' ),
                Field::make( 'text', 'alt_step', 'Step' ),
                Field::make( 'text', 'alt_heading', 'Heading' ),
                Field::make( 'textarea', 'alt_content', 'Content' ),
            ) ),
    ) )
    ->set_icon( 'align-left' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        $items = $block['crb_subscription_alt_text_images'];
        ?>
        <section class="alt-text-images">
            <div class="overlay"></div>
            <div class="items">
            <?php foreach( $items as $i=>$item ){ ?>
                <?php $image = wp_get_attachment_image_src( $item['alt_image'], 'large' ); ?>
                <div class="item columns is-gapless is-multiline <?= $i%2 == 0 ? 'even' : 'odd'; ?>">
                    <div class="column">
                        <div class="text">
                            <p class="step has-text-centered"><?= $item['alt_step']; ?></p>
                            <h2 class="has-text-centered"><?= $item['alt_heading']; ?></h2>
                            <div class="content has-text-centered"><?= nl2br($item['alt_content']); ?></div>
                        </div>
                    </div>
                    <div class="column image" style="background-image: url('<?= $image[0]; ?>')"></div>
                </div>
            <?php } ?>
            </div>
        </section>
        <?php
    } );

    // Subscriptions boxes
    Block::make( __( 'Subscriptions Boxes' ) )
    ->set_icon( 'align-left' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        $args = array(
            'post_type'      => 'product',
            'posts_per_page' => -1,
            'product_cat'    => 'flower-subscriptions'
        );
        $subscriptions = get_posts( $args );
        ?>

        <div class="columns subscriptions is-multiline">
            <?php foreach($subscriptions as $subscription){ ?>
                <div class="column is-one-third">
                    <div class="subscription-image" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id($subscription->ID)) ?>'); background-size: cover;background-position: center center; "></div>
                    <div class="columns subscription-info">
                        <div class="column"><p><?php echo $subscription->post_title; ?></p></div>
                        <div class="column"><p class="price"><?php echo "€".wc_get_product($subscription->ID)->get_variation_price('min') ; ?></p></div>
                    </div>
                    <div class="has-text-centered">
                        <a class="subscribe-button" href="<?php echo get_permalink($subscription->ID); ?>">SUBSCRIBE</a>
                    </div>
                </div>
            <?php } ?>
        </div>
        
        <?php
    } );

    // Venues in Malta
    Block::make( __( 'Venues' ) )
    ->add_fields( array(
        Field::make( 'image', 'crb_malta_image', 'Malta Button Image'),
        Field::make( 'text', 'crb_malta_text', 'Malta Button Text'),
        Field::make( 'image', 'crb_gozo_image', 'Gozo Button Image'),
        Field::make( 'text', 'crb_gozo_text', 'Gozo Button Text'),
        Field::make( 'complex', 'crb_venues_malta', 'Venues in Malta' )
            ->set_layout( 'tabbed-horizontal' )
            ->add_fields( array(
                Field::make( 'image', 'venue_image', 'Image' ),
                Field::make( 'text', 'venue_name', 'Name' ),
                Field::make( 'text', 'venue_location', 'Location' ),
                Field::make( 'text', 'venue_description', 'Description' ),
                Field::make( 'text', 'venue_website', 'Website' ),
            ) ),
        Field::make( 'complex', 'crb_venues_gozo', 'Venues in Gozo' )
        ->set_layout( 'tabbed-horizontal' )
        ->add_fields( array(
            Field::make( 'image', 'venue_image', 'Image' ),
            Field::make( 'text', 'venue_name', 'Name' ),
            Field::make( 'text', 'venue_location', 'Location' ),
            Field::make( 'text', 'venue_description', 'Description' ),
            Field::make( 'text', 'venue_website', 'Website' ),
        ) ),
    ) )
    ->set_icon( 'align-left' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        $malta_image = $block['crb_malta_image'];
        $malta_text = $block['crb_malta_text'];
        $gozo_image = $block['crb_gozo_image'];
        $gozo_text = $block['crb_gozo_text'];
        $items = $block['crb_venues_malta'];

        ?>

        <section class="feautered_boxes has-text-centered has-margin-top-100 has-margin-bottom-100">
            <div class="columns is-marginless is-centered is-multiline">
                <div class="column">
                    <div class="hero" style="background-image: url('<?php echo wp_get_attachment_url($malta_image) ?>'); background-size: cover;background-position: center center; " onclick="change(0)" >
                        <div class="box-wrap has-height-350">
                            <div class="box-ratio has-wide-ratio">
                                <div class="box-content">
                                <div class="box-elements is-flex button-border">
                                    <div class="button is-normal is-borderless is-radiusless btn-color-medium-light">Venues in Malta</div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <p class="is-size-7 has-text-weight-semibold has-padding-30"><?php echo $malta_text ?></p>
                </div>

                <div class="column">
                    <div class="hero" style="background-image: url('<?php echo wp_get_attachment_url($gozo_image) ?>'); background-size: cover;background-position: center center; " onclick="change(1)" >
                        <div class="box-wrap has-height-350">
                            <div class="box-ratio has-wide-ratio">
                                <div class="box-content">
                                <div class="box-elements is-flex button-border">
                                    <div class="button is-normal is-borderless is-radiusless btn-color-medium-light">Venues in Gozo</div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="is-size-7 has-text-weight-semibold has-padding-30"><?php echo $gozo_text ?></p>
                </div>
            </div>
        </section>
        <div id="venues" class="venues has-bg-green-medium-light alignfull">
            <div id="venues-malta" class="column is-10 is-offset-1">
                <h1 class="section-title has-text-centered has-padding-80">
                    Venues in Malta
                        <span>
                        ~ EXPLORE VENUES ~
                        </span>
                    </h1>
                <div class="columns">
                    <div class="column">
                        <section class="has-padding-10">
                            <div class="items">
                            <?php foreach( $items as $i=>$item ){ ?>
                                <?php $image = wp_get_attachment_image_src( $item['venue_image'], 'large' ); ?>
                                <div class="item columns is-gapless is-multiline is-marginless <?= $i%2 == 0 ? 'even' : 'odd'; ?>">
                                    <div class="column has-background-white	">
                                        <div class="text has-padding-100-desktop has-padding-50">
                                            <p class="is-size-6 has-text-weight-bold is-uppercase"><?= $item['venue_name']; ?></p>
                                            <p class="is-size-7 has-text-weight-semibold"><?= $item['venue_location']; ?></p>
                                            <div class="content is-size-7 has-text-weight-semibold"><?= nl2br($item['venue_description']); ?></div>
                                            <p class="">
                                                <a href="<?= $item['venue_website']; ?>" class="button is-normal is-borderless is-radiusless btn-color-medium-light" title="Visit Website" style="white-space: nowrap !important;">
                                                    <?= $item['venue_name']; ?>
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="column image has-min-height-350" style="background-image: url('<?= $image[0]; ?>'); background-repeat: no-repeat; background-size: cover;"></div>
                                </div>
                            <?php } ?>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
            <?php


            $items_gozo = $block['crb_venues_gozo'];
            ?>
            <div id="venues-gozo" class="column is-10 is-offset-1">
                <h1 class="section-title has-text-centered has-padding-80">
                    Venues in Gozo
                        <span>
                        ~ EXPLORE VENUES ~
                        </span>
                    </h1>

                <div class="columns">
                    <div class="column">
                        <section class="has-padding-10">
                            <div class="items">
                            <?php foreach( $items_gozo as $i=>$item_gozo ){ ?>
                                <?php $image = wp_get_attachment_image_src( $item_gozo['venue_image'], 'large' ); ?>
                                <div class="item columns is-gapless is-multiline is-marginless <?= $i%2 == 0 ? 'even' : 'odd'; ?>">
                                    <div class="column has-background-white">
                                        <div class="text has-padding-100">
                                            <p class="is-size-6 has-text-weight-bold is-uppercase"><?= $item_gozo['venue_name']; ?></p>
                                            <p class="is-size-7 has-text-weight-semibold"><?= $item_gozo['venue_location']; ?></p>
                                            <div class="content is-size-7 has-text-weight-semibold"><?= nl2br($item['venue_description']); ?></div>
                                            <p class="is-size-7 has-text-weight-bold">
                                                <a href="<?= $item_gozo['venue_website']; ?>" class="button is-normal is-borderless is-radiusless btn-color-medium-light" title="Visit Website" style="white-space: nowrap !important;">
                                                    <?= $item_gozo['venue_name']; ?>
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="column image has-min-height-350" style="background-image: url('<?= $image[0]; ?>')"></div>
                                </div>
                            <?php } ?>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function change(selectedPage) {

                if(selectedPage == 0){
                    document.getElementById('venues-malta').style.display = 'block';
                    document.getElementById('venues-gozo').style.display = 'none';
                }else if(selectedPage == 1){
                    document.getElementById('venues-malta').style.display = 'none';
                    document.getElementById('venues-gozo').style.display = 'block';
                }

                var venues_div = document.getElementById("venues");
                venues_div.scrollIntoView({ block: 'start',  behavior: 'smooth' });
            }
        </script>

        <?php
    } );

    // Testimonials
    Block::make( __( 'Testimonials' ) )
    ->add_fields( array(
        Field::make( 'complex', 'crb_testimonials', 'Items' )
            ->set_layout( 'tabbed-horizontal' )
            ->add_fields( array(
                Field::make( 'text', 'test_name', 'Name' ),
                Field::make( 'textarea', 'test_content', 'Content' ),
                Field::make( 'checkbox', 'has_logo', 'Has Logo')->set_option_value('yes')->set_default_value( true ),
                Field::make( 'image', 'company_logo', 'Company Logo' ),
            ) ),
    ) )
    ->set_icon( 'format-quote' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        $items = $block['crb_testimonials'];
        ?>
        <section class="testimonials columns is-marginless">
            <ul class="items column is-four-fifths ">
            <?php foreach( $items as $i=>$item ){ ?>
                <li class="item has-text-centered">
                    <?php if($item['has_logo']==true) { ?>
                    <div class="has-margin-bottom-50">
                    <div class="is-relative has-height-150 has-width-150 profile-image-circle" style="background-image: url(<?php echo wp_get_attachment_url($item['company_logo']) ?>); background-size: cover;background-position: center center; background-size: contain; background-repeat: no-repeat; "></div>
                    </div>
                    <?php } ?>
                    <div class="test-content"><?= $item['test_content']; ?></div>
                    <div class="test-name">- <?= $item['test_name']; ?> -</div>
                </li>
            <?php } ?>
            </ul>
        </section>
        <?php
    } );

    // Testimonials with Filters
    Block::make( __( 'Testimonials with Filters' ) )
    ->set_icon( 'format-quote' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        
        // $items = $block['crb_testimonials'];

        $args = array(
            'post_type'=> 'testimonial',
            'numberposts' => -1,
            );

        $testimonials = get_posts($args);
        $categories = get_categories(array('taxonomy' => 'testimonial_category'));

        ?>
        <div class="columns column is-full is-centered is-hidden-mobile">
            <?php
                foreach($categories as $category){ ?>
                    <div class="column is-narrow is-uppercase is-clickable has-text-weight-semibold has-text-centered" onclick="showMenuContents('<?php echo $category->slug ?>')"><?php echo $category->name ?></div>
                <?php }
            ?>
        </div>
        <section id="testimonials" class="testimonials columns is-marginless">

            <?php
                foreach($categories as $category){
                ?>
                <ul class="items column is-three-fifths is-offset-one-fifth" id="<?php echo $category->slug ?>">

                    <?php foreach( $testimonials as $testimonial ){
                        //get testimonial category
                        $testimonial_category = get_the_terms($testimonial, 'testimonial_category');
                        if($testimonial_category[0]->slug == $category->slug){ ?>
                            <li class="item has-text-centered">
                                <div class="test-content"><?php echo carbon_get_post_meta($testimonial->ID,'testimonial_text') ?></div>
                                <div class="test-name">- <?php echo carbon_get_post_meta($testimonial->ID,'testimonial_client_name') ?> -</div>
                            </li>
                        <?php }
                        ?>
                    <?php } ?>
                </ul>
            <?php } ?>

        </section>

        <div class="columns is-centered is-hidden-desktop is-hidden-tablet has-text-centered has-margin-bottom-50">
            <a class="column wp-block-button__link has-background no-border-radius is-inline-block" href="<?=get_permalink(get_page_by_path('testimonials')) ?>">VIEW ALL TESTIMONIALS</a>
        </div>

        <script>
            window.onload = setTimeout(function(){
                var allCategories = document.getElementById("testimonials").children;
                for(i = 0; i<allCategories.length; i++){
                    if(i != 0){
                        allCategories[i].style.display = "none";
                    }
                }
            }, 3000);

            function showMenuContents(selectedCategory) {

                var allCategories = document.getElementById("testimonials").children;
                for(i = 0; i<allCategories.length; i++){
                    allCategories[i].style.display = "none";
                }

                document.getElementById(selectedCategory).style.display = "block";

            }
        </script>
        <?php
    } );


    //Latest blog posts
    $post_types = get_post_types();

    Block::make( __( 'Latest Posts Selectable' ) )
    ->add_fields( array(
        Field::make( 'select', 'crb_post_type', 'Post Type' )
            ->add_options( $post_types ),
    ) )
    ->set_icon( 'format-quote' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        $post_type = $block['crb_post_type'];
        ?>
        <div class="columns is-centered has-padding-top-100 latest-posts">
            <div class="columns column is-multiline">
            <?php
                $args = array(
                'numberposts' => 3,
                'offset' => 0,
                'category' => 0,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'include' => '',
                'exclude' => '',
                'meta_key' => '',
                'meta_value' =>'',
                'post_type' => $post_type,
                'post_status' => 'publish',
                'suppress_filters' => true
                );

                $recent_posts = wp_get_recent_posts( $args, ARRAY_A );

                foreach($recent_posts as $post){
                if($post['ID'] !== get_the_ID()) {?>
                <li class="column is-one-third is-marginless">
                    <a href="<?php echo get_permalink($post['ID']) ?>"  class="featured-image" style="background-image: url(<?php echo get_the_post_thumbnail_url($post['ID']) ?>); background-size: cover;background-position: center center; ">
                    <div class="button">
                        READ MORE
                    </div>
                    </a>
                    <p class="date is-marginless has-text-centered is-size-7 has-padding-30 has-text-weight-semibold	"><?php echo date('j.n.Y',strtotime($post['post_date'])) ?></p>
                    <p class="montserrat is-marginless is-size-6 has-text-centered has-text-weight-bold"><?php echo $post['post_title'] ?></p>
                    <p class="has-margin-top-30 has-max-height-100 is-size-7 has-text-centered has-text-weight-normal"><?php echo wp_trim_words(wp_strip_all_tags($post['post_content']),15,NULL)  ?></p>
                </li>
                <?php } }
            ?>
            </div>
        </div>
        <?php
    } );


    //Latest Wedding Blogs

    Block::make( __( 'Latest Wedding Blogs' ) )
    ->set_icon( 'format-quote' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        ?>
        <div class="columns is-centered has-padding-top-100 latest-posts">
            <div id="latest-wedding-slider" class="columns column is-multiline">
            <?php
                $args = array(
                'numberposts' => -1,
                'offset' => 0,
                'category' => 0,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'include' => '',
                'exclude' => '',
                'meta_key' => '',
                'meta_value' =>'',
                'post_type' => 'blog',
                'post_status' => 'publish',
                'suppress_filters' => true,
                );

                $recent_posts = wp_get_recent_posts( $args, ARRAY_A );

                $index = 0;

                foreach($recent_posts as $post){
                $terms = get_the_terms($post['ID'], "blog_category");
                $is_wedding = false;
                if($terms){
                    foreach($terms as $term){
                        if($term->slug == "weddings"){
                            $is_wedding = true;
                        }else {
                            $is_wedding = false;
                        }
                    }
                }

                if(($post['ID'] !== get_the_ID()) && ($is_wedding == true) && ($index < 3)) { $index++;?>
                <?php
                    $blog_preview_image = carbon_get_post_meta($post['ID'], 'blog_preview_image');
                ?>
                <li class="column is-one-third is-marginless">
                    <a href="<?php echo get_permalink($post['ID']) ?>"  class="featured-image" style="background-image: url(<?php echo wp_get_attachment_url($blog_preview_image) ?>); background-size: cover;background-position: center center; ">
                    <div class="button">
                        READ MORE
                    </div>
                    </a>
                    <p class="date is-marginless has-text-centered is-size-7 has-padding-30 has-text-weight-semibold	"><?php echo date('j.n.Y',strtotime($post['post_date'])) ?></p>
                    <p class="montserrat is-marginless is-size-6 has-text-centered has-text-weight-bold"><?php echo $post['post_title'] ?></p>
                    <p class="has-margin-top-30 has-max-height-100 is-size-7 has-text-centered has-text-weight-normal"><?php echo wp_trim_words(wp_strip_all_tags($post['post_content']),15,NULL)  ?></p>
                </li>
                <?php } }
            ?>
            </div>
        </div>
        <?php
    } );


    // Feautured Boxes
    Block::make( __( 'Feutured Boxes' ) )
    ->add_fields( array(
        Field::make( 'radio', 'crb_columns', 'Columns' )->add_options( array(
            'c2' => '2 columns',
            'c3' => '3 columns',
        ))->set_default_value(
            'c2'
        ),
        Field::make('checkbox', 'crb_no_padding', 'No Padding')->set_option_value('yes'),
        Field::make( 'complex', 'crb_feautered_boxes', 'Feutured Boxes' )
            ->set_layout( 'tabbed-horizontal' )
            ->add_fields( array(
                Field::make( 'image', 'image', 'Image' ),
                Field::make( 'text', 'btn_text', 'Button Text' ),
                Field::make( 'text', 'btn_url', 'Button URL' ),
                Field::make( 'checkbox', 'crb_wide', 'Wide size' )->set_option_value( 'yes' ),
                Field::make( 'checkbox', 'crb_wide_ratio', 'Wide ratio' )->set_option_value( 'yes' ),
                Field::make( 'checkbox', 'crb_wide_ratio_larger', 'Wide ratio larger height' )->set_option_value( 'no' ),
                Field::make( 'checkbox', 'crb_has_button', 'Has Button' )->set_option_value( 'yes' )->set_default_value( true ),
                Field::make( 'checkbox', 'crb_hide_button', 'Hide Button on Start' )->set_option_value( 'yes' )->set_default_value( false ),
                Field::make( 'checkbox', 'crb_button_no_bg', 'Button No Background' )->set_option_value( 'yes' )->set_default_value( false ),
                Field::make( 'checkbox', 'crb_button_height', 'Button Small Height' )->set_option_value( 'yes' )->set_default_value( true ),
            ) ),
    ) )
    ->set_icon( 'screenoptions' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        $have_columns = $block['crb_columns'];
        $feautered_boxes = $block['crb_feautered_boxes'];

        ?>
        <section class="feautered_boxes has-text-centered ">

            <div class="columns is-marginless is-centered is-multiline">
                <?php foreach ($feautered_boxes as $f_box) {
                    $image = wp_get_attachment_image_src( $f_box['image'], 'large', false );
                    // Only for tow columns selected
                    if($have_columns == 'c2'){
                        $size = 'is-half';
                    } else {
                        $size ='is-4';
                    }
                    if($f_box['crb_wide'] == true){
                        $size ='is-two-thirds';

                        if(array_key_exists('crb_wide_ratio_larger', $f_box)){
                            if($f_box['crb_wide_ratio_larger'] == true){
                                $wide_ratio = 'has-wide-ratio has-larger-height';
                            }else{
                                $wide_ratio = 'has-wide-ratio';
                            }
                        }else {
                            $f_box['crb_wide_ratio_larger'] = false;
                            $wide_ratio = "has-wide-ratio";
                        }
                    } else {
                        $wide_ratio = '';
                    }

                    if(array_key_exists('crb_wide_ratio', $f_box)){
                        if(($f_box['crb_wide_ratio'] == true)){
                            if($f_box['crb_wide_ratio_larger'] == true){
                                $wide_ratio = 'has-wide-ratio has-larger-height';
                            }else{
                                $wide_ratio = 'has-wide-ratio';
                            }
                        }
                    }
                    
                    

                    if(array_key_exists('crb_hide_button', $f_box)){
                        if(($f_box['crb_hide_button'] == true)){
                            $hide_button = 'btn-hide';
                        }else {
                            $hide_button = 'btn-show';
                        }
                    }else{
                        $hide_button = 'btn-show';
                    }
                    
                    if(array_key_exists('crb_button_no_bg', $f_box)){
                        if($f_box['crb_button_no_bg'] == true){
                            $button_style = " button_no_bg ";
                        }else{
                            $button_style = "";
                        }
                    }else {
                        $f_box['crb_button_no_bg'] = null;
                        $button_style = ""; 
                    }

                    if(!isset($button_style)){
                        $button_style = "";
                    }

                    if(!array_key_exists('crb_no_padding', $block)){
                        $block['crb_no_padding'] = null;
                    }

                    if(!array_key_exists('crb_button_height', $f_box)){
                        $f_box['crb_button_height'] = null;
                    }

                ?>

                    <div class="column <?php echo $size; if($block['crb_no_padding']){ echo ' is-paddingless'; }?>">
                        <div class="hero" style="background-image: url('<?= $image[0]; ?>'); background-size: cover;background-position: center center; " onclick="window.location='<?php echo $f_box['btn_url']; ?>';" >
                            <div class="box-wrap">
                                <div class="box-ratio <?php echo $wide_ratio; ?>">
                                    <div class="box-content">
                                    <?php if($f_box['crb_has_button']){ ?>
                                        <div class="box-elements is-flex button-border <?php echo $button_style ?>">
                                            <a href="<?php echo $f_box['btn_url']; ?>" class=" <?php if($f_box['crb_button_no_bg'] == false){echo "button is-normal is-borderless is-radiusless btn-color-medium-light";} ?>  <?php echo $hide_button ?> <?php if($f_box['crb_button_height']){ echo "small-height"; } ?>"><?php echo $f_box['btn_text']; ?></a>
                                        </div>
                                    <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php } ?>
            </div>

        </section>
        <?php
    } );

    // Shop by Collection
    Block::make( __( 'Shop by Collection' ) )
    ->add_fields( array(
        Field::make( 'complex', 'occasions', 'Feutured Boxes' )
            ->set_layout( 'tabbed-horizontal' )
            ->add_fields( array(
                Field::make( 'image', 'image', 'Image' ),
                Field::make( 'text', 'text', 'Title' ),
                Field::make( 'text', 'url', 'URL' ),
            ) ),
    ) )
    ->set_icon( 'screenoptions' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        $occasions = $block['occasions'];
        ?>
        <div class="shop-by-occasion">
            <?php foreach($occasions as $occasion){ ?>
                <a href="<?php echo $occasion["url"]; ?>" class="occasion" style="background-image: url('<?php echo wp_get_attachment_url($occasion["image"]); ?>'); background-size: cover; background-position: center;">
                    <span class="occasion-title has-text-white	has-text-centered is-size-2 has-text-weight-semibold has-margin-bottom-10"><?php echo $occasion["text"]; ?></span>
                    <span class="shop-now has-text-centered is-marginless is-size-6 has-text-weight-semibold has-text-white ">SHOP NOW</span>
                </a>
            <?php } ?>
        </div>
        <?php
    } );

    //Type of Flowers
    Block::make( __( 'Type of Flowers' ) )
    ->add_fields( array(
        Field::make('complex', 'crb_flowers_pages', 'Type of Flowers')
            ->set_layout('tabbed-horizontal')
            ->add_fields( array(
                Field::make('text', 'crb_heading', 'Heading' ),
                Field::make('rich_text', 'crb_description', 'Description'),
                Field::make( 'complex', 'crb_flowers', 'Flowers' )
                    ->set_layout( 'tabbed-horizontal' )
                    ->add_fields( array(
                        Field::make( 'image', 'crb_flower_image', 'Flower Image' ),
                        Field::make( 'text', 'crb_flower_name', 'Flower Name' ),
                        Field::make( 'text', 'crb_flower_description', 'Flower Description' ),
                )),
            )),
    ) )
    ->set_icon( 'screenoptions' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        $types_of_flowers = $block['crb_flowers_pages'];
        $selected_page = 0;
        $number_of_types = sizeof($types_of_flowers);
        ?>
        <section class="flower-pages has-text-centered">
            <div class="columns column is-10 is-offset-1 has-padding-top-50 is-multiline is-centered">
                <div class="column is-uppercase is-clickable has-text-weight-semibold flex-item-no-overflow is-narrow" onclick="showMenuContents(-1, <?php echo $number_of_types ?>)"><p class="column is-uppercase">ALL</p></div>
                <?php
                    foreach ($types_of_flowers as $key=>$type){
                        $heading = $type['crb_heading'];
                ?>
                <div class="column is-uppercase is-clickable has-text-weight-semibold flex-item-no-overflow is-narrow" onclick="showMenuContents(<?php echo $key ?>, <?php echo $number_of_types ?>)"><p class="column is-uppercase"><?php echo $heading ?></p></div>
                <?php } ?>
            </div>

            <div>
                <?php
                    foreach ($types_of_flowers as $key=>$type){
                        $heading = $type['crb_heading'];
                        $description = $type['crb_description'];
                        $flowers = $type['crb_flowers'];
                ?>
                <div id="flower-type-<?php echo $key ?>" class="has-padding-50">
                    <h1 class=""><?php echo $heading ?></h1>
                    <p class=""><?php echo $description ?></p>

                    <div class="columns is-marginless is-centered is-multiline has-padding-top-50">
                        <?php
                            foreach($flowers as $flower){
                        ?>
                            <div class="column is-one-third has-padding-bottom-100">
                                <img class="has-height-200" src="<?= wp_get_attachment_image_src($flower['crb_flower_image'])[0]; ?>" />
                                <p class="is-uppercase has-text-weight-semibold has-padding-top-30 has-padding-bottom-30"><?php echo $flower['crb_flower_name'] ?></p>
                                <p><?php echo $flower['crb_flower_description'] ?></p>
                            </div>

                        <?php } ?>
                    </div>
                    <div class="is-divider column"></div>
                </div>
                <?php } ?>
            </div>


        </section>

        <script>
            function showMenuContents(selectedPage, numberOfTypes) {

                var i = 0;
                if(selectedPage == -1){
                    for(i = 0; i < numberOfTypes; i++){
                        document.getElementById('flower-type-'+i.toString()).style.display = 'block';
                    }
                }else{
                    for (i = 0; i < numberOfTypes; i++) {
                    if(i !== selectedPage){
                        document.getElementById('flower-type-'+i.toString()).style.display = 'none';
                    }

                    document.getElementById('flower-type-'+selectedPage.toString()).style.display = 'block';
                    }
                }
            }
        </script>

        <?php
    } );

    //Vendors
    Block::make( __( 'Vendors' ) )
    ->add_fields( array(
        Field::make('complex', 'crb_vendor_pages', 'Vendors')
            ->set_layout('tabbed-horizontal')
            ->add_fields( array(
                Field::make('text', 'crb_vendor_category', 'Vendor Category' ),
                Field::make( 'complex', 'crb_vendors', 'Vendors' )
                    ->set_layout( 'tabbed-horizontal' )
                    ->add_fields( array(
                        Field::make( 'image', 'crb_vendor_image', 'Vendor Image' ),
                        Field::make( 'text', 'crb_vendor_name', 'Vendor Name' ),
                        Field::make( 'rich_text', 'crb_vendor_description', 'Vendor Description' ),
                )),
            )),
    ) )
    ->set_icon( 'screenoptions' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        $vendor_categories = $block['crb_vendor_pages'];
        $selected_page = 0;
        $number_of_categories = sizeof($vendor_categories);
        ?>
        <section class="vendor-pages has-text-centered">
            <div id="vendor-menu" class="columns has-padding-top-50">
                <div class="columns column is-10-desktop is-offset-1 has-padding-top-50 is-multiline is-centered">
                    <div class="column is-uppercase is-clickable has-text-weight-semibold flex-item-no-overflow is-narrow" onclick="showMenuContents(-1, <?php echo $number_of_categories ?>)"><p>ALL</p></div>
                    <?php
                        foreach ($vendor_categories as $key=>$vendor_category){
                            $heading = $vendor_category['crb_vendor_category'];
                    ?>
                    <div class="column is-uppercase is-clickable has-text-weight-semibold flex-item-no-overflow is-narrow" onclick="showMenuContents(<?php echo $key ?>, <?php echo $number_of_categories ?>)"><p><?php echo $heading ?></p></div>
                    <?php } ?>
                </div>
            </div>

            <div class="columns has-padding-bottom-50">
                <div class="column">
                    <?php
                        foreach ($vendor_categories as $key=>$vendor_category){
                            $heading = $vendor_category['crb_vendor_category'];
                            $vendors = $vendor_category['crb_vendors'];
                    ?>
                    <div id="vendor-category-<?php echo $key ?>" class="has-padding-top-50">
                        <h1 class=""><?php echo $heading ?></h1>

                        <div class="columns is-desktop is-multiline ">
                            <?php
                                foreach($vendors as $vendor){
                            ?>
                                <div class="column is-half-desktop ">
                                    <div class="columns is-mobile has-bg-green-medium-light has-margin-10">
                                        <div class="column is-half" style="background-image: url('https://amp.insider.com/images/5bce3ad1c7621910294c7655-750-563.jpg'); background-size: cover;background-position: center center; ">
                                        </div>
                                        <div class="column is-half background-color-green-medium-light has-padding-50 has-text-left flex-vert-center has-text-black">
                                            <p class="is-uppercase has-text-weight-bold has-text-left"><?php echo $vendor['crb_vendor_name'] ?></p>
                                            <div class="has-text-left has-text-weight-medium lh-10 is-size-7">
                                                <p class=""><?php echo $vendor['crb_vendor_description'] ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>

        </section>

        <script>
            function showMenuContents(selectedPage, numberOfCategories) {

                var menu_div = document.getElementById("vendor-menu");
                menu_div.scrollIntoView({ block: 'start',  behavior: 'smooth' });

                var i = 0;
                if(selectedPage == -1){
                    for(i = 0; i < numberOfCategories; i++){
                        document.getElementById('vendor-category-'+i.toString()).style.display = 'block';
                    }
                }else{
                    for (i = 0; i < numberOfCategories; i++) {
                    if(i !== selectedPage){
                        document.getElementById('vendor-category-'+i.toString()).style.display = 'none';
                    }

                    document.getElementById('vendor-category-'+selectedPage.toString()).style.display = 'block';
                    }
                }
            }
        </script>

        <?php
    } );

    //Wedding Planners & Organisers
    Block::make( __( 'Planners Organisers' ) )
    ->add_fields( array(
        Field::make('complex', 'crb_planners_organisers_pages', 'Planners & Organisers')
            ->set_layout('tabbed-horizontal')
            ->add_fields( array(
                Field::make('text', 'crb_planners_organisers_category', 'Planners & Organisers Category' ),
                Field::make( 'complex', 'crb_planners_organisers', 'Planners & Organisers' )
                    ->set_layout( 'tabbed-horizontal' )
                    ->add_fields( array(
                        Field::make( 'image', 'crb_planners_organisers_image', 'Planners & Organisers Image' ),
                        Field::make( 'text', 'crb_planners_organisers_name', 'Planners & Organisers Name' ),
                        Field::make( 'rich_text', 'crb_planners_organisers_description', 'Planners & Organisers Description' ),
                )),
            )),
    ) )
    ->set_icon( 'screenoptions' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        $planners_organisers_categories = $block['crb_planners_organisers_pages'];
        $selected_page = 0;
        $number_of_categories = sizeof($planners_organisers_categories);
        ?>
        <section class="planners-organisers-pages has-text-centered">
            <?php if($number_of_categories > 1){ ?>
            <div id="planners-organisers-menu" class="columns has-padding-top-50">
                <div class="columns column is-10-desktop is-offset-1 has-padding-top-50 is-multiline is-centered">
                    <div class="column is-uppercase is-clickable has-text-weight-semibold flex-item-no-overflow is-narrow" onclick="showMenuContents(-1, <?php echo $number_of_categories ?>)"><p>ALL</p></div>
                    <?php
                        foreach ($planners_organisers_categories as $key=>$planners_organisers_category){
                            $heading = $planners_organisers_category['crb_planners_organisers_category'];
                    ?>
                    <div class="column is-uppercase is-clickable has-text-weight-semibold flex-item-no-overflow is-narrow" onclick="showMenuContents(<?php echo $key ?>, <?php echo $number_of_categories ?>)"><p><?php echo $heading ?></p></div>
                    <?php } ?>
                </div>
            </div>
            <?php } ?>

            <div class="columns has-padding-bottom-50">
                <div class="column is-10-desktop is-offset-1">
                    <?php
                        foreach ($planners_organisers_categories as $key=>$planners_organisers_category){
                            $heading = $planners_organisers_category['crb_planners_organisers_category'];
                            $planners_organisers = $planners_organisers_category['crb_planners_organisers'];
                    ?>
                    <div id="planners-organisers-category-<?php echo $key ?>" class="has-padding-top-50">
                        <?php if(!empty($heading)){ ?>
                        <h1 class=""><?php echo $heading ?></h1>
                        <?php } ?>

                        <div class="columns is-desktop is-marginless is-centered is-multiline ">
                            <?php
                                foreach($planners_organisers as $planner_organiser){
                            ?>
                                <div class="column is-full-desktop has-bg-green-medium-light has-margin-10">
                                    <div class="columns is-marginless has-min-height-250">
                                        <div class="column is-quarter has-min-height-200" style="background-image: url('<?php echo wp_get_attachment_url($planner_organiser['crb_planners_organisers_image']) ?>'); background-size: cover;background-position: center center; ">
                                        </div>
                                        <div class="column is-three-quarters background-color-green-medium-light has-padding-50 has-text-left flex-vert-center has-text-black">
                                            <p class="is-uppercase has-text-weight-bold has-text-left"><?php echo $planner_organiser['crb_planners_organisers_name'] ?></p>
                                            <div class="has-text-left has-text-weight-medium lh-10 is-size-7">
                                                <p class=""><?php echo $planner_organiser['crb_planners_organisers_description'] ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>

        </section>

        <script>
            function showMenuContents(selectedPage, numberOfCategories) {

                var menu_div = document.getElementById("planners-organisers-menu");
                menu_div.scrollIntoView({ block: 'start',  behavior: 'smooth' });

                var i = 0;
                if(selectedPage == -1){
                    for(i = 0; i < numberOfCategories; i++){
                        document.getElementById('planners-organisers-category-'+i.toString()).style.display = 'block';
                    }
                }else{
                    for (i = 0; i < numberOfCategories; i++) {
                    if(i !== selectedPage){
                        document.getElementById('planners-organisers-category-'+i.toString()).style.display = 'none';
                    }

                    document.getElementById('planners-organisers-category-'+selectedPage.toString()).style.display = 'block';
                    }
                }
            }
        </script>

        <?php
    } );


    // Sliding text
    Block::make( __( 'Sliding text' ) )
    ->add_fields( array(
        Field::make( 'complex', 'crb_sliding_text', 'Sliding Text' )
            ->set_layout( 'tabbed-horizontal' )
            ->add_fields( array(
                Field::make( 'image', 'image', 'Image' ),
                Field::make( 'textarea', 'crb_content', 'Content' ),
            ) ),
    ) )
    ->set_icon( 'image-flip-horizontal' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        $sliding_text = $block['crb_sliding_text'];
        ?>

    <section class="sliding_text">
        <div class=" is-centered">
            <div class="sliding-text-slick">
                <?php foreach ($sliding_text as $s_text) {
                    $image = wp_get_attachment_image_src( $s_text['image'], 'small' );
                ?>
                    <div class="column is-one-quarter has-text-centered">
                        <figure class="image is-64x64">
                            <img src="<?php echo $image[0]; ?>">
                        </figure>
                        <?php echo $s_text['crb_content']; ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
        <?php
    } );

    // Standing Order
    Block::make( __( 'Standing Order' ) )
    ->add_fields( array(
        Field::make( 'image', 'image', 'Image' ),
        Field::make( 'text', 'heading', 'Heading' ),
        Field::make( 'text', 'subtitle', 'Subtitle' ),
        Field::make( 'text', 'btn_text', 'Button text' ),
        Field::make( 'text', 'btn_url', 'Button URL' ),
        Field::make( 'complex', 'list', 'List' )
            ->set_layout( 'tabbed-horizontal' )
            ->add_fields( array(
                Field::make( 'text', 'title', 'Title'),
                Field::make( 'text', 'content', 'Content'),
            )),
    ) )
    ->set_icon( 'image-flip-horizontal' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        $image = $block['image'];
        $heading = $block['heading'];
        $subtitle = $block['subtitle'];
        $btn_text = $block['btn_text'];
        $btn_url = $block['btn_url'];
        $list = $block['list'];
        ?>

        <div class="standing-order-block columns alignfull" style="background-color: #f8fbf9">
            <div class="column is-half is-paddingless">
                <img src="<?php echo wp_get_attachment_url($image) ?>" alt="" style="object-fit: cover; min-width: 100%; min-height: 100%; height: 100%;">
            </div>
            <div class="column is-half has-padding-100 has-padding-50-mobile">
                <h2 class="section-title has-text-left"><?php echo $heading ?>
                    <span><?php echo $subtitle ?></span>
                </h2>
                <div class="has-padding-top-25 has-padding-bottom-25">
                    <?php foreach($list as $list_item){ ?>
                        <div class="columns">
                            <div class="column is-narrow">
                                <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28">
                                    <path id="Path_532" data-name="Path 532" d="M56.667,42.667a14,14,0,1,0,14,14A14,14,0,0,0,56.667,42.667ZM54.1,63.859l-6.164-6.164,2.055-2.055,4.11,4.11L63.345,50.5,65.4,52.557Z" transform="translate(-42.667 -42.667)" fill="#965250"/>
                                </svg></div>
                            <div class="column">
                                <p class="is-marginless" style="color: #965250"><?php echo $list_item["title"] ?></p>
                                <p class="is-marginless"><?php echo $list_item["content"] ?></p>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <a href="<?php echo $btn_url ?>" class="button is-normal is-radiusless btn-color-primary has-text-white" style="background-color: #965250 !important"><?php echo $btn_text ?></a>
            </div>
        </div>

        <?php
    } );

    // Out clients
    Block::make( __( 'Clients' ) )
    ->add_fields( array(
        Field::make( 'complex', 'crb_clients', 'Clients' )
            ->set_layout( 'tabbed-horizontal' )
            ->add_fields( array(
                Field::make( 'image', 'image', 'Image' ),
                Field::make( 'text', 'crb_company_name', 'Company Name' ),
                Field::make( 'textarea', 'crb_description', 'Description' ),
            ) ),
    ) )
    ->set_icon( 'image-flip-horizontal' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        $clients = $block['crb_clients'];
        ?>
        <div id="" class="columns is-multiline is-marginless is-centered">
        <?php
        foreach($clients as $client){
            //$image = wp_get_attachment_image_src( $client['image'], 'small' );
            $image = wp_get_attachment_image_src( $client['image'], 'large' );
            // var_dump($image); die();
        ?>

            <li class="column is-one-quarter has-padding-30">
                <div>
                    <div class="is-relative has-height-150 has-width-150 profile-image-circle" style="background-image: url('<?= $image[0]; ?>'); background-size: contain;background-position: center center; background-repeat: no-repeat; "></div>
                    <!-- <img src="<?= $image[0]?>" alt=""> -->
                </div>
                <p class="has-padding-top-40 is-uppercase has-text-weight-semibold has-text-centered"><?php echo $client['crb_company_name'] ?></p>
                <p class="has-text-centered has-padding-10 is-clipped"><?php echo $client['crb_description'] ?></p>
            </li>

        <?php } ?>
        </div>
        <?php
    } );

    // Sliding boxes
    Block::make( __( 'Sliding Boxes' ) )
    ->add_fields( array(
        Field::make( 'complex', 'crb_sliding_boxes', 'Sliding Boxes' )
            ->set_layout( 'tabbed-horizontal' )
            ->add_fields( array(
                Field::make( 'image', 'image', 'Image' ),
                Field::make( 'text', 'btn_text', 'Button Text' ),
                Field::make( 'text', 'btn_url', 'Button URL' ),
            ) ),
    ) )
    ->set_icon( 'screenoptions' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        $sliding_boxes = $block['crb_sliding_boxes'];
        ?>

    <section class="sliding_text feautered_boxes">
        <div class=" is-centered">
            <div class="sliding-text-slick">
                <?php foreach ($sliding_boxes as $f_box) {
                    $image = wp_get_attachment_image_src( $f_box['image'], 'small' );
                ?>
                    <div class="column is-4">
                        <div class="hero" style="background-image: url('<?= $image[0]; ?>'); background-size: cover;background-position: center center; " onclick="window.location='<?php echo $f_box['btn_url']; ?>';" >
                            <div class="box-wrap">
                                <div class="box-ratio">
                                    <div class="box-content">
                                        <div class="box-elements is-flex button-border">
                                            <a href="<?php echo $f_box['btn_url']; ?>" class="button is-normal is-borderless is-radiusless btn-color-medium-light"><?php echo $f_box['btn_text']; ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
        <?php
    } );

    $post_types = get_post_types();

    // Sliding posts
    Block::make( __( 'Sliding Posts' ) )
    ->add_fields( array(
        Field::make( 'multiselect', 'crb_sliding_posts', 'Products List' )
            ->add_options( $post_types ),
    ) )
    ->set_icon( 'image-flip-horizontal' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        $sliding_boxes = $block['crb_sliding_posts'];

        $args = array(
            'post_type'=> $sliding_boxes[0],
            'numberposts'      => -1,
            );   
        
        $posts = get_posts($args);
        ?>
        <div class="sliding_text feautered_boxes">
            <div class=" is-centered">
                <div class="sliding-text-slick">
                <?php
                foreach($posts as $post){
                    $images = array();
                    $images = carbon_get_post_meta($post->ID, 'wedding_images');
                    $title = carbon_get_post_meta($post->ID, 'wedding_surname');
                    $preview_image = carbon_get_post_meta($post->ID, 'wedding_preview_image');

                ?>
                <li class="column is-4-desktop">
                    <div class="hero" style="background-image: url('<?php echo wp_get_attachment_url($preview_image[0]); ?>'); background-size: cover;background-position: center center; " >
                        <div class="box-wrap">
                            <div class="box-ratio">
                                <div class="box-content">
                                    <div class="box-elements is-flex button-border">
                                        <a href="<?php echo get_permalink($wedding['ID']); ?>" class="button is-normal is-borderless is-radiusless btn-color-medium-light"><?php echo $title;?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                
                <?php } ?>
                </div>
            </div>
        </div>
        <?php 
    } );

    // Contact Details
    Block::make( __( 'Contact Details' ) )
    ->add_fields( array(
        Field::make( 'html', 'crb_contact_details', 'Contact Details' )
            ->set_html( '<h2>Contact Details Placeholder</h2><p>Contact Details can be amended from Theme Options > Contact Information</p>' )
    ) )
    ->set_icon( 'phone' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) { ?>
        <div class="contact-details">
            <div class="item">
                <div class="icon icon-address"></div>
                <div class="text">
                    <h4><?php _e( 'Address', 'als-front' ); ?></h4>
                    <?php echo carbon_get_theme_option( 'als_contact_address' ); ?>
                </div>
            </div>
            <div class="item">
                <div class="icon icon-phone"></div>
                <div class="text">
                    <h4><?php _e( 'Phone', 'als-front' ); ?></h4>
                    <?php echo carbon_get_theme_option( 'als_contact_phone' ); ?>
                </div>
            </div>
            <div class="item">
                <div class="icon icon-mobile"></div>
                <div class="text">
                    <h4><?php _e( 'Mobile', 'als-front' ); ?></h4>
                    <?php echo carbon_get_theme_option( 'als_contact_mobile' ); ?>
                </div>
            </div>
            <div class="item">
                <div class="icon icon-email"></div>
                <div class="text">
                    <h4><?php _e( 'Email', 'als-front' ); ?></h4>
                    <a href="mailto:<?php echo carbon_get_theme_option( 'als_contact_email' ); ?>">
                        <?php echo carbon_get_theme_option( 'als_contact_email' ); ?>
                    </a>
                </div>
            </div>
        </div>
        <?php
    } );

    // Map
    Block::make( __( 'Map Placeholder' ) )
        ->add_fields( array(
            Field::make( 'html', 'crb_map_placehodler', 'Map Placeholder' )
                ->set_html( '<h2>Map Placeholder</h2>' )
        ) )
    ->set_icon( 'admin-site' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) { ?>
        <div class="map-wrap">
            <div id="gmap_canvas" style="height: 500px;"></div>
        </div>
        <?php
    } );

    // Gear Up Section
    Block::make( __( 'Gear Up Section' ) )
        ->add_fields( array(
            Field::make( 'image', 'als_gear_up_image', 'Image' ),
            Field::make( 'text', 'als_gear_up_title', 'Title' ),
            Field::make( 'complex', 'als_gear_up_items', 'Items' )
                ->set_layout( 'tabbed-horizontal' )
                ->add_fields( array(
                    Field::make( 'image', 'image', 'Image' ),
                    Field::make( 'textarea', 'item_content', 'Content' ),
                ) ),
        ) )
    ->set_icon( 'editor-ol' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        $image = wp_get_attachment_image_src($block['als_gear_up_image'], 'large', false);
        ?>
        <section class="alt-text-images gear-up">
            <div class="overlay"></div>
            <div class="columns is-gapless is-multiline">
                <div class="column image" style="background-image: url('<?php echo $image[0]; ?>');"></div>
                <div class="column">
                    <div class="text">
                        <h2><?php echo $block['als_gear_up_title']; ?></h2>
                        <ul class="items-list">
                            <?php foreach( $block['als_gear_up_items'] as $item ){ ?>
                                <li>
                                    <span class="icon">
                                        <?php echo wp_get_attachment_image($item['image'], 'full', false); ?>
                                    </span>
                                    <span class="item-text">
                                        <?php echo $item['item_content']; ?>
                                    </span>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <?php
    } );

    // Vacancies
    Block::make( __( 'Vacancies' ) )
    ->add_fields( array(
        Field::make( 'complex', 'crb_vacancies', 'Vacancies' )
            ->set_layout( 'tabbed-horizontal' )
            ->add_fields( array(
                Field::make( 'text', 'job_position', 'Job Position' ),
                Field::make( 'radio', 'job_type', 'Full/Part Time' )
                ->add_options( array(
                    'Full-time' => 'Full-time',
                    'Part-time' => 'Part-time',
                ) ),
                Field::make( 'textarea', 'job_description', 'Description' ),
            ) ),
    ) )
    ->set_icon( 'format-quote' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) {
        $items = $block['crb_vacancies'];
        ?>
        <section class="columns is-centered">
            <div class="columns column is-three-fifths is-multiline">
                <?php
                    foreach($items as $job){ ?>
                        <div class="column is-half has-padding-30">
                            <p class="is-uppercase has-margin-top-20 has-margin-bottom-20 is-size-6 has-text-weight-bold"><?php echo $job['job_position'] ?></p>
                            <p class="job-type has-text-weight-bold is-marginless"><?php echo $job['job_type'] ?></p>
                            <p class="is-size-6 has-text-weight-normal has-margin-top-20 has-margin-bottom-20"><?php echo $job['job_description'] ?></p>
                        </div>
                    <?php }
                ?>
            </div>
        </section>
        <?php
    } );

    // Flower Care Steps
    Block::make( __( 'Flower Care Steps' ) )
        ->add_fields( array(
            Field::make( 'complex', 'als_care_steps', 'Steps' )
                ->set_layout( 'tabbed-horizontal' )
                ->add_fields( array(
                    Field::make( 'image', 'image', 'Image' ),
                    Field::make( 'text', 'subtitle', 'Subtitle' ),
                    Field::make( 'text', 'title', 'Title' ),
                    Field::make( 'textarea', 'content', 'Content' ),
                ) ),
        ) )
    ->set_icon( 'editor-ol' )
    ->set_preview_mode( true )
    ->set_render_callback( function ( $block ) { ?>
        <div class="flower-care-steps">
            <?php foreach($block['als_care_steps'] as $i=>$step){ ?>
                <?php if($i == 1){ echo '<div class="steps-wrap">'; } ?>
                <div class="item item-<?php echo $i; ?> has-text-centered">
                    <?php if(isset($step['image'])){ ?>
                        <div class="image">
                            <?php echo wp_get_attachment_image($step['image'], 'medium', false); ?>
                        </div>
                    <?php } ?>
                    <h2 class="section-title">
                        <span><?php echo $step['subtitle']; ?></span>
                        <?php echo $step['title']; ?>
                    </h2>
                    <div class="content">
                        <?php echo nl2br($step['content']); ?>
                    </div>
                </div>
            <?php } ?>
            <?php if($i > 0){ echo '</div>'; } ?>
        </div>
        <?php
    } );

    // Header Image on Features archive page
    Container::make('post_meta', 'Features Header Options')
        ->where( 'post_template', '=', 'views/archive-feature.blade.php')
        ->add_fields( array(
            Field::make( 'image', 'crb_features_header', 'Header Image' )
        ));

    // Header Image on Blog archive page
    Container::make('post_meta', 'Blog Header Options')
        ->where( 'post_template', '=', 'views/archive-blog.blade.php')
        ->add_fields( array(
            Field::make( 'image', 'crb_blog_header', 'Header Image' )
        ));

    // Header Image on Weddings archive page
    Container::make('post_meta', 'Wedding Header Options')
        ->where( 'post_template', '=', 'views/archive-wedding.blade.php')
        ->add_fields( array(
            Field::make( 'image', 'crb_wedding_header', 'Header Image' )
        ));

    // Header Image on Events archive page
    Container::make('post_meta', 'Events Header Options')
        ->where( 'post_template', '=', 'views/archive-event.blade.php')
        ->add_fields( array(
            Field::make( 'image', 'crb_event_header', 'Header Image' )
        ));

    // Header Image on Designs archive page
    Container::make('post_meta', 'Design Header Options')
        ->where( 'post_template', '=', 'views/archive-design.blade.php')
        ->add_fields( array(
            Field::make( 'image', 'crb_design_header', 'Header Image' )
        ));

    // Event Category Image on Designs archive page
    Container::make('term_meta', 'Category Image Options')
        ->where( 'term_taxonomy', '=', 'event_category')
        ->add_fields( array(
            Field::make( 'image', 'crb_box_image', 'Box Image' )
        ));

    // Event Category Banner image
    Container::make('term_meta', 'Category Image Options')
        ->where( 'term_taxonomy', '=', 'event_category')
        ->add_fields( array(
            Field::make( 'image', 'crb_banner_image', 'Banner Image' )
        ));

    // Event Category Seo Background Image
    Container::make('term_meta', 'Category Seo Options')
        ->where( 'term_taxonomy', '=', 'event_category')
        ->add_fields( array(
            Field::make( 'image', 'crb_seo_image', 'Seo Background Image' )
        ));

    // Event Category Seo Column 1
    Container::make('term_meta', 'Category Seo Options')
        ->where( 'term_taxonomy', '=', 'event_category')
        ->add_fields( array(
            Field::make( 'text', 'crb_seo_column_1', 'Seo Column 1' )
        ));

    // Event Category Seo Column 2
    Container::make('term_meta', 'Category Seo Options')
        ->where( 'term_taxonomy', '=', 'event_category')
        ->add_fields( array(
            Field::make( 'text', 'crb_seo_column_2', 'Seo Column 2' )
        ));

    // Header Image on Careers archive page
    Container::make('post_meta', 'Careers Options')
        ->where( 'post_template', '=', 'views/archive-vacancy.blade.php')
        ->add_fields( array(
            Field::make( 'image', 'crb_career_header', 'Header Image' ),
            Field::make( 'text', 'crb_apply_form', 'Apply Form Shortcode' )
        ));

    // Header Image on Testimonials archive page
    Container::make('post_meta', 'Testimonials Header Options')
        ->where( 'post_template', '=', 'views/archive-testimonial.blade.php')
        ->add_fields( array(
            Field::make( 'image', 'crb_testimonial_header', 'Header Image' )
        ));

    // Theme Options
    Container::make( 'theme_options', 'Theme Options' )
        ->add_tab( __('Shop Settings'), array(
            Field::make( 'select', 'als_gift_category', __( 'Gift Addons Category' ) )
                ->add_options( function() {
                    $cats = get_terms( 'product_cat' );
                    $catsArray = array( '- Select -' );

                    if( !empty($cats) ) {
                        foreach( $cats as $cat ) {
                            $catsArray[$cat->term_id] = $cat->name;
                        }

                        return $catsArray;
                    } else {
                        return array( 'No categories found' );
                    }
                } )
        ) )
        ->add_tab( __('Contact Information'), array(
            Field::make( 'text', 'als_contact_address', __( 'Address' ) ),
            Field::make( 'text', 'als_contact_phone', __( 'Phone' ) ),
            Field::make( 'text', 'als_contact_mobile', __( 'Mobile' ) ),
            Field::make( 'text', 'als_contact_email', __( 'Email' ) )
        ) )
        ->add_tab( __('Social Links'), array(
            Field::make( 'text', 'als_facebook', __( 'Facebook' ) ),
            Field::make( 'text', 'als_pinterest', __( 'Pinterest' ) ),
            Field::make( 'text', 'als_instagram', __( 'Instagram' ) ),
            Field::make( 'text', 'als_linkedin', __( 'Linkedin' ) ),
            Field::make( 'text', 'als_youtube', __( 'Youtube' ) )
        ) )
        ->add_tab( __('Landing Page Content'), array(
            Field::make( 'image', 'bg_image', 'Background Image' ),
            Field::make( 'image', 'logo', 'Logo' ),
            Field::make( 'complex', 'crb_buttons', 'Buttons')
                ->set_layout( 'tabbed-horizontal' )
                ->add_fields( array(
                    Field::make( 'text', 'crb_btn_text', 'Button Text' ),
                    Field::make( 'text', 'crb_btn_url', 'Button Url' ),
                )),
            ))
        ->add_tab( __('Checkout Options'), array(
            Field::make( 'complex', 'checkout_disabled_dates', __( 'Disabled Dates' ))
                ->set_layout( 'tabbed-horizontal' )
                ->add_fields( array(
                    Field::make( 'date', 'crb_disabled_date_date', 'Date' ),
                    Field::make( 'multiselect', 'crb_disabled_date_time', 'Time' )
                        ->add_options( array(
                            'morning' => 'Morning',
                            'afternoon' => 'Afternoon',
                            'anytime' => 'Anytime',
                            'custom_time' => 'Custom Time',
                        ) ),
                    Field::make( 'multiselect', 'crb_disabled_date_custom_time', 'Select Delivery Time to Disable (works only for Custom Time)' )
                        ->add_options( array(
                            '8:00 AM' => '8:00 AM',
                            '9:00 AM' => '9:00 AM',
                            '10:00 AM' => '10:00 AM',
                            '11:00 AM' => '11:00 AM',
                            '12:00 PM' => '12:00 PM',
                            '1:00 PM' => '1:00 PM',
                            '2:00 PM' => '2:00 PM',
                            '3:00 PM' => '3:00 PM',
                            '4:00 PM' => '4:00 PM',
                            '5:00 PM' => '5:00 PM',
                            '6:00 PM' => '6:00 PM',
                            '7:00 PM' => '7:00 PM',
                        ) ),

                    Field::make( 'textarea', 'checkout_disabled_note_delivery_time', __( 'Custom Delivery Time Note' ) ),
                ) ),

            Field::make( 'textarea', 'checkout_disabled_note', __( 'Disabled Date Note' ) ),
            Field::make( 'textarea', 'checkout_sameday_note', __( 'After 4pm Delivery Note' ) )
        ) );

    // NEW tick on products
    Container::make('post_meta', 'NEW Product')
        ->where( 'post_type', '=', 'product')
        ->add_fields( array(
            Field::make( 'checkbox', 'crb_product_new', 'NEW' )
        ));

});

/**
 * Boot Carbon Fields
 */
add_action( 'after_setup_theme', function () {
    \Carbon_Fields\Carbon_Fields::boot();
});

