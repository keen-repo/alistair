<?php

namespace App;

/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    // Add postMessage support
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial('blogname', [
        'selector' => '.brand',
        'render_callback' => function () {
            bloginfo('name');
        }
    ]);
});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('sage/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});

// woocommerce custom order metaboxes
add_action( 'woocommerce_admin_order_data_after_order_details', function ($order){
    ?>

        <?php
			/*
			* get all the meta data values we need
			*/
            $delivery_type = get_post_meta( $order->id, 'delivery_type', $unique = false );

            $get_pickup_date = get_post_meta( $order->id, 'pickup_date', $unique = false );
            $get_pickup_time = get_post_meta( $order->id, 'pickup_time', $unique = false );
            $get_delivery_time = get_post_meta( $order->id, 'delivery_time', $unique = false );

            //var_dump($get_pickup_time);
		?>
        <br class="clear" />
        <h4>Delivery Type</h4>
        <p class="form-field form-field-wide">
            <input class="input is-radiusless" type="text" value="<?= $delivery_type[0]; ?>" disabled>
        </p>
        <br class="clear" />
        <h4>Pick Up Info</h4>
        <p class="form-field form-field-wide">
            <label for="pickup_date">Pick Up Date:</label>
            <input class="input is-radiusless" type="text" value="<?= $get_pickup_date[0]; ?>" disabled>
            <?php if (!empty($get_pickup_time[0])) { ?>
                <label for="pickup_date">Pick Up Time:</label>
                <input class="input is-radiusless" type="text" value="<?= $get_pickup_time[0]; ?>" disabled>
            <?php } ?>
            <?php if(!empty($get_delivery_time[0])) { ?>
                <label for="pickup_date">Delivery Time:</label>
                <input class="input is-radiusless" type="text" value="<?= $get_delivery_time[0]; ?>" disabled>
            <?php } ?>
        </p>

    <?php
} );

add_action( 'woocommerce_admin_order_data_after_billing_address', function ($order){

    /*
    * get all the meta data values we need
    */
    $get_region = get_post_meta( $order->id, 'region', true );
    $get_address_type = get_post_meta( $order->id, 'address_type', $unique = false );

    $get_message_oncard = get_post_meta( $order->id, 'message_on_card', $single = true );

    ?>
    <p class="form-field form-field-wide">
        <label for="pickup_date">Region:</label>
        <input class="input is-radiusless" type="text" value="<?= $get_region; ?>" disabled>
        <label for="pickup_date">Address Type:</label>
        <input class="input is-radiusless" type="text" value="<?= $get_address_type[0]; ?>" disabled>
    </p>
    <br class="clear" />
    <h4>Message on Card:</h4>
    <p class="form-field form-field-wide wc-order-status">
        <textarea name="message" class="textarea is-radiusless"><?= $get_message_oncard; ?></textarea>
    </p>

    <?php
} );

add_action( 'woocommerce_admin_order_data_after_shipping_address', function ($order){

    /*
    * get all the meta data values we need
    */
    $delivery_phone = get_post_meta( $order->id, 'delivery_phone', true );

    ?>
    <p class="form-field form-field-wide">
        <label for="delivery_phone">Delivery Phone:</label>
        <input class="input is-radiusless" type="text" value="<?= $delivery_phone; ?>" disabled>
    </p>
    <?php
} );

// New order notification only for "Pending" Order status
add_action( 'woocommerce_checkout_order_processed', function($order_id) {
    // Get an instance of the WC_Order object
    $order = wc_get_order( $order_id );

    // Only for "pending" order status
    if( ! $order->has_status( 'pending' ) ) return;

    // Send "New Email" notification (to admin)
    WC()->mailer()->get_emails()['WC_Email_New_Order']->trigger( $order_id );
} );
