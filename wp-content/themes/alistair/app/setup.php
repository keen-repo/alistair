<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), false, null);
    wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), ['jquery'], null, true);

    wp_register_script( 'intlTelInput', 'https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.4/js/intlTelInput.min.js', null, null, true );
    wp_enqueue_script('intlTelInput');

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}, 100);

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil-clean-up');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage'),
        'secondary_navigation' => __('Secondary Navigation', 'sage'),
        'utility_navigation' => __('Utility Navigation', 'sage'),
        'footer_navigation' => __('Footer Navigation', 'sage'),
        //'footer_copyright_navigation' => __('Footer Copyright Navigation', 'sage')
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');
    add_image_size('hero', 2000, 800, true);

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    // Add support for full and wide align images.
    add_theme_support( 'align-wide' );

    // Add support for seo yoast bredcrumbs
    add_theme_support( 'yoast-seo-breadcrumbs' );

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));


    /**
     * Include shortcodes
     */
    require_once('shortcodes.php');

}, 20);

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ];
    register_sidebar([
        'name'          => __('Primary', 'sage'),
        'id'            => 'sidebar-primary'
    ] + $config);
    register_sidebar([
        'name'          => __('Footer', 'sage'),
        'id'            => 'sidebar-footer'
    ] + $config);
});

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
});

/**
 * Setup Woocommerce
 */

 /**
  * Wrap woocommerce content
  */

add_action( 'woocommerce_before_single_product_summary', function(){
    echo '<div class="full-width product-info-wrap"><div class="container">';
}, 0 );

add_action( 'woocommerce_after_single_product_summary', function(){
    echo '</div></div>';
}, 0 );

/**
 * Add thumbnail hover
 */

 add_action( 'woocommerce_before_shop_loop_item_title', function() {
    global $product;

    $images = $product->get_gallery_image_ids();
    if( isset($images[0]) ){
        $imageURL = wp_get_attachment_image_src($images[0], 'large', false);
        echo '<div class="overlay" style="background-image: url('.$imageURL[0].');"></div>';
    }
 }, 9 );

/**
 * Shop/archives: wrap the product image/thumbnail in a div.
 */
add_action( 'woocommerce_before_shop_loop_item_title', function(){
    echo '<div class="product-image-wrap">';
}, 8 );
add_action( 'woocommerce_before_shop_loop_item_title', function(){
    echo '</div>';
}, 11 );

/**
 * Shop/archives: wrap the product boxes in a div.
 */
add_action( 'woocommerce_before_shop_loop_item', function(){
    echo '<div class="product-wrap">';
}, 9 );
add_action( 'woocommerce_after_shop_loop_item', function(){
    echo '</div>';
}, 11 );

/**
 * Remove deafult woo breadcrumbs
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );

/**
 * Add Sidebar to shop category archives
 */

add_filter('sage/display_sidebar', function ($display) {
    static $display;

    isset($display) || $display = in_array(true, [
      is_product_category(),
      is_tax()
    ]);

    return $display;
});

/**
 * Show Woo page title only where we want
 */
add_filter('woocommerce_show_page_title', function () {
    if(is_shop()) return false;

	return true;
});

/**
 * Change number or products per row to 3
 */
add_filter('loop_shop_columns', function () {
	return 3; // 3 products per row
});

/**
 * Move product description
 */

add_action( 'woocommerce_single_product_summary', function () {
    global $post;
    echo '<div class="product-post-content">' . the_content() . '</div>';
}, 25);

/**
 * Remove all tabs
 */

add_filter( 'woocommerce_product_tabs', function ( $tabs ) {
    unset( $tabs['description'] );          // Remove the description tab
    unset( $tabs['reviews'] );          // Remove the reviews tab
    unset( $tabs['additional_information'] );   // Remove the additional information tab
    return $tabs;
}, 98);


/**
 * reorder price on product box and add a wrapper to these 2 sections
 */

remove_filter( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_price', 7 );

add_action( 'woocommerce_after_shop_loop_item', function() {
    echo '<div class="price-button-wrap is-flex">';
}, 6);

add_action( 'woocommerce_after_shop_loop_item', function() {
    echo '</div">';
}, 15);

/**
 * Remove number of results display
 */

remove_filter( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );

/**
 * Add product attributes to product page
 */

add_action('woocommerce_single_product_summary', function() {
    global $product;
    $attributes = $product->get_attributes();
    if ( ! $attributes ) {
        return;
    }

    $display_result = '';

    foreach ( $attributes as $attribute ) {


        if ( $attribute->get_variation() ) {
            continue;
        }
        $name = $attribute->get_name();
        if ( $attribute->is_taxonomy() ) {

            $terms = wp_get_post_terms( $product->get_id(), $name, 'all' );

            $cwtax = $terms[0]->taxonomy;

            $cw_object_taxonomy = get_taxonomy($cwtax);

            if ( isset ($cw_object_taxonomy->labels->singular_name) ) {
                $tax_label = $cw_object_taxonomy->labels->singular_name;
            } elseif ( isset( $cw_object_taxonomy->label ) ) {
                $tax_label = $cw_object_taxonomy->label;
                if ( 0 === strpos( $tax_label, 'Product ' ) ) {
                    $tax_label = substr( $tax_label, 8 );
                }
            }
            $display_result .= '<strong>'.$tax_label . ':</strong> ';
            $tax_terms = array();
            foreach ( $terms as $term ) {
                $single_term = esc_html( $term->name );
                array_push( $tax_terms, $single_term );
            }
            $display_result .= implode(', ', $tax_terms) .  '<br />';

        } else {
            $display_result .= $name . ': ';
            $display_result .= esc_html( implode( ', ', $attribute->get_options() ) ) . '<br />';
        }
    }
    echo $display_result;
}, 25);

/**
 * Add Gift addons to product page
 */

add_action( 'load_gift_addons', function() {
    global $product;
    $previous_global = $product;

    $addonsCat = carbon_get_theme_option( 'als_gift_category' );

    if( $addonsCat > 0 ) {

        $args = array(
            'post_type'             => 'product',
            'post_status'           => 'publish',
            'ignore_sticky_posts'   => 1,
            'posts_per_page'        => 16,
            'orderby'               => 'rand',
            'meta_query' => array(
                array(
                    'key' => '_stock_status', // Filter the add-ons shows only instock
                    'value' => 'instock'
                ),
            ),
            'tax_query'             => array(
                array(
                    'taxonomy'      => 'product_cat',
                    'field'         => 'term_id', //This is optional, as it defaults to 'term_id'
                    'terms'         => $addonsCat,
                    'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
                ),
                array(
                    'taxonomy'      => 'product_visibility',
                    'field'         => 'slug',
                    'terms'         => 'exclude-from-catalog', // Possibly 'exclude-from-search' too
                    'operator'      => 'NOT IN'
                )
            )
        );

        $products = get_posts($args);

        if(count($products) > 0) {

            $productsArr = array();

            foreach( $products as $i=>$product ){
                $prod = wc_get_product( $product->ID );
                $productsArr[$i]['ID'] = $product->ID;
                $productsArr[$i]['title'] = $product->post_title;
                $productsArr[$i]['price'] = $prod->get_price();
                $productsArr[$i]['price_html'] = $prod->get_price_html();
                $productsArr[$i]['desc'] = $prod->get_description();

                $image = $prod->get_image_id();
                if($image) {
                    $productsArr[$i]['thumb'] = wp_get_attachment_image($image, 'thumbnail', true);
                    $productsArr[$i]['image'] = wp_get_attachment_image($image, 'medium', true);
                }

                if($prod->is_type( 'variable' )){
                    $variations = $prod->get_available_variations();

                    foreach( $variations as $v=>$variation ){
                        if($variation['variation_is_active'] == true && $variation['variation_is_visible'] == true) {
                            $productsArr[$i]['variations'][$v]['ID'] = $variation['variation_id'];
                            $productsArr[$i]['variations'][$v]['price'] = $variation['display_price'];

                            $attributes = $variation['attributes'];
                            foreach( $attributes as $a=>$att ) {
                                $productsArr[$i]['variations'][$v]['attributes'][$a] = $att;
                            }
                        }
                    }

                    $productVariations = array();

                    foreach( $productsArr[$i]['variations'] as $c=>$variation ){
                        foreach( $variation['attributes'] as $k=>$attrib ){
                            $productVariations[$k][$c] = array(
                                'ID' => $variation['ID'],
                                'title' => $attrib,
                                'price' => $variation['price']
                            );
                        }
                    }

                    $productsArr[$i]['variations'] = $productVariations;
                }
            }

        ?>
            <h4><?php _e( 'Add Something Special', 'als-front' ); ?></h4>

            <div class="gift-addons">
                <div class="gifts-slider">
                    <?php foreach($productsArr as $i=>$prd) { ?>
                        <a target="_blank" class="item is-block" href="<?=get_permalink($prd["ID"]) ?>" data-item="<?php echo $i; ?>" data-prod-id="<?php echo $prd['ID']; ?>">
                            <?php
                                global $product;
                                $product = wc_get_product($prd['ID']);
                                woocommerce_show_product_loop_sale_flash();
                                if(isset($prd['thumb'])){ ?>
                                    <div class="image">
                                        <div class="overlay is-flex"><?php _e( 'View', 'als-front' ); ?></div>
                                        <?php echo $prd['thumb'] ?>
                                    </div>
                                <?php }
                            ?>
                            <div class="title"><?php echo $prd['title']; ?></div>
                            <div class="actions is-flex">
                                <?php
                                    $checked = false;
                                    if(isset($_POST['gift_addon_'.$prd['ID']]) && $_POST['gift_addon_'.$prd['ID']] == 'on'){
                                        $checked = true;
                                    }
                                ?>
                                <input type="checkbox" name="gift_addon_<?php echo $prd['ID']; ?>" data-id="<?php echo $prd['ID']; ?>" data-title="<?php echo htmlentities($prd['title'], ENT_QUOTES); ?>" data-price="<?php echo $prd['price']; ?>" <?php if($checked) echo 'class="is-checked"' ?> />
                                <?php if( isset($prd['variations']) ){
                                    foreach( $prd['variations'] as $k=>$variation ){ ?>
                                        <input type="hidden" name="<?php echo $k; ?>_<?php echo $prd['ID']; ?>" value="" />
                                    <?php }
                                } ?>
                                <span class="price">
                                    &euro;<?php echo $prd['price'] ?>
                                </span>
                            </div>
                        </a>
                    <?php } ?>
                </div>
            </div>

            <?php
                global $product;
                $product = $previous_global;
            ?>
            <!-- <div class="remodal addons-modal" data-remodal-id="add-addons">
                <div class="modal-header columns">
                    <button data-remodal-action="close" class="remodal-close"></button>
                    <button id="show-addons-list"><span></span></button>
                </div>
                <div id="addons-list">
                    <div class="addon-title"><?php _e( 'Add-ons', 'als-front' ); ?></div>
                    <div class="columns is-multiline">
                        <?php foreach( $productsArr as $prd ) { ?>
                            <?php if(!isset($prd['variations'])){ ?>
                                <div class="column is-12 is-one-third-desktop">
                                    <div class="image"><?php echo $prd['thumb']; ?></div>
                                    <div class="addon-title"><?php echo $prd['title']; ?></div>
                                    <div class="price"><?php echo $prd['price_html']; ?></div>
                                    <button class="button alt" data-addon-id="<?php echo $prd['ID']; ?>"><?php _e( 'Add', 'als-front' ); ?></button>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
                <div id="single-addons-list">
                    <div class="single-addons-slider">
                        <?php foreach($productsArr as $prd) { ?>
                            <div class="item">
                                <div class="addon-title"><?php echo $prd['title'] ?></div>
                                <div class="image"><?php echo $prd['image']; ?></div>
                                <div class="desc"><?php echo $prd['desc']; ?></div>
                                <div class="price"><?php echo $prd['price_html']; ?></div>
                                <div class="default-price"><?php echo $prd['price_html']; ?></div>
                                <?php if(isset($prd['variations'])){ ?>
                                    <div class="variations">
                                    <?php

                                    foreach( $prd['variations'] as $k=>$variations ){
                                        if(isset(explode( 'attribute_pa_', $k )[1])){
                                            $attrName = explode( 'attribute_pa_', $k )[1];
                                            _e( 'Choose ' . ucwords($attrName) . ':' );
                                        }
                                        ?>
                                        <select name="<?php echo $k; ?>">
                                            <option value="0"><?php _e( '- Select -', 'als-front' ); ?></option>
                                            <?php foreach( $variations as $attr ) { ?>
                                                <option value="<?php echo $attr['ID']; ?>" data-price="<?php echo $attr['price']; ?>">
                                                    <?php echo $attr['title']; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    <?php }

                                    ?>
                                    </div>
                                <?php } ?>
                                <button class="button alt" data-addon-id="<?php echo $prd['ID']; ?>"><?php _e( 'Add', 'als-front' ); ?></button>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div> -->
        <?php }
    }
}, 1 );

add_action( 'woocommerce_before_add_to_cart_button', function() {
    do_action('load_gift_addons');
}, 1);

/**
 * Add addons to cart
 */

add_action( 'template_redirect', function(){
    // had to hook this up to template_redirect as hooking to woocommerce_add_to_cart was resulting in a recursive
    // so let's check if we have a product in the request

    if( isset($_POST['add-to-cart']) && !is_admin() ){
        // check if we have any gifts in the request
        $gifts = array();

        foreach( $_POST as $k=>$v ){
            if(strpos($k, 'gift_addon_') !== false){
                $giftID = (int)explode( 'gift_addon_', $k )[1];

                if(isset($_POST['attribute_pa_size_'.$giftID])){
                    $gifts[] = $_POST['attribute_pa_size_'.$giftID];
                } else {
                    $gifts[] = $giftID;
                }
            }
        }

        if( count($gifts) > 0 ){
            foreach( $gifts as $gift ){
                WC()->cart->add_to_cart( $gift );
            }
        }
    }

});

/**
 * Remove product meta
 */

remove_filter( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

/**
 * Change buy now button text
 */

add_filter( 'woocommerce_product_single_add_to_cart_text', function(){
    return __( 'ADD TO CART', 'als-front' );
});

/**
 * Add payment methods beneath add to cart button
 */

add_action( 'woocommerce_single_product_summary', function(){ ?>
    <ul class="payment-methods">
        <!-- <li><img src="<?php echo asset_path('images/paypal-logo.png') ?>" /></li> -->
        <!-- <li>Pay via phone</li> -->
        <!-- <li><img src="<?php echo asset_path('images/revolut-logo-small.png') ?>" /></li> -->
        <li><img src="<?php echo asset_path('images/payments.png') ?>" /></li>
        </ul>
<?php }, 39 );

/**
 * Change related products text
 */

add_filter( 'gettext', function( $translated_text, $text, $domain ){
    switch ( $translated_text ) {
        case 'Related products' :
            $translated_text = __( 'You might also like', 'als-front' );
            break;
    }
    return $translated_text;
}, 20, 3 );

/**
 * Process custom checkout form and redirect to correct payment page
 */

add_action( 'template_redirect', function() {
    if( isset($_POST['process-checkout']) && !is_admin() ){

        /* First Step*/

        // get the type of delivery

        // get the details of customer
        if(isset($_POST['pick_up'])) {

            $delivery_type = "Pick Up";

            $name = $_POST['name'];
            $mobile = $_POST['mobile'];
            $email = $_POST['email'];

            $pickup_date = $_POST['pickup_date'];
            $time = $_POST['time'];
        }
        if(isset($_POST['delivery_to_Me'])) {

            $delivery_type = "Delivery to Me";

            $name = $_POST['name2'];
            $mobile = $_POST['mobile2'];
            $email = $_POST['email2'];

            $address1 = $_POST['door_number2'] . ', ' . $_POST['address12'];
            $address2 = $_POST['address22'];
            $city = $_POST['city2'];
            $region = $_POST['billing_region'];
            $post_code = $_POST['post_code2'];

            $addr_type = $_POST['addr_type'];
            $pickup_date = $_POST['date2'];
            if($pickup_date=='2020-05-08' || $pickup_date=='2020-05-09' || $pickup_date=='2020-05-10'){

            }else{
                $delivery_time = $_POST['delivery_time2'];
            }

            $customer_note = $_POST['additional_info2'];

            $region = $_POST['billing_region'];
        }

        if(isset($_POST['delivery_to_Receiver'])) {

            $delivery_type = "Delivery to Receiver";

            $name = $_POST['name3'];
            $mobile = $_POST['mobile3'];
            $email = $_POST['email3'];

            $address1 = $_POST['door_number3'] . ', ' . $_POST['address13'];
            $address2 = $_POST['address23'];
            $city = $_POST['city3'];
            $region = $_POST['billing_region_2'];
            $post_code = $_POST['post_code3'];

            // receiver info
            $rec_name = explode(' ', $_POST['rec_name']);
            $rec_mobile = $_POST['rec_mobile'];
            $rec_email = $_POST['rec_email'];

            $pickup_date = $_POST['rec_date'];

            if($pickup_date=='2020-05-08' || $pickup_date=='2020-05-09' || $pickup_date=='2020-05-10'){

            }else{
                $delivery_time = $_POST['delivery_time3'];
            }

            $customer_note = $_POST['additional_info3'];

        }
        // Spliting the full name field
        $name = explode(' ',$name);

        /* Second Step */
        // get the message on card
        $message_on_card = $_POST['message_on_card'];

        /* Third Step */
        // payment details

        // Now we create the order
        $order = wc_create_order();
        // The add_product() function below is located in /plugins/woocommerce/includes/abstracts/abstract_wc_order.php

        foreach( WC()->cart->get_cart() as $cart_item ){
            $productID = $cart_item['product_id'];
            $quantity = $cart_item['quantity'];

            if(isset($cart_item['variation_id']) && !empty($cart_item['variation_id'])){
                $productID = $cart_item['variation_id'];
            }

            $order->add_product( wc_get_product($productID), $quantity, $args); // Use the product IDs to add
        }


        // Add addons added during checkout
        // check if we have any gifts in the request
        $gifts = array();

        foreach( $_POST as $k=>$v ){
            if(strpos($k, 'gift_addon_') !== false){
                $giftID = (int)explode( 'gift_addon_', $k )[1];

                if(isset($_POST['attribute_pa_size_'.$giftID])){
                    $gifts[] = $_POST['attribute_pa_size_'.$giftID];
                } else {
                    $gifts[] = $giftID;
                }
            }
        }

        if( count($gifts) > 0 ){
            foreach( $gifts as $gift ){
                $order->add_product( wc_get_product( $gift ), 1);
            }
        }

        update_post_meta( $order->id, 'delivery_type', $delivery_type, $unique = false );
        update_post_meta( $order->id, 'pickup_date', $pickup_date, $unique = false );
        update_post_meta( $order->id, 'pickup_time', $time, $unique = false );
        update_post_meta( $order->id, 'message_on_card', $message_on_card, $unique = false );

        if($delivery_type == 'Pick Up') {
            $last_name = $name[count($name)-1];

            $first_name = "";
            if(count($name) > 1){
                $first_name = "";
                for ($x = 0; $x <= count($name)-2; $x++) {
                    $first_name .= " ".$name[$x];
                }
            }else {
                $first_name = $last_name;
            }

            $address = array(
                'first_name' => $first_name,
                'last_name'  => $last_name,
                'email'      => $email,
                'phone'      => $mobile,
            );
            $order->set_address( $address, 'billing' );
        }

        if($delivery_type == 'Delivery to Me') {

            $last_name = $name[count($name)-1];
            $first_name = "";
            if(count($name) > 1){
                for ($x = 0; $x <= count($name)-2; $x++) {
                    $first_name .= " ".$name[$x];
                }
            }else {
                $first_name = $last_name;
            }

            $address = array(
                'first_name' => $first_name,
                'last_name'  => $last_name,
                'company'    => '',
                'email'      => $email,
                'phone'      => $mobile,
                'address_1'  => isset($address1) ? $address1 : 'n/a',
                'address_2'  => isset($address2) ? $address2 : 'n/a',
                'city'       => isset($city) ? $city : 'n/a',
                'postcode'   => isset($post_code) ? $post_code : 'n/a',
                'country'    => 'MT'
            );

            $order->set_address( $address, 'billing' );
            $order->set_address( $address, 'shipping' );

            if(!empty( $customer_note )){
                $order->add_order_note( $customer_note );
            }

            update_post_meta( $order->id, 'customer_note', $customer_note, false );
            update_post_meta( $order->id, 'region', $region, false );
            update_post_meta( $order->id, 'address_type', $addr_type, false );
            update_post_meta( $order->id, 'delivery_time', $delivery_time, false );
        }

        if($delivery_type == 'Delivery to Receiver') {

            $last_name = $name[count($name)-1];
            $first_name = "";
            if(count($name) > 1){
                for ($x = 0; $x <= count($name)-2; $x++) {
                    $first_name .= " ".$name[$x];
                }
            }else {
                $first_name = $last_name;
            }

            $address = array(
                'first_name' => $first_name,
                'last_name'  => $last_name,
                'company'    => '',
                'email'      => $email,
                'phone'      => $mobile,
            );

            $order->set_address( $address, 'billing' );

            $last_name = $rec_name[count($rec_name)-1];
            $first_name = "";
            if(count($rec_name) > 1){
                for ($x = 0; $x <= count($rec_name)-2; $x++) {
                    $first_name .= " ".$rec_name[$x];
                }
            }else {
                $first_name = $last_name;
            }

            $address = array(
                'first_name' => $first_name,
                'last_name'  => $last_name,
                'company'    => '',
                'email'      => $rec_email,
                'phone'      => $rec_mobile,
                'address_1'  => isset($address1) ? $address1 : 'n/a',
                'address_2'  => isset($address2) ? $address2 : 'n/a',
                'city'       => isset($city) ? $city : 'n/a',
                'postcode'   => isset($post_code) ? $post_code : 'n/a',
                'country'    => 'MT'
            );

            $order->set_address( $address, 'shipping' );

            if(!empty( $customer_note )){
                $order->add_order_note( $customer_note );
            }

            update_post_meta( $order->id, 'customer_note', $customer_note, false );
            update_post_meta( $order->id, 'region', $region, false );
            update_post_meta( $order->id, 'address_type', $addr_type, false );
            update_post_meta( $order->id, 'delivery_time', $delivery_time, false );
            update_post_meta( $order->id, 'delivery_phone', $rec_mobile, false );

        }

        // Set payment gateway
        $payment_gateways = WC()->payment_gateways->payment_gateways();
        $payment_gateways_id = $_POST['payment_method'];
        //var_dump($payment_gateways_id);
        $order->set_payment_method( $payment_gateways[$payment_gateways_id] );

        // Set shipping method and costs
        $pickup_date_arr = explode('-', $pickup_date);
        $pickup_date_ts = mktime( 0, 0, 0, $pickup_date_arr[1], $pickup_date_arr[2], $pickup_date_arr[0] );

        // define public holidays
        $publicHolidays = [
            $pickup_date_arr[0] . '-01-01', // new year
            $pickup_date_arr[0] . '-02-10', // st paul
            $pickup_date_arr[0] . '-03-19', // st joseph
            $pickup_date_arr[0] . '-03-31', // freedom day
            $pickup_date_arr[0] . '-04-15', // good friday
            $pickup_date_arr[0] . '-05-01', // workers day
            $pickup_date_arr[0] . '-06-07', // sette giugno
            $pickup_date_arr[0] . '-06-29', // st peter and paul
            $pickup_date_arr[0] . '-08-15', // assumption
            $pickup_date_arr[0] . '-09-08', // lady of victories
            $pickup_date_arr[0] . '-09-21', // independence
            $pickup_date_arr[0] . '-12-08', // immaculate conception
            $pickup_date_arr[0] . '-12-13', // republic day
            $pickup_date_arr[0] . '-12-25', // christmas day
        ];

        $exclusions = [
            '2019-04-12', // mother's day
            '2023-04-07', // good friday
            '2023-04-09' //easter
        ];

        $rate = new \Alistair_Shipping_Method();
        $item = new \WC_Order_Item_Shipping();

        if(!isset($_POST['pick_up']) && !in_array($pickup_date, $exclusions) && (date('D', $pickup_date_ts) == 'Sun' || in_array($pickup_date, $publicHolidays))){

            $item->set_method_id( "alistair" ); // set an existing Shipping method rate ID

            if($region == 'Gozo'){
                $message = 'Gozo Sunday/Public Holiday Delivery';
                $cost = $rate->gozo_sunday_cost;
            } else if($pickup_date=="2022-05-08"){
                $message = 'Mothers Day Delivery';
                $cost = 5;
            }else{
                $message = 'Sunday/Public Holiday Delivery';
                $cost = $rate->sunday_cost;
            }




            $item->set_method_title( $message );
            $item->set_total( $cost );
            $order->add_item( $item );
        } else {
            if($region == 'Gozo'){
                $message = 'Gozo Delivery';
                $cost = $rate->gozo_cost;

                $item->set_method_id( "alistair" ); // set an existing Shipping method rate ID
                $item->set_method_title( $message );
                $item->set_total( $cost );
                $order->add_item( $item );
            }
        }

        // Calculate totals
        $order->calculate_totals();
        $order->update_status( 'Completed', TRUE);
        /**/

        $orderID = $order->id;

        // New payment method for pick up payment (Pay on collection)
        if($payment_gateways_id == 'pay_on_collection' ){
            $payment_gateways_id = 'cod';
        }

        // payment methods: ppec_paypal, phone_pay, revolut_pay, cod
        if($payment_gateways_id == 'cod'){
            // send the email to user
            // Get WooCommerce email objects
            $mailer = WC()->mailer()->get_emails();
            $mailer['WC_Email_Customer_Processing_Order']->trigger( $orderID );
            // email for the admin
            $mailer['WC_Email_New_Order']->trigger( $orderID );

            wp_redirect( 'checkout/?id='.$orderID.'' );

        }
        if($payment_gateways_id == 'phone_pay'){
            // send the email to user
            $mailer = WC()->mailer()->get_emails();
            $mailer['WC_Email_Customer_Processing_Order']->trigger( $orderID );
            // email for the admin
            $mailer['WC_Email_New_Order']->trigger( $orderID );

            wp_redirect( 'checkout/?id='.$orderID.'' );
        }
        if($payment_gateways_id == 'revolut_pay'){
            // send the email to user
            $mailer = WC()->mailer()->get_emails();
            $mailer['WC_Email_Customer_Processing_Order']->trigger( $orderID );
            // email for the admin
            $mailer['WC_Email_New_Order']->trigger( $orderID );

            wp_redirect( 'checkout/?id='.$orderID.'' );
        }
        if($payment_gateways_id == 'paypal'){

            $result = $payment_gateways[ 'paypal' ]->process_payment( $order->id );

            // Redirect to success/confirmation/payment page
            if ( $result['result'] == 'success' ) {

                $result = apply_filters( 'woocommerce_payment_successful_result', $result, $order->id );

                wp_redirect( $result['redirect'] );
                exit;
            }
        }
        if($payment_gateways_id == 'mypos_virtual'){

            $result = $payment_gateways[ 'mypos_virtual' ]->process_payment( $order->id );

            // Redirect to success/confirmation/payment page
            if ( $result['result'] == 'success' ) {

                $result = apply_filters( 'woocommerce_payment_successful_result', $result, $order->id );

                wp_redirect( $result['redirect'] );
                exit;
            }
        }

    }
} );


// Remove cross-sells at cart
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );

/**
 * Add support for product gallery features
 */

add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );

/**
 * Add category description on archive pages
 */

$count = 0;
add_action( 'get_footer', function($name) {
    global $wp_query;

    // getting the taxonomy
    $q_object = get_queried_object();
    if(is_object($q_object)){
        if(property_exists($q_object,'taxonomy')){
            $taxonomy = $q_object->taxonomy;
        }else{
            $taxonomy = "";
        }
    }


    if(is_product_category() && $name !== 'shop' OR $taxonomy == 'pa_collections'){

        if($taxonomy == 'pa_collections') {
            $cat = $wp_query->get_queried_object()->term_id;
            $desc = term_description( $cat );
            // getting image url from ACF
            $acf_image = get_field('coll_back_img', $q_object);
            $src = $acf_image['url'];

        } else {
            $cat = $wp_query->get_queried_object()->term_id;
            $thumb = get_term_meta( $cat, 'thumbnail_id', true );
            $desc = term_description( $cat );

            if(!empty($thumb)){
                $src = wp_get_attachment_image_src($thumb, 'full-width', false)[0];
            } else {
                $src = '';
            }
        }

        // testimonials by product category
        $term = get_term( $cat, $taxonomy );
        $product_categ_slug = $term->slug;

        //var_dump($product_categ_slug);

        /*


        */
        global $wpdb;

        $pargs = array(
            'post_type' => 'testimonial',
            'post_status' => 'publish'
        );
        $myProducts = get_posts($pargs);

        ?>
         <section class="testimonials columns is-marginless">
            <ul class="items column is-three-fifths is-offset-one-fifth">
        <?php

        // looping trough testimonials
        foreach ($myProducts as $testimonial) {

            $category_slug = $wpdb->get_results( "SELECT
            t.slug
            FROM wp_posts p
            LEFT JOIN wp_term_relationships r ON r.object_id = p.ID
            LEFT JOIN wp_terms t ON t.term_id = r.term_taxonomy_id
            WHERE p.post_type = 'testimonial'
            AND p.post_status = 'publish'
            AND p.ID =$testimonial->ID", OBJECT );

            if($product_categ_slug == $category_slug[0]->slug){


                $meta = get_post_meta($testimonial->ID);

                $name = $meta['_testimonial_client_name'][0];
                $text = $meta['_testimonial_text'][0];
                $image = $meta['_testimonial_image'][0];

               /* var_dump($name);
                var_dump($text);
                var_dump($image);*/

                ?>
                    <li class="item has-text-centered">
                        <div class="has-margin-bottom-50">
                        <div class="is-relative has-height-150 has-width-150 profile-image-circle" style="background-image: url('<?php echo wp_get_attachment_url($image); ?>'); background-size: cover;background-position: center center; "></div>
                        </div>
                        <div class="test-content"><?= $text ?></div>
                        <div class="test-name">- <?= $name ?> -</div>
                    </li>
                <?php



            }
        } // End of foreach
        ?>
            </ul>
        </section>

            <!--
            <section class="testimonials columns is-marginless">
                <ul class="items column is-three-fifths is-offset-one-fifth">
                    <li class="item has-text-centered">
                        <div class="has-margin-bottom-50">
                        <div class="is-relative has-height-150 has-width-150 profile-image-circle" style="background-image: url('https://dummyimage.com/600x400/000/fff'); background-size: cover;background-position: center center; "></div>
                        </div>
                        <div class="test-content">Text</div>
                        <div class="test-name">- Name -</div>
                    </li>

                    <li class="item has-text-centered">
                        <div class="has-margin-bottom-50">
                        <div class="is-relative has-height-150 has-width-150 profile-image-circle" style="background-image: url('https://dummyimage.com/600x400/fcba03/fff'); background-size: cover;background-position: center center; "></div>
                        </div>
                        <div class="test-content">Text</div>
                        <div class="test-name">- Name -</div>
                    </li>

                    <li class="item has-text-centered">
                        <div class="has-margin-bottom-50">
                        <div class="is-relative has-height-150 has-width-150 profile-image-circle" style="background-image: url('https://dummyimage.com/600x400/a103fc/fff'); background-size: cover;background-position: center center; "></div>
                        </div>
                        <div class="test-content">Text</div>
                        <div class="test-name">- Name -</div>
                    </li>
                </ul>
            </section>-->
        <?php

        if(!empty($desc)){ ?>
            <div class="shop-category-desc" style="background-image: url('<?php echo $src; ?>')">
                <div class="container columns is-marginless" style="max-width: 100%; width: 100%;">
                    <?php
                        $desc = str_replace('</p>', '', $desc);

                        $desc_array = explode('<p>', $desc);
                    ?>
                    <div class="column"><?php echo $desc_array[1]; ?></div>
                    <div class="column"><?php echo $desc_array[2]; ?></div>
                </div>
            </div>
        <?php }
    } // End of check for product category

} );

/**
 * Remove archive description from original position as we're adding it up here
 */

remove_action( 'woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10);

/**
 * Change position of add to cart button
 */

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 35 );

/**
 * Remove product links in cart for gift addons
 */

// define the woocommerce_cart_item_permalink callback
function filter_woocommerce_cart_item_permalink( $product_get_permalink_cart_item, $cart_item, $cart_item_key ) {
    // make filter magic happen here...
    return $product_get_permalink_cart_item;
};

// add the filter
add_filter( 'woocommerce_cart_item_permalink', function($product_get_permalink_cart_item, $cart_item, $cart_item_key) {
    $cats = get_the_terms( $cart_item['product_id'], 'product_cat' );
    $addonsCat = carbon_get_theme_option( 'als_gift_category' );

    foreach( $cats as $cat ) {
        if($cat->term_id == $addonsCat)
            return null;
    }

    return $product_get_permalink_cart_item;
}, 10, 3 );


/**
 * Set minimum order amount
 */

add_action( 'woocommerce_proceed_to_checkout', function() {
    if(is_cart()) {
        $total = WC()->cart->get_subtotal();
        if((float)$total < 30){
            $notice = 'Sorry, we only accept orders starting from €30. Please <a href="/shop/">add a bouquet</a> to your cart to continue.';
            wc_add_notice( $notice, 'error' );
            remove_action( 'woocommerce_proceed_to_checkout', 'woocommerce_button_proceed_to_checkout', 20 );
            echo '<a href="#" class="checkout-button button alt wc-forward">Proceed to checkout</a>';
        }
    }
}, 1 );

/**
 * Woocomercre Logout Reditedt to Homepage
 */
add_action('wp_logout', function (){

    wp_redirect( home_url() );
    exit();

  });

  /**
   * Ajax woocomentce quick fix when the all records need to be shown the reirect is
   * changing the url that works showing all records (prevulsy showed "no results found")
   */
  add_action( 'template_redirect', function () {
    if ( is_search() && ! empty( $_GET['s'] ) ) {
        wp_redirect( home_url( "/search/" ) . urlencode( get_query_var( 's' ) ) );
        exit();
    }
} );


add_action('nav_menu_css_class', function($classes, $item){
    $slug = $item->title;
    $str = strtolower(str_replace(' ', '-', $slug));

    if(strpos($_SERVER['REQUEST_URI'], $str) !== false){
        $classes[] = "active";
    }

    return $classes;

}, 10, 4);



ini_set('max_execution_time', 300);
set_time_limit(300);
