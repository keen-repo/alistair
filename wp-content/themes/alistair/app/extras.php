<?php

namespace App;

function has_children() {
    global $post;
    
    $pages = get_pages('child_of=' . $post->ID);
    
    if (count($pages) > 0):
      return true;
    else:
      return false;
    endif;
  }

function get_post_slug() {
    global $post;
    return $post->post_name;
}

?>