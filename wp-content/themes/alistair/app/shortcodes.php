<?php

namespace App;

/**
 * Add the shortcodes
 */
add_action('init', function () {

    /**
     * @snippet       WooCommerce User Login Shortcode
     * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
     * @author        Rodolfo Melogli
     * @compatible    WooCommerce 3.6.2
     * @donate $9     https://businessbloomer.com/bloomer-armada/
     */

    // THIS WILL CREATE A NEW SHORTCODE: [wc_login_form_bbloomer]

    add_shortcode( 'wc_login_form_bbloomer', function () {
        if ( is_admin() ) return;
            ob_start();
        if ( ! is_user_logged_in() ) {

            // NOTE: THE FOLLOWING <FORM> IS COPIED FROM woocommerce\templates\myaccount\form-login.php
            // IF WOOCOMMERCE RELEASES AN UPDATE TO THAT TEMPLATE, YOU MUST CHANGE THIS ACCORDINGLY

            // Set the Blade template we're going to use for the Shortcode
            $template = 'woocommerce/userauth/login';

            // Set up template data
            $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
                return apply_filters("sage/template/{$class}/data", $data, $template);
            }, []);

            // Echo the shortcode blade template
            echo Template($template, $data);

            // END OF COPIED HTML
            // ------------------
        }
        return ob_get_clean();
    } );


    /**
    * @snippet       WooCommerce User Registration Shortcode
    * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
    * @author        Rodolfo Melogli
    * @compatible    WooCommerce 3.6.2
    * @donate $9     https://businessbloomer.com/bloomer-armada/
    */

    // THIS WILL CREATE A NEW SHORTCODE: [wc_reg_form_bbloomer]

    add_shortcode( 'wc_reg_form_bbloomer', function () {
        if ( is_admin() ) return;
            ob_start();
        if ( is_user_logged_in() ) {
            wc_add_notice( sprintf( __( 'You are currently logged in. If you wish to register with a different account please <a href="%s">log out</a> first', 'bbloomer' ), wc_logout_url() ) );
            wc_print_notices();
        } else {
            // NOTE: THE FOLLOWING <FORM> IS COPIED FROM woocommerce\templates\myaccount\form-login.php
            // IF WOOCOMMERCE RELEASES AN UPDATE TO THAT TEMPLATE, YOU MUST CHANGE THIS ACCORDINGLY

            // Set the Blade template we're going to use for the Shortcode
            $template = 'woocommerce/userauth/register';

            // Set up template data
            $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
                return apply_filters("sage/template/{$class}/data", $data, $template);
            }, []);

            // Echo the shortcode blade template
            echo Template($template, $data);

            // END OF COPIED HTML
            // ------------------
        }
        return ob_get_clean();
    } );


});
