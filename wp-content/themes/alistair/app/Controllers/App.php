<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

    /**
    * Primary Nav Menu arguments
    * @return array
    */
    public function primarymenu() {
        $args = array(
        'theme_location'    => 'primary_navigation',
        'walker'            => new \App\wp_bulma_navwalker()
        );
        return $args;
    }

    public function weddingsmenu() {
        $args = array(
        'menu'    => 'Weddings Menu',
        'walker'            => new \App\wp_bulma_navwalker()
        );
        return $args;
    }

    public function eventscorporatemenu() {
        $args = array(
        'menu'    => 'Events Corporate Menu',
        'walker'            => new \App\wp_bulma_navwalker()
        );
        return $args;
    }

    public function shopmenu() {
        $args = array(
        'menu'    => 'Fresh Flower Shop Menu',
        'walker'            => new \App\wp_bulma_navwalker()
        );
        return $args;
    }

    /**
    * Footer Nav Menu arguments
    * @return array
    */
    public function footermenu() {
        $args = array(
        'theme_location'    => 'footer_navigation',
        'walker'            => new \App\wp_bulma_navwalker()
        );
        return $args;
    }

    // Carbon Fields
    /*public function greeting() {
        return carbon_get_the_post_meta( 'sage_greeting' );
    }*/

    /**
     * Related Products
     */
    public function kn_related_products() {

        global $product;

        if($product == null){
            return 'Product not found - possibly because this is included outside of the single products view';
        }

        if(is_object($product)){
            if($product->get_id()){
                $related_ids = wc_get_related_products($product->get_id(), 9);
                $related_products = array();
        
                foreach($related_ids as $i=>$id){
                    $product_obj = wc_get_product($id);
                    $related_products[] = $product_obj;
                }
        
                return $related_products;
            }else {
                return null;
            }
        }
        
    }

}
