<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    collect(['get_header', 'wp_head'])->each(function ($tag) {
        ob_start();
        do_action($tag);
        $output = ob_get_clean();
        remove_all_actions($tag);
        add_action($tag, function () use ($output) {
            echo $output;
        });
    });
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Render comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );

    $data = collect(get_body_class())->reduce(function ($data, $class) use ($comments_template) {
        return apply_filters("sage/template/{$class}/data", $data, $comments_template);
    }, []);

    $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

    if ($theme_template) {
        echo template($theme_template, $data);
        return get_stylesheet_directory().'/index.php';
    }

    return $comments_template;
}, 100);

/**
* Change the sorting titles in order by dropbox
*/
add_filter( 'woocommerce_catalog_orderby', function ($options) {

    //'menu_order' => __( 'Default sorting', 'woocommerce' ),
    $options['popularity'] = __( 'Popularity', 'woocommerce' );
    $options['rating'] = __( 'Average rating', 'woocommerce' );
    $options['date'] = __( 'Newness', 'woocommerce' );

    $options['price'] = __( 'Price, low to high', 'woocommerce' );
    $options['price-desc'] = __( 'Price, high to low', 'woocommerce' );

    return $options;
});

/**
 * Woocomerce user login redirect
*/
add_filter('woocommerce_login_redirect', function ( $redirect_to ) {
    $redirect_to = home_url(); // example get_permalink( 70 );
    return $redirect_to;
});

/**
 * Woocomerce - Show cart contents / total Ajax
 */
add_filter( 'woocommerce_add_to_cart_fragments', function ( $fragments ) {
	global $woocommerce;

	ob_start();

    ?>
    <div class="cart-total">
    <a class="cart-customlocation" href="<?php echo esc_url(wc_get_cart_url()); ?>" title="<?php _e('View your shopping cart', 'woocommerce'); ?>" data-cart-number="<?php echo $woocommerce->cart->cart_contents_count ?>">
        <?php echo sprintf(_n('(%d)', '(%d)', $woocommerce->cart->cart_contents_count, 'woocommerce'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?>
    </a>
    </div>
	<?php
	$fragments['a.cart-customlocation'] = ob_get_clean();
	return $fragments;
} );


/**
* WooCommerce: show all product attributes, separated by comma, on cart page
*/
/*add_filter( 'woocommerce_cart_item_name', function ( $item_name, $cart_item, $cart_item_key) {

    //$item_data = $cart_item_key['data'];
    //var_dump($cart_item_key['data']);
    //$attributes = $item_data->get_attributes();

    if ( ! $attributes ) {
        return $cart_item;
    }

    $out = $cart_item . '<br />';

    $count = count( $attributes );

    $i = 0;
    foreach ( $attributes as $attribute ) {

        // skip variations
        if ( $attribute->get_variation() ) {
             continue;
        }

        $name = $attribute->get_name();
        if ( $attribute->is_taxonomy() ) {

            $product_id = $item_data->get_id();
            $terms = wp_get_post_terms( $product_id, $name, 'all' );

            // get the taxonomy
            $tax = $terms[0]->taxonomy;

            // get the tax object
            $tax_object = get_taxonomy($tax);

            // get tax label
            if ( isset ( $tax_object->labels->singular_name ) ) {
                $tax_label = $tax_object->labels->singular_name;
            } elseif ( isset( $tax_object->label ) ) {
                $tax_label = $tax_object->label;
                // Trim label prefix since WC 3.0
                $label_prefix = 'Product ';
                if ( 0 === strpos( $tax_label,  $label_prefix ) ) {
                    $tax_label = substr( $tax_label, strlen( $label_prefix ) );
                }
            }
            $out .= $tax_label . ': ';

            $tax_terms = array();
            foreach ( $terms as $term ) {
                $single_term = esc_html( $term->name );
                array_push( $tax_terms, $single_term );
            }
            $out .= implode(', ', $tax_terms);

            if ( $count > 1 && ( $i < ($count - 1) ) ) {
                $out .= ', ';
            }

            $i++;
            // end for taxonomies

        } else {

            // not a taxonomy

            $out .= $name . ': ';
            $out .= esc_html( implode( ', ', $attribute->get_options() ) );

            if ( $count > 1 && ( $i < ($count - 1) ) ) {
                $out .= ', ';
            }

            $i++;
        }
    }
    echo $out;
}, 10, 3 );*/


/**
 *  woocomerce show product categories to Cart template
*/
/*add_filter( 'woocommerce_cart_item_name', function ( $item_name, $cart_item, $cart_item_key ) {
    $term_names = array();

    // Get product categories
    $terms = wp_get_post_terms( $cart_item['product_id'], 'product_cat' );

    if( count($terms) > 0 ){
        foreach( $terms as $term ) $term_names[] = $term->name;

        $item_name .= '<p class="item-category" style="margin:12px 0 0; font-size: .875em;">
            <strong class="label">' . _n( 'Category', 'Categories', count($terms), 'woocommerce' ) . ': </strong>
            <span class="values">' . implode( ', ', $term_names ) . '</span>
        </p>';
    }
    return $item_name;
}, 10, 3);*/

add_filter( 'woocommerce_cart_item_name', function ($title, $cart_item, $cart_item_key ) {
    $item = $cart_item['data'];

    if(!empty($item) && $item->is_type( 'variation' ) ) {
        return $item->get_name();
    } else
        return $title;
}, 20, 3);

// add_filter('post_type_link', function($post_link, $post, $leavename, $sample) {
//     if (false !== strpos($post_link, '%event-category%')) {
//         $eventscategory_type_term = get_the_terms($post->ID, 'event_category');
//         if (!empty($eventscategory_type_term))
//             $post_link = str_replace('%event-category%', array_pop($eventscategory_type_term)->
//             slug, $post_link);
//         else
//             $post_link = str_replace('%event-category%', 'uncategorized', $post_link);
//     }
//     return $post_link;
// }, 10, 4);


// add_filter( 'woocommerce_product_add_to_cart_text', 'custom_cart_button_text' );
//     function custom_cart_button_text() {
//         global $product;
//     if ( has_term( 'flower-subscriptions', 'product_cat', $product->ID ) ) {
//         return 'Subscribe Now';
//     }else {
//         return 'Subscribe Now';
//     }
// }

add_filter('woocommerce_product_single_add_to_cart_text', function () {
    global $post;
    $terms = get_the_terms( $post->ID, 'product_cat' );

    if($terms[0]->slug == "flower-subscriptions"){
        return 'Subscribe Now';
    }else {
        return 'Add to Cart';
    }
});


//added the amount of related products
add_filter( 'woocommerce_output_related_products_args', function(){
    $args = array(
        'posts_per_page' => 9,
        'columns'        => 9,
        'orderby'        => 'rand', // @codingStandardsIgnoreLine.
    );
    return $args;
}, 20 );

remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );


/* Hide item meta on emails to "hide" the mess with conflicting IDs in the database */
// add_filter( 'woocommerce_display_item_meta', function($html, $item, $args) {
//     $html = '';

//     return $html;
// }, 20, 3 );
