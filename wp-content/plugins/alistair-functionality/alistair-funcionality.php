<?php
/**
 * Plugin Name:       Alistair Functionality
 * Plugin URI:        https://www.blexr.com
 * Description:       Let's pump up this WP install with some good ol functionality...
 * Version:           1.0.0
 * Author:            Keen
 * Author URI:        https://www.keen.com.mt/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       alistair-functionality
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( !defined( 'WPINC' ) ) {
	die;
}

function enqueue_script() {   
	wp_enqueue_script( 'admin-scripts', plugin_dir_url( __FILE__ ) . 'includes/js/admin-scripts.js' );
}
add_action('admin_enqueue_scripts', 'enqueue_script');

define('BASE_PATH', plugin_dir_path(__FILE__));

include 'includes/al-functionality-wedding.php';
include 'includes/al-functionality-design.php';
include 'includes/al-functionality-testimonial.php';
include 'includes/al-functionality-events-and-designs.php';
include 'includes/al-functionality-vacancies.php';
include 'includes/al-functionality-features.php';
include 'includes/al-functionality-blog.php';
include 'includes/al-functions.php';
