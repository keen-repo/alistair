<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * Hide buttons in editor
 */
add_filter( 'crb_media_buttons_html', function( $html, $field_name ) {
	if (
		$field_name === 'design_name' ||
		$field_name === 'design_images' 
	) {
		return;
	}
	return $html;
}, 10, 2);

add_action( 'carbon_fields_register_fields', function () {
    Container::make( 'post_meta', 'Blog Details' )
		->where( 'post_type', '=', 'blog' )
		->add_tab( __('Preview Image'), array(
			Field::make( 'image', 'blog_preview_image' )
		) );
});

// Register Custom Post Type
function blog() {

	$labels = array(
		'name'                  => _x( 'Blog', 'Post Type General Name', 'ax-front' ),
		'singular_name'         => _x( 'Blog', 'Post Type Singular Name', 'ax-front' ),
		'menu_name'             => __( 'Blog', 'ax-front' ),
		'name_admin_bar'        => __( 'Blog', 'ax-front' ),
		'archives'              => __( 'Item Archives', 'ax-front' ),
		'attributes'            => __( 'Item Attributes', 'ax-front' ),
		'parent_item_colon'     => __( 'Parent Item:', 'ax-front' ),
		'all_items'             => __( 'All Items', 'ax-front' ),
		'add_new_item'          => __( 'Add New Item', 'ax-front' ),
		'add_new'               => __( 'Add New', 'ax-front' ),
		'new_item'              => __( 'New Item', 'ax-front' ),
		'edit_item'             => __( 'Edit Item', 'ax-front' ),
		'update_item'           => __( 'Update Item', 'ax-front' ),
		'view_item'             => __( 'View Item', 'ax-front' ),
		'view_items'            => __( 'View Items', 'ax-front' ),
		'search_items'          => __( 'Search Item', 'ax-front' ),
		'not_found'             => __( 'Not found', 'ax-front' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'ax-front' ),
		'featured_image'        => __( 'Featured Image', 'ax-front' ),
		'set_featured_image'    => __( 'Set featured image', 'ax-front' ),
		'remove_featured_image' => __( 'Remove featured image', 'ax-front' ),
		'use_featured_image'    => __( 'Use as featured image', 'ax-front' ),
		'insert_into_item'      => __( 'Insert into item', 'ax-front' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'ax-front' ),
		'items_list'            => __( 'Items list', 'ax-front' ),
		'items_list_navigation' => __( 'Items list navigation', 'ax-front' ),
		'filter_items_list'     => __( 'Filter items list', 'ax-front' ),
	);
	$rewrite = array(
		'slug'                  => 'blog',
		'with_front'            => false,
		'pages'                 => false,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Blog', 'ax-front' ),
		'description'           => __( 'Alistair blog manager', 'ax-front' ),
		'labels'                => $labels,
		'supports'              => array( 'title'),
		'taxonomies'            => array( 'blog_category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-welcome-write-blog',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest' => true,
		'supports' => array('title','editor','thumbnail','author','comments')
	);
	register_post_type( 'blog', $args );

}
add_action( 'init', 'blog', 0 );

// Register Custom Taxonomy
function blog_category() {

	$labels = array(
		'name'                       => _x( 'Categories', 'Taxonomy General Name', 'ax-front' ),
		'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'ax-front' ),
		'menu_name'                  => __( 'Categories', 'ax-front' ),
		'all_items'                  => __( 'All Categories', 'ax-front' ),
		'parent_item'                => __( 'Parent Item', 'ax-front' ),
		'parent_item_colon'          => __( 'Parent Item:', 'ax-front' ),
		'new_item_name'              => __( 'New Item Name', 'ax-front' ),
		'add_new_item'               => __( 'Add New Item', 'ax-front' ),
		'edit_item'                  => __( 'Edit Item', 'ax-front' ),
		'update_item'                => __( 'Update Item', 'ax-front' ),
		'view_item'                  => __( 'View Item', 'ax-front' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'ax-front' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'ax-front' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'ax-front' ),
		'popular_items'              => __( 'Popular Items', 'ax-front' ),
		'search_items'               => __( 'Search Items', 'ax-front' ),
		'not_found'                  => __( 'Not Found', 'ax-front' ),
		'no_terms'                   => __( 'No items', 'ax-front' ),
		'items_list'                 => __( 'Items list', 'ax-front' ),
		'items_list_navigation'      => __( 'Items list navigation', 'ax-front' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest' 				 => true,
	);
	register_taxonomy( 'blog_category', array( 'blog' ), $args );

}
add_action( 'init', 'blog_category', 0 );
