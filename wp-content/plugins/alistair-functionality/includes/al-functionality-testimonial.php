<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * Define CPT custom fields
 */
add_action( 'carbon_fields_register_fields', function () {
    Container::make( 'post_meta', 'Testimonial Details' )
		->where( 'post_type', '=', 'testimonial' )
		->add_tab( __('Testimonial Client Name'), array(
			Field::make( 'rich_text', 'testimonial_client_name' )
		) )
		->add_tab( __('Testimonial Text'), array(
			Field::make( 'rich_text', 'testimonial_text' )
		) )
		->add_tab( __('Testimonial Image'), array(
			Field::make( 'image', 'testimonial_image' )
		) );
});

/**
 * Hide buttons in editor
 */
add_filter( 'crb_media_buttons_html', function( $html, $field_name ) {
	if (
		$field_name === 'testimonial_client_name' ||
		$field_name === 'testimonial_text' ||
		$field_name === 'testimonial_image'
	) {
		return;
	}
	return $html;
}, 10, 2);

// Register Custom Post Type
function testimonial() {

	$labels = array(
		'name'                  => _x( 'Testimonials', 'Post Type General Name', 'ax-front' ),
		'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'ax-front' ),
		'menu_name'             => __( 'Testimonials', 'ax-front' ),
		'name_admin_bar'        => __( 'Testimonial', 'ax-front' ),
		'archives'              => __( 'Item Archives', 'ax-front' ),
		'attributes'            => __( 'Item Attributes', 'ax-front' ),
		'parent_item_colon'     => __( 'Parent Item:', 'ax-front' ),
		'all_items'             => __( 'All Items', 'ax-front' ),
		'add_new_item'          => __( 'Add New Item', 'ax-front' ),
		'add_new'               => __( 'Add New', 'ax-front' ),
		'new_item'              => __( 'New Item', 'ax-front' ),
		'edit_item'             => __( 'Edit Item', 'ax-front' ),
		'update_item'           => __( 'Update Item', 'ax-front' ),
		'view_item'             => __( 'View Item', 'ax-front' ),
		'view_items'            => __( 'View Items', 'ax-front' ),
		'search_items'          => __( 'Search Item', 'ax-front' ),
		'not_found'             => __( 'Not found', 'ax-front' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'ax-front' ),
		'featured_image'        => __( 'Featured Image', 'ax-front' ),
		'set_featured_image'    => __( 'Set featured image', 'ax-front' ),
		'remove_featured_image' => __( 'Remove featured image', 'ax-front' ),
		'use_featured_image'    => __( 'Use as featured image', 'ax-front' ),
		'insert_into_item'      => __( 'Insert into item', 'ax-front' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'ax-front' ),
		'items_list'            => __( 'Items list', 'ax-front' ),
		'items_list_navigation' => __( 'Items list navigation', 'ax-front' ),
		'filter_items_list'     => __( 'Filter items list', 'ax-front' ),
	);
	$rewrite = array(
		'slug'                  => 'testimonials',
		'with_front'            => false,
		'pages'                 => false,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Testimonial', 'ax-front' ),
		'description'           => __( 'Alistair testimonials manager', 'ax-front' ),
		'labels'                => $labels,
		'supports'              => array( 'title'),
		'taxonomies'            => array( 'testimonial_category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-welcome-write-blog',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
	);
	register_post_type( 'testimonial', $args );

}
add_action( 'init', 'testimonial', 0 );

// Register Custom Taxonomy
function testimonial_category() {

	$labels = array(
		'name'                       => _x( 'Categories', 'Taxonomy General Name', 'ax-front' ),
		'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'ax-front' ),
		'menu_name'                  => __( 'Categories', 'ax-front' ),
		'all_items'                  => __( 'All Categories', 'ax-front' ),
		'parent_item'                => __( 'Parent Item', 'ax-front' ),
		'parent_item_colon'          => __( 'Parent Item:', 'ax-front' ),
		'new_item_name'              => __( 'New Item Name', 'ax-front' ),
		'add_new_item'               => __( 'Add New Item', 'ax-front' ),
		'edit_item'                  => __( 'Edit Item', 'ax-front' ),
		'update_item'                => __( 'Update Item', 'ax-front' ),
		'view_item'                  => __( 'View Item', 'ax-front' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'ax-front' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'ax-front' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'ax-front' ),
		'popular_items'              => __( 'Popular Items', 'ax-front' ),
		'search_items'               => __( 'Search Items', 'ax-front' ),
		'not_found'                  => __( 'Not Found', 'ax-front' ),
		'no_terms'                   => __( 'No items', 'ax-front' ),
		'items_list'                 => __( 'Items list', 'ax-front' ),
		'items_list_navigation'      => __( 'Items list navigation', 'ax-front' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'testimonial_category', array( 'testimonial' ), $args );

}
add_action( 'init', 'testimonial_category', 0 );
