<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * Define CPT custom fields
 */
add_action( 'carbon_fields_register_fields', function () {
    Container::make( 'post_meta', 'Event Details' )
		->where( 'post_type', '=', 'event' )
		->add_tab( __('Event Name'), array(
			Field::make( 'rich_text', 'event_name' )
		) )
		->add_tab( __('Event Images'), array(
			Field::make( 'media_gallery', 'event_images' ) ->set_type( array( 'image' ) )
		) );
});

/**
 * Hide buttons in editor
 */
add_filter( 'crb_media_buttons_html', function( $html, $field_name ) {
	if (
		$field_name === 'event_name' ||
		$field_name === 'event_images' 
	) {
		return;
	}
	return $html;
}, 10, 2);

// Register Custom Post Type
function event() {

	$labels = array(
		'name'                  => _x( 'Events & Designs', 'Post Type General Name', 'ax-front' ),
		'singular_name'         => _x( 'Event', 'Post Type Singular Name', 'ax-front' ),
		'menu_name'             => __( 'Events & Designs', 'ax-front' ),
		'name_admin_bar'        => __( 'Event', 'ax-front' ),
		'archives'              => __( 'Item Archives', 'ax-front' ),
		'attributes'            => __( 'Item Attributes', 'ax-front' ),
		'parent_item_colon'     => __( 'Parent Item:', 'ax-front' ),
		'all_items'             => __( 'All Items', 'ax-front' ),
		'add_new_item'          => __( 'Add New Item', 'ax-front' ),
		'add_new'               => __( 'Add New', 'ax-front' ),
		'new_item'              => __( 'New Item', 'ax-front' ),
		'edit_item'             => __( 'Edit Item', 'ax-front' ),
		'update_item'           => __( 'Update Item', 'ax-front' ),
		'view_item'             => __( 'View Item', 'ax-front' ),
		'view_items'            => __( 'View Items', 'ax-front' ),
		'search_items'          => __( 'Search Item', 'ax-front' ),
		'not_found'             => __( 'Not found', 'ax-front' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'ax-front' ),
		'featured_image'        => __( 'Featured Image', 'ax-front' ),
		'set_featured_image'    => __( 'Set featured image', 'ax-front' ),
		'remove_featured_image' => __( 'Remove featured image', 'ax-front' ),
		'use_featured_image'    => __( 'Use as featured image', 'ax-front' ),
		'insert_into_item'      => __( 'Insert into item', 'ax-front' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'ax-front' ),
		'items_list'            => __( 'Items list', 'ax-front' ),
		'items_list_navigation' => __( 'Items list navigation', 'ax-front' ),
		'filter_items_list'     => __( 'Filter items list', 'ax-front' ),
	);
	$rewrite = array(
		'slug'                  => 'events-corporate/events-designs/event-category',
		'with_front'            => false,
		'pages'                 => false,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Event', 'ax-front' ),
		'description'           => __( 'Alistair events and designs manager', 'ax-front' ),
		'labels'                => $labels,
		'supports'              => array( 'title'),
		'taxonomies'            => array( 'event_category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-welcome-write-blog',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest' => true,
  		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);
	register_post_type( 'event', $args );

}
add_action( 'init', 'event', 0 );

// Register Custom Taxonomy
function event_category() {

	$labels = array(
		'name'                       => _x( 'Event Categories', 'Taxonomy General Name', 'ax-front' ),
		'singular_name'              => _x( 'Event Category', 'Taxonomy Singular Name', 'ax-front' ),
		'menu_name'                  => __( 'Event Categories', 'ax-front' ),
		'all_items'                  => __( 'All Categories', 'ax-front' ),
		'parent_item'                => __( 'Parent Item', 'ax-front' ),
		'parent_item_colon'          => __( 'Parent Item:', 'ax-front' ),
		'new_item_name'              => __( 'New Item Name', 'ax-front' ),
		'add_new_item'               => __( 'Add New Item', 'ax-front' ),
		'edit_item'                  => __( 'Edit Item', 'ax-front' ),
		'update_item'                => __( 'Update Item', 'ax-front' ),
		'view_item'                  => __( 'View Item', 'ax-front' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'ax-front' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'ax-front' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'ax-front' ),
		'popular_items'              => __( 'Popular Items', 'ax-front' ),
		'search_items'               => __( 'Search Items', 'ax-front' ),
		'not_found'                  => __( 'Not Found', 'ax-front' ),
		'no_terms'                   => __( 'No items', 'ax-front' ),
		'items_list'                 => __( 'Items list', 'ax-front' ),
		'items_list_navigation'      => __( 'Items list navigation', 'ax-front' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest' 				 => true,
		'rewrite'           => array( 'slug' => 'event-category', 'hierarchical' => true, ),
		'show_in_rest' => true,
  		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);
	register_taxonomy( 'event_category', array( 'event' ), $args );

}
add_action( 'init', 'event_category', 0 );

