jQuery(document).ready(function($){
	'use strict';

	// Instantiates the variable that holds the media library frame.
	var metaImageFrame;

	$('#bx_horizontal_video_upload_btn, #bx_vertical_video_upload_btn').click(function(e){
		var btn = e.target;
		var field = $( btn ).data( 'media-uploader-target' );
		var convertButton = $( btn ).data( 'media-uploader-convert-target' );
		e.preventDefault();

		// Sets up the media library frame
		metaImageFrame = wp.media.frames.metaImageFrame = wp.media({
			title: meta_image.title,
			button: { text:  'Use this file' },
			multiple: false,
			library: {
				type: [ 'video' ]
			},
		});

		// Runs when an image is selected.
		metaImageFrame.on('select', function() {

			// Grabs the attachment selection and creates a JSON representation of the model.
			var media_attachment = metaImageFrame.state().get('selection').first().toJSON();

			// Sends the attachment URL to our custom image input field.
			$( field ).val(media_attachment.url);
			$( convertButton ).show();

		});

		// Opens the media library frame.
		metaImageFrame.open();
	});

	// Handle convert video ajax
	$('#bx_horizontal_video_convert_btn, #bx_vertical_video_convert_btn').click(function(e){
		var btn = e.target;
		var orientation = $(btn).data('orientation');
		var inputTarget = $('#bx_'+orientation+'_media');
		var jobTarget = $('#bx_'+orientation+'_job');
		var messageTarget = $('#bx_'+orientation+'_messages');
		var originalURL = inputTarget.val();

		var inputParent = inputTarget.parents('fieldset');
		inputParent.find('.spinner').css('visibility', 'visible');
		$( btn ).css('visibility', 'hidden');

		if(orientation == 'horizontal'){
			$('#bx_horizontal_media_mp4_hd').val('');
			$('#bx_horizontal_media_mp4_sd').val('');
			$('#bx_horizontal_media_webm_hd').val('');
			$('#bx_horizontal_media_webm_sd').val('');
			$('#bx_horizontal_media_ogv_hd').val('');
			$('#bx_horizontal_media_ogv_sd').val('');
			$('#bx_horizontal_messages').html('');
		} else {
			$('#bx_vertical_media_mp4_hd').val('');
			$('#bx_vertical_media_mp4_sd').val('');
			$('#bx_vertical_media_webm_hd').val('');
			$('#bx_vertical_media_webm_sd').val('');
			$('#bx_vertical_media_ogv_hd').val('');
			$('#bx_vertical_media_ogv_sd').val('');
			$('#bx_vertical_messages').html('');
		}

		if(originalURL !== ''){

			var data = {
				'action': 'bx_hero_video_add_to_queue',
				'original': originalURL,
				'orientation': orientation
			};

			$.post(ajaxurl, data, function(response) {
				var ajaxResponse = $.parseJSON(response);
				inputParent.find('.spinner').css('visibility', 'hidden');

				if(ajaxResponse.error !== undefined){
					console.log(ajaxResponse.error);
					messageTarget.text('Cannot add job to queue. Error: ' + ajaxResponse.error);
				} else {
					jobTarget.val(ajaxResponse.job);
					messageTarget.text('Job has been added to queue. Video will be converted in the background. Page will now refresh so data is saved...')
					$('#publishing-action input[type="submit"]').click();
				}
			});
		} else {
			alert('No video URL specified');
		}
	});

	// check job progress if a job id exists and statuses are empty
	var horizontalJob = $('#bx_horizontal_job').val()*1;
	if(horizontalJob > 0){
		check_job_progress(horizontalJob, 'horizontal');
	}

	var verticalJob = $('#bx_vertical_job').val()*1;
	if(verticalJob > 0){
		check_job_progress(verticalJob, 'vertical');
	}

	function check_job_progress(job, orientation){
		console.log(orientation + ' job is not empty, checking for status...');
		// only run if status of job is not 10! TODO
		$('#bx_'+orientation+'_messages').html('Retrieving conversion status...');

		setInterval(function(){
			var data = {
				'action': 'bx_hero_video_job_progress',
				'job': job,
				'post_id': $('#post_ID').val()
			}
			$.post(ajaxurl, data, function(response) {
				response = $.parseJSON(response);
				$('#bx_'+orientation+'_status').val(response.status);
				$('#bx_'+orientation+'_media_mp4_hd').val(response.mp4_hd);
				$('#bx_'+orientation+'_media_mp4_sd').val(response.mp4_sd);
				$('#bx_'+orientation+'_media_webm_hd').val(response.webm_hd);
				$('#bx_'+orientation+'_media_webm_sd').val(response.webm_sd);
				$('#bx_'+orientation+'_media_ogv_hd').val(response.ogv_hd);
				$('#bx_'+orientation+'_media_ogv_sd').val(response.ogv_sd);

				update_messages(response, orientation);
			});
		}, 5000);
	}

	// let's add some nice loaders
	function update_messages(response, orientation){

		var status = parseInt(response.status);
		var format = '';
		var percent = 0;
		var message = '';

		if(status < 10 && status > 0){
			switch(status){
				case 1:
					format = 'MP4 HD';
					percent = response.mp4_hd;
					break;
				case 2:
					format = 'WEBM HD';
					percent = response.webm_hd;
					break;
				case 3:
					format = 'OGV HD';
					percent = response.ogv_hd;
					break;
				case 4:
					format = 'MP4 SD';
					percent = response.mp4_sd;
					break;
				case 5:
					format = 'WEBM SD';
					percent = response.webm_sd;
					break;
				case 6:
					format = 'OGV SD';
					percent = response.ogv_sd;
					break;
			}

			if(isNaN(parseInt(percent))){
				message = 'Converting '+format+'...';
			} else {
				message = '<span class="format-label">' + format + ':</span> <span class="bar"><span class="active" style="right: '+(100-percent)+'%"></span></span>';
			}
		} else if(status == 10) {
			message = 'All conversions are done!';
		} else if(status == 0){
			message = 'Job waiting to be picked up...';
		}

		$('#bx_'+orientation+'_messages').html(message);
	}

});
