<?php

/**
 * Plugin Name: Alistair Custom Shipping Method
 * Description: Custom Shipping Method for WooCommerce
 * Version: 1.0.0
 * Author: Keen
 * Author URI: https://www.keen.com.mt
 */

if ( ! defined( 'WPINC' ) ) {

    die;

}

/*
 * Check if WooCommerce is active
 */
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

    function alistair_shipping_method() {
        if ( ! class_exists( 'Alistair_Shipping_Method' ) ) {
            class Alistair_Shipping_Method extends WC_Shipping_Method {
                /**
                 * Constructor for your shipping class
                 *
                 * @access public
                 * @return void
                 */
                public function __construct() {
                    $this->id                 = 'alistair';
                    $this->method_title       = __( 'Alistair Shipping', 'alistair' );
                    $this->method_description = __( 'Custom Shipping Method for Alistair', 'tutalistairsplus' );

                    $this->init();

                    $this->enabled = isset( $this->settings['enabled'] ) ? $this->settings['enabled'] : 'yes';
					$this->title = isset( $this->settings['title'] ) ? $this->settings['title'] : __( 'Alistair Shipping', 'alistair' );
					$this->sunday_cost = isset( $this->settings['sunday_cost'] ) ? $this->settings['sunday_cost'] : 15;
					$this->gozo_cost = isset( $this->settings['gozo_cost'] ) ? $this->settings['gozo_cost'] : 15;
					$this->gozo_sunday_cost = isset( $this->settings['gozo_sunday_cost'] ) ? $this->settings['gozo_sunday_cost'] : 30;
                }

                /**
                 * Init your settings
                 *
                 * @access public
                 * @return void
                 */
                function init() {
                    // Load the settings API
                    $this->init_form_fields();
                    $this->init_settings();

                    // Save settings in admin if you have any defined
                    add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
                }

                /**
                 * Define settings field for this shipping
                 * @return void
                 */
                function init_form_fields() {

                    $this->form_fields = array(

						'enabled' => array(
							'title' => __( 'Enable', 'alistair' ),
							'type' => 'checkbox',
							'description' => __( 'Enable this shipping.', 'alistair' ),
							'default' => 'yes'
						),

						'title' => array(
							'title' => __( 'Title', 'alistair' ),
							'type' => 'text',
							'description' => __( 'Title to be display on site', 'alistair' ),
							'default' => __( 'Alistair Shipping', 'alistair' )
						),

						'sunday_cost' => array(
							'title' => __( 'Malta Sunday and Public Holidays', 'alistair' ),
							'type' => 'text',
							'description' => __( 'Extra cost to charge on checkout', 'alistair' ),
							'default' => __( '15', 'alistair' )
						),

						'gozo_cost' => array(
							'title' => __( 'Gozo Standard', 'alistair' ),
							'type' => 'text',
							'description' => __( 'Extra cost to charge on checkout', 'alistair' ),
							'default' => __( '15', 'alistair' )
						),

						'gozo_sunday_cost' => array(
							'title' => __( 'Gozo Sunday and Public Holidays', 'alistair' ),
							'type' => 'text',
							'description' => __( 'Extra cost to charge on checkout', 'alistair' ),
							'default' => __( '30', 'alistair' )
						),
					);

                }

                /**
                 * This function is used to calculate the shipping cost. Within this function we can check for weights, dimensions and other parameters.
                 *
                 * @access public
                 * @param mixed $package
                 * @return void
                 */
                public function calculate_shipping( $package = array() ) {

					$rate = array(
						'id' => $this->id,
						'label' => $this->title,
						'cost' => 0
					);

					$this->add_rate( $rate );

                }
            }
        }
    }

    add_action( 'woocommerce_shipping_init', 'alistair_shipping_method' );

    function add_alistair_shipping_method( $methods ) {
        $methods[] = 'Alistair_Shipping_Method';
        return $methods;
    }

	add_filter( 'woocommerce_shipping_methods', 'add_alistair_shipping_method' );

}
