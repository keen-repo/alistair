<?php 
define( 'DONOTCACHEPAGE', true );

$_SERVER['HTTP_HOST'] = 'www.alistairfloraldesign.com';
$_SERVER['HTTPS'] = 'on';

define('WP_USE_THEMES', false);
require('../wp-blog-header.php');



$products = get_posts(array(
    'post_type' => 'product',
    'numberposts' => -1
));

update_option('all_products', $products);

die('products updated!');
